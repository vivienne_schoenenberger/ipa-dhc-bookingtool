package com.example.application;

import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.shared.ui.Transport;
import com.vaadin.flow.theme.Theme;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The entry point of the Spring Boot application.
 *
 * Use the @PWA annotation make the application installable on phones, tablets
 * and some desktop browsers.
 *
 */

@PWA(name = "Vaadin CRM",
        shortName = "CRM")
@EntityScan("com.example.application.entities")
@EnableJpaRepositories("com.example.application.repos")
@Theme(value = "mytodo")
@SpringBootApplication
@NpmPackage(value = "lumo-css-framework", version = "^4.0.10")
@Push()
public class Application extends SpringBootServletInitializer implements AppShellConfigurator {
    public static void main(String[] args) {SpringApplication.run(Application.class, args);}
}
