package com.example.application.controller;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class WebhookController {
    //TODO: accessing the endpoint only creates 302
    @PostMapping (value="/webhook/notification",consumes = "application/x-www-form-urlencoded",produces ="application/json")
    public JSONObject createDetails (@RequestBody MultiValueMap<String, String> request){
        JSONObject response = new JSONObject();
        response.put("notificationResponse","[accepted]");
        System.out.println("....");
        WrappedSession session = VaadinSession.getCurrent().getSession();
        return response;
    }
}
