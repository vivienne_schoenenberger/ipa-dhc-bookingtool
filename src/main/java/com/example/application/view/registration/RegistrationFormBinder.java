package com.example.application.view.registration;


import com.example.application.entities.ApplicationUser;
import com.example.application.service.UserRoleService;
import com.example.application.service.UserService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.data.binder.*;
import com.vaadin.flow.data.validator.EmailValidator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class RegistrationFormBinder {

     final RegistrationForm registrationForm;

    private UserService userService;

    /**
     * Flag for disabling first run for password validation
     */
    private boolean enablePasswordValidation;

    private boolean enableUsernameValidation;


    private UserRoleService userRoleService;

    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    public RegistrationFormBinder(RegistrationForm registrationForm, UserService userService, UserRoleService userRoleService) {
        this.registrationForm = registrationForm;
        this.userRoleService = userRoleService;
        this.userService = userService;

    }
    /**
     * Method to add the data binding and validation logics
     * to the registration form
     */

    public void addBindingAndValidation() {
        Binder<ApplicationUser> binder = new Binder<>(ApplicationUser.class);
        binder.bindInstanceFields(registrationForm);

        // Check if username is unique
        binder.forField(registrationForm.getUsername())
                .withValidator(this::checkIfUserUnique).bind("username");

            binder.forField(registrationForm.getFirstName())
                    .withValidator(
                            name -> name.length() >= 2,
                            "Name must contain at least two characters")
                    .bind(ApplicationUser::getFirstName, ApplicationUser::setFirstName);

            binder.forField(registrationForm.getLastName())
                    .withValidator(
                            name -> name.length() >= 2,
                            "Last name must contain at least two characters")
                    .bind(ApplicationUser::getLastName, ApplicationUser::setLastName);

            binder.forField(registrationForm.getEmail())
                    .withValidator(name -> name.length() >= 2, "Email can't be empty")
                    .withValidator(new EmailValidator("This doesn't look like a valid email address"))
                    .bind(ApplicationUser::getEmail, ApplicationUser::setEmail);

        // A custom validator for password fields
        binder.forField(registrationForm.getPasswordField())
                .withValidator(this::passwordValidator).bind("password");

        // The second password field is not connected to the Binder, but we
        // want the binder to re-check the password validator when the field
        // value changes. The easiest way is just to do that manually.

        registrationForm.getPasswordConfirmField().addValueChangeListener(e -> {
            // The user has modified the second field, now we can validate and show errors.
            // See passwordValidator() for how this flag is used.
            enablePasswordValidation = true;

        });

        //username:

        binder.forField(registrationForm.getUsername())
                .withValidator(this::checkIfUserUnique).bind("username");

        registrationForm.getUsername().addValueChangeListener(e ->{
            enableUsernameValidation = true;

        });

        // Set the label where bean-level error messages go
        binder.setStatusLabel(registrationForm.getErrorMessageField());

        // And finally the submit button
        registrationForm.getSubmitButton().addClickListener(event -> {
            try {

                // Create empty bean to store the details into
                ApplicationUser userBean = new ApplicationUser();

                // Run validators and write the values to the bean
                binder.writeBean(userBean);
                //set Userrole
                userBean.setUserRole(userRoleService.getUserRoleById(3L));
                // Encrypt PW
                BCryptPasswordEncoder passwordEncoder = passwordEncoder();
                userBean.setPassword(passwordEncoder.encode(userBean.getPassword()));
                // Typically, you would here call backend to store the bean
                userService.add(userBean);

                // Show success message if everything went well
                showSuccess(userBean);

                UI.getCurrent().navigate("/login");

            } catch (ValidationException exception) {
                // validation errors are already visible for each field,
                // and bean-level errors are shown in the status label.
                // We could show additional messages here if we want, do logging, etc.
            }
        });
    }

    /**
     * Method to validate that:
     * <p>
     * 1) Password is at least 8 characters long
     * <p>
     * 2) Values in both fields match each other
     */
    private ValidationResult passwordValidator(String pass1, ValueContext ctx) {
        /*
         * Just a simple length check. A real version should check for password
         * complexity as well!
         */

        if (pass1 == null || pass1.length() < 8) {
            return ValidationResult.error("Password should be at least 8 characters long");
        }

        if (!enablePasswordValidation) {
            // user hasn't visited the field yet, so don't validate just yet, but next time.
            enablePasswordValidation = true;
            return ValidationResult.ok();
        }

        String pass2 = registrationForm.getPasswordConfirmField().getValue();

        if (pass1 != null && pass1.equals(pass2)) {
            return ValidationResult.ok();
        }

        return ValidationResult.error("Passwords do not match");
    }

    /**
     * We call this method when form submission has succeeded
     */
private ValidationResult checkIfUserUnique(String username, ValueContext ctx){

    if (!enableUsernameValidation) {
        // user hasn't visited the field yet, so don't validate just yet, but next time.
        enableUsernameValidation = true;
        return ValidationResult.ok();
    }

    if ( username == "" ){
        return ValidationResult.error("Username can't be empty");
    }
    if ( !userService.checkIfUsernameExists(username)){
        return ValidationResult.ok();
    }
    return ValidationResult.error("Desired username already assigned" );
}

    private void showSuccess(ApplicationUser userBean) {

        Notification notification =
                Notification.show("Data saved, welcome " + userBean.getFirstName());
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);

        // Here you'd typically redirect the user to another view
    }




}