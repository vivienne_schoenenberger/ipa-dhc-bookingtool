package com.example.application.view.registration;

import com.example.application.service.UserRoleService;
import com.example.application.service.UserService;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;

@AnonymousAllowed
@Route("/register")
@PageTitle("Registration | Bookingtool")

public class RegistrationView extends VerticalLayout {

    public RegistrationView(UserService userService, UserRoleService userRoleService) {

        RegistrationForm registrationForm = new RegistrationForm();
        // Center the RegistrationForm
        setHorizontalComponentAlignment(Alignment.CENTER, registrationForm);
        setSizeFull();
        setAlignItems(Alignment.CENTER);
        setJustifyContentMode(JustifyContentMode.CENTER);
        add(registrationForm);

        RegistrationFormBinder registrationFormBinder = new RegistrationFormBinder(registrationForm, userService, userRoleService);
        registrationFormBinder.addBindingAndValidation();





    }
}