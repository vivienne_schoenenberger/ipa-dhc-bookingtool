package com.example.application.view.payment;

import com.example.application.entities.Reservation;
import com.example.application.filter.MyAsyncThread;
import com.example.application.service.ReservationService;
import com.example.application.view.calendar.CalendarView;
import com.example.application.view.main.MainMenuView;
import com.example.application.view.main.MainView;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@CssImport("./styles/pending.css")
@PermitAll

@Route(value = "payment-pending",layout = MainMenuView.class)
@PageTitle("Pending | Bookingtool")
public class PaymentPending extends VerticalLayout implements BeforeEnterObserver, BeforeLeaveObserver, HasUrlParameter<String>{
    private Long reservationId;
    private ReservationService reservationService;
    private Image reservationImage;
    private H2 paymentTitle;
    private Icon unsuccessfullIcon;
    private Div content;
    private Paragraph detailTitle;
    private Paragraph btnDescription;
    private ProgressBar progressBar;
    private Div progressBarLabel;
    private Div progressBarSubLabel;
    private MyAsyncThread asyncThread;
    private Div btnDiv;
    private Button cancelBtn;
    private Reservation reservation;

    public PaymentPending(ReservationService reservationService, HttpServletRequest httpServletRequest) {
        this.reservationService = reservationService;
        unsuccessfullIcon = new Icon(VaadinIcon.CLOCK);
        unsuccessfullIcon.setId("icon-pending");

        paymentTitle = new H2("Payment pending...");
        paymentTitle.setId("payment-title-pending");
        btnDescription= new Paragraph("Please do not leave this page.\n" +
                "Loading may take a few minutes\n" +
                "\n");
        btnDescription.setId("p-pending");
        progressBar= new ProgressBar();
        progressBar.setIndeterminate(true);
        progressBar.setId("progress-bar");
        progressBarLabel = new Div();
        progressBarLabel.setText("Loading...");
        progressBarSubLabel = new Div();
        progressBarSubLabel.getStyle().set("font-size",
                "var(--lumo-font-size-xs)");
        progressBarSubLabel.setText("Process can take upwards of 10 minutes");
        detailTitle =new Paragraph("Waiting for payment success");
        detailTitle.setId("h3-details-title");
        cancelBtn = new Button("Cancel Payment");
        btnDiv = new Div(btnDescription,cancelBtn);

    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter String parameter) {
        Location location = beforeEvent.getLocation();
        QueryParameters queryParameters = location
                .getQueryParameters();

        Map<String, List<String>> parametersMap =
                queryParameters.getParameters();

        reservationId = Long.valueOf(parametersMap.get("id").get(0));
        reservation = reservationService.findReservationById(reservationId);

        if(reservation.isOnlyRoom()){
            reservationImage = new Image(reservation.getRoom().getImageName(),"");
            reservationImage.setId("reservation-img");
        }
        else{
            reservationImage = new Image("default_workspace.jpg","");
            reservationImage.setId("reservation-img");

        }
        /**
         * If payment gets cancelled the reservation is deleted and
         * user is navigated to calendar view
         */
        cancelBtn.addClickListener(buttonClickEvent -> {
            reservationService.delete(reservation);
            UI.getCurrent().navigate(CalendarView.class);
        });
        setId("layout");
        content= new Div(reservationImage,detailTitle,progressBarLabel,progressBar,progressBarSubLabel,btnDescription,btnDiv);
        content.setId("pending-content-div");
        add(unsuccessfullIcon,paymentTitle,content);
        /**
         * Creating a asynchronized thread for checking updates
         */
        asyncThread = new MyAsyncThread(UI.getCurrent(),reservationService,reservation);
        asyncThread.start();

    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {

        String status = (String) VaadinSession.getCurrent().getSession().getAttribute("payment-status");
        if ( !(status.equals("Refused") || status.equals("Received"))) {
            UI.getCurrent().getPage().setLocation("https://localhost:8443");
            beforeEnterEvent.rerouteTo(MainView.class);
        }
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        VaadinSession.getCurrent().getSession().setAttribute("payment-status",null);
    }
}
