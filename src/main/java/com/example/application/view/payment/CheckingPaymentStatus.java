package com.example.application.view.payment;

import com.example.application.entities.Reservation;
import com.example.application.entities.Workspace;
import com.example.application.filter.MyAsyncThread;
import com.example.application.service.ReservationService;
import com.example.application.service.RestClientService;
import com.example.application.view.calendar.CalendarView;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;
import org.springframework.boot.web.client.RestTemplateBuilder;

import javax.annotation.security.PermitAll;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@CssImport("./styles/loading.css")
@PermitAll
@Route(value = "checkingPaymentStatus")
@PageTitle("Loading | Bookingtool")
public class CheckingPaymentStatus extends VerticalLayout implements HasUrlParameter<String> {
    RestClientService restClientService;
    ReservationService reservationService;
    private String redirectData;
    private String reservationID;
    private String workspaceNameAndDesc;
    private Notification notification;
    private Div errorDiv;


    public CheckingPaymentStatus(RestClientService restClientService, ReservationService reservationService) {
        this.reservationService = reservationService;
        this.restClientService = restClientService;

    }
    /**
     * Since we are getting the status of our
     * payment as a parameter in our given redirect URL (this view)
     * we have to overrride setParameter to get the 'resultCode' which
     * contains the 5 possible status.
     */
    @Override
    public void setParameter(BeforeEvent event,
                             @OptionalParameter String parameter) {
        Location location = event.getLocation();
        QueryParameters queryParameters = location
                .getQueryParameters();

        Map<String, List<String>> parametersMap =
                queryParameters.getParameters();
        restClientService= new RestClientService(new RestTemplateBuilder());
        redirectData = parametersMap.get("redirectResult").get(0);

        JSONObject redirectDataObject = restClientService.getPaymentDetails(redirectData);
        if(redirectDataObject.get("error")!= null){
            notification = new Notification();
            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
            notification.setPosition(Notification.Position.MIDDLE);
            Button calendarBtn = new Button(new Icon(VaadinIcon.CALENDAR));
            calendarBtn.addClickListener(buttonClickEvent -> {
                UI.getCurrent().navigate(CalendarView.class);
            });
            errorDiv = new Div(new Text("An Error has occured: "+redirectDataObject.getAsString("error")), calendarBtn);
            errorDiv.getStyle().set("display","flex");
            errorDiv.getStyle().set("flex-direction","column");
            notification.add(errorDiv);
            notification.open();
        }
        else{
            String resultCode = redirectDataObject.getAsString("resultCode");
            reservationID = redirectDataObject.getAsString("merchantReference");
            navigateToStatus(resultCode);
        }

    }
    /**
     * Now we have to navigate to the right status page and also
     * transfer the reservation ID as a parameter to provide
     * reservation details which have to be desplayed on the success page
     */
    public void navigateToStatus(String paymentStatus) {
        WrappedSession session = VaadinSession.getCurrent().getSession();
        session.setAttribute("payment-status", paymentStatus);
        Reservation reservation= reservationService.findReservationById(Long.valueOf(reservationID));
        workspaceNameAndDesc="";
        for(Workspace workspace: reservation.getWorkspaceList()){
            workspaceNameAndDesc= workspaceNameAndDesc.concat(workspace.getBezeichnung()+": "+ workspace.getWorkspaceDescription()+" ");
        }
        switch (paymentStatus) {
            case "Authorised":
                UI.getCurrent().getPage().setLocation("payment-successfull?id=" + reservationID);
                /**
                 * if the status is successfull a conformation email is sent
                 */
                try {
                    sendEmail(reservation);
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
                break;
            case "Cancelled":
                UI.getCurrent().getPage().setLocation("payment-unsuccessfull?id=" + reservationID+"&status="+paymentStatus );
                break;
            case "Error":
                UI.getCurrent().getPage().setLocation("payment-unsuccessfull?id=" + reservationID+"&status="+paymentStatus);
                break;
            case "Refused":
                UI.getCurrent().getPage().setLocation("payment-unsuccessfull?id=" + reservationID+"&status="+paymentStatus);

                break;
            case "Received":
                UI.getCurrent().getPage().setLocation("payment-pending?id=" + reservationID);
                break;
        }
    }
    /**
     * With JavaMail API we send a simple E-mail containing the most important infos
     */
    public void sendEmail( Reservation reservation) throws MessagingException {
        DateTimeFormatter customFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy 'at' hh:mm");
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        prop.put("mail.smtp.ssl.protocols","TLSv1.2");
        prop.put("mail.transport.protocol","imap");
        prop.put("mail.smtp.starttls.required","true");
        prop.put("mail.smtp.socketFactory.fallback","true");
        prop.put("mail.debug","true");
        String pw = System.getenv("MAIL_PASSWORD");
        String username = System.getenv("MAIL_USERNAME");
        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, pw);
            }
        });
        Message message = new MimeMessage(session);
        message.setRecipient(Message.RecipientType.TO,new InternetAddress(reservation.getUser().getEmail()));
        String msg="";
        if(reservation.isOnlyRoom()) {
            msg = ("<p>Hello <b>" + reservation.getUser().getFirstName() + " " + reservation.getUser().getLastName() + "<b>"
                    + ",</p>"
                    + "<h1>Your reservation has been confirmed! </h1>"
                    + "<h3>" + reservation.getRoom().getBezeichnung() + "<h3>"
                    + "<h3>" + reservation.getStart_time().format(customFormatter) + " - " + reservation.getEnd_time().format(customFormatter) + "</h3>"
                    + "<p> Room Details: </p>"
                    + "\b"
                    + "<p>"+reservation.getRoom().getRoomDescription()+"</p>"
                    + "<p>Log into your account to see more about your reservations</p>"
                    + "<br>"
                    + "<p>See you soon</p>");
        }
        else {

            msg = ("<p>Hello <b>" + reservation.getUser().getFirstName() + " " + reservation.getUser().getLastName() + "<b>"
                    + ",</p>"
                    + "<p>Your reservation has been confirmed! </p>"
                    + "<h3>" + reservation.getStart_time().format(customFormatter) + " - " + reservation.getEnd_time().format(customFormatter) + "</h3>"
                    + "<p> Workspace Details: </p>"
                    + "\b"
                    + "<p>"+workspaceNameAndDesc+"</p>"
                    + "<p>Log into your account to see more about your reservations</p>"
                    + "<br>"
                    + "<p>See you soon</p>");
        }
        message.setSubject("Your Reservation");
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(msg, "text/html; charset=utf-8");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        message.setContent(multipart);
        Transport.send(message);

    }
}
