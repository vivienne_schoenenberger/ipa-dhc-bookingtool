package com.example.application.view.payment;

import com.example.application.view.calendar.CalendarView;
import com.example.application.entities.Reservation;
import com.example.application.entities.payment.MyPaymentResponse;
import com.example.application.service.ReservationService;
import com.example.application.service.RestClientService;
import com.example.application.view.main.MainMenuView;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.InputStreamFactory;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;

import javax.annotation.security.PermitAll;
import java.io.ByteArrayInputStream;
@CssImport("./styles/twint.css")
@PermitAll
@Route(value = "twint-payment",layout = MainMenuView.class)
@PageTitle("Twint-Payment | Bookingtool")
public class TwintPaymentView extends VerticalLayout implements BeforeEnterObserver, BeforeLeaveObserver {

    Image twint;
    Div twintDiv;
    Paragraph cost;
    Image qrCode;
    Paragraph redirectP;
    Button redirectBtn;
    MyPaymentResponse response;
    ReservationService reservationService;
    Reservation resFromSession;
    Object responseObj;
    Object resObj;
    Image adyenImg;
    Notification notification;
    Div errorDiv;

    public TwintPaymentView(RestClientService restClientService,ReservationService reservationService) {
        this.reservationService = reservationService;
        if (VaadinSession.getCurrent().getSession().getAttribute("payment-response") != null) {

            responseObj = VaadinSession.getCurrent().getSession().getAttribute("payment-response");
            resObj = VaadinSession.getCurrent().getSession().getAttribute("reservation");
            resFromSession = (Reservation) resObj;
            response = (MyPaymentResponse) responseObj;
            if (response.getError() == null) {
                qrCode = new Image();
                qrCode = getQrCodeImage(restClientService);
                qrCode.setId("qr-code");

                twint = new Image("twint_logo.png", "twint logo");
                twint.setId("twint-img");
                twintDiv = new Div();
                twintDiv.setId("black-price-div");
                cost = new Paragraph("CHF " + resFromSession.getCosts() + "0");
                cost.setId("price-p");
                twintDiv.add(cost);
                redirectP = new Paragraph("Redirect to ADYEN");
                redirectP.setId("redirect-p");
                adyenImg = new Image("ADYEN.png", "");
                adyenImg.setId("adyen-img");
                redirectBtn = new Button(adyenImg);
                redirectBtn.setId("redirect-btn");
                redirectBtn.addClickListener(buttonClickEvent -> {
                    UI.getCurrent().getPage().setLocation(getRedirectString(restClientService));
                });

                setId("layout");
                add(twint, twintDiv, qrCode, redirectP, redirectBtn);

            }else{
                notification = new Notification();
                notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
                notification.setPosition(Notification.Position.MIDDLE);
                Button calendarBtn = new Button(new Icon(VaadinIcon.CALENDAR));
                calendarBtn.addClickListener(buttonClickEvent -> {
                   UI.getCurrent().navigate(CalendarView.class);
                });
                errorDiv = new Div(new Text("An Error has occured: "+response.getError().getLocalizedMessage()), calendarBtn);
                errorDiv.getStyle().set("display","flex");
                errorDiv.getStyle().set("flex-direction","column");
                notification.add(errorDiv);
                notification.open();
                reservationService.delete(resFromSession);
            }
        }
    }
    /**
     * Get URL Attribute from Payment Response
     *
     */
    public String getRedirectString (RestClientService restClientService){
        JSONObject redirectURLJson = restClientService.getShortURL(response.getAction().getUrl());
        if(redirectURLJson.get("error")!=null){
            notification = new Notification();
            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
            notification.setPosition(Notification.Position.MIDDLE);
            Button calendarBtn = new Button(new Icon(VaadinIcon.CALENDAR));
            calendarBtn.addClickListener(buttonClickEvent -> {
                UI.getCurrent().navigate(CalendarView.class);
            });
            errorDiv = new Div(new Text("An Error has occured: "+redirectURLJson.getAsString("error")), calendarBtn);
            errorDiv.getStyle().set("display","flex");
            errorDiv.getStyle().set("flex-direction","column");
            notification.add(errorDiv);
            notification.open();
            reservationService.delete(resFromSession);
            return "";
        }
        return redirectURLJson.getAsString("result_url");


    }
    /**
     * Get QR Code and turn byte[] into Image Component
     *
     */
    public Image getQrCodeImage(RestClientService restClientService){
        byte[] imageBytes = restClientService.getQRcode(getRedirectString(restClientService));
        StreamResource resource = new StreamResource("qr.svg", (InputStreamFactory) () -> new ByteArrayInputStream(imageBytes));
        return new Image(resource,"");
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        Notification notification = new Notification();
        notification.setPosition(Notification.Position.MIDDLE);
        H3 title = new H3("Cancel reservation?");
        Paragraph text = new Paragraph("Are you sure you want to cancel and leave this view?");
        Button stayBtn = new Button("Stay");
        stayBtn.setId("notif-stay-btn");
        Button cancelBtn = new Button("Cancel");
        cancelBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                ButtonVariant.LUMO_ERROR);
        Div btnDiv = new Div(stayBtn,cancelBtn);
        btnDiv.setId("notif-btns-div");
        Div info = new Div(title,text,btnDiv);
        HorizontalLayout layout = new HorizontalLayout(info);
        notification.add(layout);
        stayBtn.addClickListener(e-> notification.close());
        BeforeLeaveEvent.ContinueNavigationAction action =
                beforeLeaveEvent.postpone();
        notification.open();
        cancelBtn.addClickListener(e-> {
            reservationService.delete(resFromSession);
            UI.getCurrent().getSession().getSession().setAttribute("reservation", null);
            UI.getCurrent().getSession().getSession().setAttribute("payment-response", null);
            action.proceed();
            notification.close();
        });
    }
    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (VaadinSession.getCurrent().getSession().getAttribute("payment-response") == null ||  VaadinSession.getCurrent().getSession().getAttribute("reservation")==null) {
            beforeEnterEvent.rerouteTo(CalendarView.class);
        }
    }

}
