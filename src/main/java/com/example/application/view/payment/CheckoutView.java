package com.example.application.view.payment;

import com.example.application.view.calendar.CalendarView;
import com.example.application.entities.Reservation;
import com.example.application.entities.Workspace;
import com.example.application.entities.payment.MyPaymentRequest;
import com.example.application.entities.payment.MyPaymentResponse;
import com.example.application.service.ReservationService;
import com.example.application.service.RestClientService;
import com.example.application.view.main.MainMenuView;
import com.example.application.view.main.MainView;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.splitlayout.SplitLayoutVariant;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;

import javax.annotation.security.PermitAll;
import java.time.format.DateTimeFormatter;

@CssImport("./styles/checkout.css")
@PermitAll
@Route(value = "checkout", layout = MainMenuView.class)
@PageTitle("Checkout | Bookingtool")
public class CheckoutView extends Div implements BeforeLeaveObserver, BeforeEnterObserver {
    RestClientService restClientService;
    VerticalLayout reservationLayout;
    VerticalLayout paymentLayout;
    SplitLayout splitLayout;

    H2 title;
    H3 subtitle;
    Div cropImageDiv;
    Paragraph resDatesP;
    Paragraph infosDHC;
    Text infosDHCT;
    Div titleNameObj;
    DateTimeFormatter customFormatter;
    Image reservationImage;
    Paragraph nameOfResObj;
    Paragraph coloredNameResObj;
    Paragraph seats;
    Paragraph description;
    Paragraph location;
    Paragraph cost;
    Div resDatesDiv;
    Div resObjDiv;
    Div reservationDetailsDiv;
    Paragraph telephone;
    Paragraph email;
    RadioButtonGroup<String> radioButtonGroup;
    Div greyBoxDiv;
    Div importantDiv;
    Paragraph importantP;
    Text importantText;
    Paragraph indivCost;
    Paragraph indivDatesP;
    Text mailInfo;
    Div blueResumeDiv;
    H3 paymentSubTitle;
    Paragraph titleResume;
    Div resumeLeftDiv;
    Div resumeRightDiv;
    Paragraph costPerH;
    Paragraph hours;
    Button cancelBtn;
    Button payBtn;
    Paragraph indivOccupiedH;
    Div blueTotalDiv;
    Div resumeDetails;
    Paragraph total;
    Hr dividerTop;
    Paragraph paymentMethods;
    Text paymentExpl;
    Div twintMethodDiv;
    RadioButtonGroup<String> twint;
    Image twintImage;
    Hr dividerBottom;
    Div btns;
    Paragraph indivTotal;
    Boolean paymentBtnClicked;
    ReservationService reservationService;
    Reservation resFromSession;

    public CheckoutView(ReservationService reservationService, RestClientService restClientService) {
        /**
         * Service(s) needed in whole class
         */
        this.restClientService = restClientService;
        this.reservationService = reservationService;
        /**
         * Main Layouts
         */
        reservationLayout = new VerticalLayout();
        reservationLayout.setId("reservation-layout");
        paymentLayout = new VerticalLayout();
        paymentLayout.setId("payment-layout");
        splitLayout = new SplitLayout(reservationLayout, paymentLayout);
        splitLayout.setId("checkout-body");
        splitLayout.addThemeVariants(SplitLayoutVariant.LUMO_MINIMAL);

        /**
         * Getting the reservation object via global session
         *
         */
        if(UI.getCurrent().getSession().getSession().getAttribute("reservation")!=null) {
            Object resObj = VaadinSession.getCurrent().getSession().getAttribute("reservation");
            resFromSession = (Reservation) resObj;

            /**
             * Static content
             * These component will not change their content
             */
            // Reservation Layout:
            title = new H2("Checkout");
            title.setId("checkout-title");
            subtitle = new H3("Ihre reservation");
            subtitle.setId("subtitle-reservation");
            cropImageDiv = new Div();
            cropImageDiv.setId("crop-image-div");
            titleNameObj = new Div();
            titleNameObj.setId("title-obj-div");
            resDatesP = new Paragraph("Dates");
            resDatesP.setClassName("h3-checkout");
            infosDHC = new Paragraph("Opening and Closing hours of DHC");
            infosDHC.setClassName("h3-checkout");
            infosDHCT = new Text("The dhc is accessible from 6am and closes at 8pm.");
            customFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy 'at' hh:mm");
            importantP = new Paragraph("Wichtig");
            importantP.setClassName("h3-checkout");
            importantText = new Text("\n" +
                    "If you have any questions or if you are unable to make the reservation " +
                    "at short notice, please contact us. If you already know that you are " +
                    "prevented and cannot come, we ask you to cancel the reservation online. " +
                    "This is possible up to 24 hours before your appointment and you will " +
                    "be reimbursed for the costs. Unfortunately, we cannot issue a refund " +
                    "after this");
            telephone = new Paragraph("+41 44 801 16 00");
            email = new Paragraph("info@dhc.zuerich");
            radioButtonGroup = new RadioButtonGroup<>();
            radioButtonGroup.setItems("I agree");
            greyBoxDiv = new Div(importantText, telephone, email, radioButtonGroup);
            greyBoxDiv.setId("grey-div");
            importantDiv = new Div(importantP, greyBoxDiv);
            // Payment Layout:
            paymentSubTitle = new H3("Payment");
            paymentSubTitle.setId("subtitle-payment");
            mailInfo = new Text("After completing the payment, you will also receive an email with the statement and reservation details.");
            blueResumeDiv = new Div();
            blueResumeDiv.setId("div-blue-resume");
            titleResume = new Paragraph("Summary");
            titleResume.setId("title-p");

            resumeRightDiv = new Div();
            resumeLeftDiv = new Div();
            costPerH = new Paragraph("Cost per h");
            costPerH.setClassName("white-p");
            hours = new Paragraph("Occupied hours");
            hours.setClassName("white-p");
            total = new Paragraph("Total");
            total.setId("title-total-p");
            dividerTop = new Hr();
            paymentMethods = new Paragraph("Payment Methods");
            paymentMethods.setClassName("h3-checkout");
            paymentExpl = new Text("At the moment there is unfortunately only the possibility to pay with Twint. In the future we will also accept credit card payments.");

            twintMethodDiv = new Div();
            twintMethodDiv.setId("twint-method-div");
            twint = new RadioButtonGroup<>();
            twint.setItems("Twint");
            twint.setId("twint-radio-btn");
            twintImage = new Image("twint_logo.png", "TWINT IMAGE");
            twintImage.setId("twint-image");
            twintMethodDiv.add(twint, twintImage);
            dividerBottom = new Hr();
            btns = new Div();
            resumeLeftDiv.add(costPerH, hours);

            /**
             * Non-static content
             * The content changes (depending on reservation type: room or workspace)
             * The content
             */
            paymentBtnClicked = false;

            if (resFromSession.isOnlyRoom()) {
                reservationImage = new Image(resFromSession.getRoom().getImageName(), "ROOM IMAGE");
                nameOfResObj = new Paragraph("Room ");
                coloredNameResObj = new Paragraph(resFromSession.getRoom().getBezeichnung());
                coloredNameResObj.getStyle().set("color", resFromSession.getColor());
                coloredNameResObj.setClassName("h3-checkout");
                titleNameObj.add(nameOfResObj, coloredNameResObj);
                seats = new Paragraph("Seats available: " + resFromSession.getRoom().getWorkspaceList().size());
                description = new Paragraph("Description: " + resFromSession.getRoom().getRoomDescription());
                location = new Paragraph("Location: " + resFromSession.getRoom().getRoomLocation());
                cost = new Paragraph("Price per h: " + resFromSession.getRoom().getPreis() + "CHF.-");
                indivCost = new Paragraph(resFromSession.getRoom().getPreis() + "CHF.-");


            } else {
                String workspaceNames = "";
                Float accumCosts = 0.00F;
                String workspacesDescription = "Description: ";
                /**
                 * Since a user can choose multiple workspaces
                 * Workspace names, costs and description has to be shown per workspace
                 */
                for (Workspace workspace : resFromSession.getWorkspaceList()) {
                    workspaceNames = workspaceNames.concat(workspace.getBezeichnung() + " ");
                    accumCosts += workspace.getPreis();
                    workspacesDescription = workspacesDescription.concat(workspace.getBezeichnung() + ": " + workspace.getWorkspaceDescription() + "\n " + "        ");
                }
                reservationImage = new Image("default_workspace.jpg", "WORKSPACE IMAGE");
                seats = new Paragraph("Seats available per Workspace: 1");
                nameOfResObj = new Paragraph("Workspace(s) ");
                coloredNameResObj = new Paragraph(workspaceNames);
                titleNameObj.add(nameOfResObj, coloredNameResObj);
                location = new Paragraph("Location: All out workspaces are on the 2. floor");
                description = new Paragraph(workspacesDescription);
                cost = new Paragraph("Price per h for workspace(s): " + accumCosts + "CHF.-");
                indivCost = new Paragraph(accumCosts + "CHF.-");

            }

            /**
             * shared by both reservations types:
             */
            reservationImage.setId("image-checkout");
            nameOfResObj.setClassName("h3-checkout");
            indivCost.setClassName("white-p");
            resDatesDiv = new Div();
            resDatesDiv.setId("res-dates-div");

            indivDatesP = new Paragraph(resFromSession.getStart_time().format(customFormatter) + "\n" + " - " + "\n"
                    + resFromSession.getEnd_time().format(customFormatter));

            resObjDiv = new Div();
            resObjDiv.setId("res-obj-div");
            if (resFromSession.isRecurring()) {
                Paragraph recurringDay = new Paragraph("Reoccurring Day(s): " + resFromSession.getReoccurringDays());
                resDatesDiv.add(recurringDay);
            }
            cropImageDiv.add(reservationImage);
            resDatesDiv.add(resDatesP, indivDatesP, infosDHC, infosDHCT);
            resObjDiv.add(titleNameObj, seats, cost, description, location);
            reservationDetailsDiv = new Div(resDatesDiv, resObjDiv);
            reservationDetailsDiv.setId("res-details-div");

            indivOccupiedH = new Paragraph(String.valueOf(resFromSession.getTotalTime()));
            indivOccupiedH.setClassName("white-p");
            resumeRightDiv.add(indivCost, indivOccupiedH);
            resumeDetails = new Div(resumeLeftDiv, resumeRightDiv);
            resumeDetails.setId("resume-details-div");
            blueResumeDiv.add(titleResume, resumeDetails);

            blueTotalDiv = new Div();
            blueTotalDiv.setId("blue-total-div");

            indivTotal = new Paragraph(resFromSession.getCosts() + "CHF.-");
            indivTotal.setId("total-price-p");
            blueTotalDiv.add(total, indivTotal);


            cancelBtn = new Button("Cancel");
            cancelBtn.setId("cancel-btn");
            cancelBtn.addThemeVariants(ButtonVariant.LUMO_ERROR);
            /**
             * on cancel the reservation object in db
             * and session are deleted
             */
            cancelBtn.addClickListener(e -> {

                UI.getCurrent().navigate(CalendarView.class);
            });

            /**
             * user can only click on pay btn if he accepted
             * agb's and selected payment method
             */
            payBtn = new Button("Pay");
            payBtn.setId("pay-btn");
            payBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
            payBtn.addClickListener(buttonClickEvent -> {
                /**
                 * Implementing API to create a payment response object
                 * This object is saved in a session
                 */
                MyPaymentResponse response = restClientService.createPayment(new MyPaymentRequest(), resFromSession);
                WrappedSession session = VaadinSession.getCurrent().getSession();
                session.setAttribute("payment-response", response);
                paymentBtnClicked = true;
                UI.getCurrent().navigate(TwintPaymentView.class);

            });
            if (radioButtonGroup.getValue() == null || twint.getValue() == null)
                payBtn.setEnabled(false);
            else payBtn.setEnabled(true);
            radioButtonGroup.addValueChangeListener(e -> {
                if (radioButtonGroup.getValue() == null || twint.getValue() == null)
                    payBtn.setEnabled(false);
                else payBtn.setEnabled(true);
            });
            twint.addValueChangeListener(e -> {
                if (radioButtonGroup.getValue() == null || twint.getValue() == null)
                    payBtn.setEnabled(false);
                else payBtn.setEnabled(true);
            });


            btns.add(cancelBtn, payBtn);
            btns.setId("buttons-div");

            /**
             * add everything to main layouts
             */

            paymentLayout.add(paymentSubTitle, mailInfo, blueResumeDiv, blueTotalDiv, dividerTop, paymentMethods, paymentExpl, twintMethodDiv, dividerBottom, dividerBottom, btns);
            reservationLayout.add(title, subtitle, cropImageDiv, reservationDetailsDiv, importantDiv);

            add(splitLayout);
        }
    }
    /**
     * beforeLeave event is triggered when the user
     * tries to navigate to a different page.
     * A cancel pop up open and asks the user to confirm cancellation
     */
    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        BeforeLeaveEvent.ContinueNavigationAction action =
                beforeLeaveEvent.postpone();
        if (!paymentBtnClicked) {
            Notification notification = new Notification();
            notification.setPosition(Notification.Position.MIDDLE);
            H3 title = new H3("Cancel reservation?");
            Paragraph text = new Paragraph("Are you sure you want to cancel and return to the calendar view?");
            Button stayBtn = new Button("Stay");
            stayBtn.setId("notif-stay-btn");
            Button cancelBtn = new Button("Cancel");
            cancelBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                    ButtonVariant.LUMO_ERROR);
            Div btnDiv = new Div(stayBtn, cancelBtn);
            btnDiv.setId("notif-btns-div");
            Div info = new Div(title, text, btnDiv);
            HorizontalLayout layout = new HorizontalLayout(info);
            notification.add(layout);
            stayBtn.addClickListener(e -> notification.close());
            notification.open();
            cancelBtn.addClickListener(e -> {
                action.proceed();
                notification.close();
                reservationService.delete(resFromSession);
                UI.getCurrent().getSession().getSession().setAttribute("reservation", null);
            });
        } else action.proceed();
    }
    /**
     * beforeEnter event is triggered when
     * user navigates to this page (after constructor is loaded). The user can't navigate
     * here unless there is a attribute for reservation in current session
     */
    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (VaadinSession.getCurrent().getSession().getAttribute("reservation") == null) {
            beforeEnterEvent.rerouteTo(MainView.class);
        }
    }
}
