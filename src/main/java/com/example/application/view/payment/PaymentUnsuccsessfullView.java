package com.example.application.view.payment;

import com.example.application.view.calendar.CalendarView;
import com.example.application.entities.Reservation;
import com.example.application.entities.payment.MyPaymentRequest;
import com.example.application.entities.payment.MyPaymentResponse;
import com.example.application.service.ReservationService;
import com.example.application.service.RestClientService;
import com.example.application.view.main.MainMenuView;
import com.example.application.view.main.MainView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;

import javax.annotation.security.PermitAll;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@CssImport("./styles/payment-unsuccessfull.css")
@PermitAll
@Route(value = "payment-unsuccessfull",layout = MainMenuView.class)
@PageTitle("Unsuccessfull | Bookingtool")
public class PaymentUnsuccsessfullView extends VerticalLayout implements HasUrlParameter<String>,BeforeLeaveObserver, BeforeEnterObserver {
    Long reservationId;
    ReservationService reservationService;
    Image reservationImage;
    H2 paymentTitle;
    Icon unsuccessfullIcon;
    Div content;
    Paragraph detailTitle;
    Paragraph btnDescription;
    Button tryAgainBtn;
    Button backToCalendarBtn;
    Div bottomContent;
    RestClientService restClientService;
    Reservation reservation;
    String status;
    Boolean retryBtnClicked;


    public PaymentUnsuccsessfullView(ReservationService reservationService, RestClientService restClientService) {
        this.reservationService = reservationService;
        this.restClientService= restClientService;
        unsuccessfullIcon = new Icon("lumo","error");
        unsuccessfullIcon.setId("icon");
        unsuccessfullIcon.setId("icon");

        btnDescription= new Paragraph("Go back to the calendar or retry payment");
        btnDescription.setId("btn-p");
        backToCalendarBtn = new Button(new Icon(VaadinIcon.CALENDAR));
        backToCalendarBtn.addThemeVariants();
        backToCalendarBtn.setClassName("un-btn");
        detailTitle =new Paragraph("Payment was unsuccessfull");
        detailTitle.setId("h3-details-title-unsuccessfull");
        tryAgainBtn = new Button(new Icon(VaadinIcon.CART));
        tryAgainBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        tryAgainBtn.setClassName("un-btn");
        bottomContent = new Div();
        retryBtnClicked= false;

    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter String parameter) {
        Location location = beforeEvent.getLocation();
        QueryParameters queryParameters = location
                .getQueryParameters();

        Map<String, List<String>> parametersMap =
                queryParameters.getParameters();

        reservationId = Long.valueOf(parametersMap.get("id").get(0));
        status = parametersMap.get("status").get(0);
        reservation= reservationService.findReservationById(reservationId);
        paymentTitle = new H2("Payment "+status+"!");
        paymentTitle.setId("payment-title");
        if(reservation.isOnlyRoom()){
            reservationImage = new Image(reservation.getRoom().getImageName(),"");
            reservationImage.setId("reservation-img");
        }
        else{
            reservationImage = new Image("default_workspace.jpg","");
            reservationImage.setId("reservation-img");

        }

        Div btnDiv = new Div(backToCalendarBtn,tryAgainBtn);
        backToCalendarBtn.addClickListener(buttonClickEvent -> {
            UI.getCurrent().navigate(CalendarView.class);
            reservationService.delete(reservation);
        });
        tryAgainBtn.addClickListener(buttonClickEvent -> {
            retryBtnClicked= true;
            MyPaymentResponse response = restClientService.createPayment(new MyPaymentRequest(),reservation);
            WrappedSession session = VaadinSession.getCurrent().getSession();
            session.setAttribute("payment-response",response);
            UI.getCurrent().navigate(TwintPaymentView.class);

        });
        btnDiv.setId("btn-div-un");
        setId("layout");
        bottomContent.add(btnDescription,btnDiv);
        content= new Div(reservationImage,detailTitle,bottomContent);
        content.setId("content-div-unsuccessfull");

        add(unsuccessfullIcon,paymentTitle,content);


    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        String status = (String) VaadinSession.getCurrent().getSession().getAttribute("payment-status");
        if ( (status == null|| !(status.equals("Cancelled") || status.equals("Error") || status.equals("Refused"))) && !Objects.equals(reservation.getPaymentStatus(), "false")) {
            UI.getCurrent().getPage().setLocation("https://localhost:8443");
            beforeEnterEvent.rerouteTo(MainView.class);
        }
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        BeforeLeaveEvent.ContinueNavigationAction action =
                beforeLeaveEvent.postpone();
        if(!retryBtnClicked) {
            Notification notification = new Notification();
            notification.setPosition(Notification.Position.MIDDLE);
            H3 title = new H3("Cancel reservation?");
            Paragraph text = new Paragraph("Are you sure you want to cancel and return to the calendar view?");
            Button stayBtn = new Button("Stay");
            stayBtn.setId("notif-stay-btn");
            Button cancelBtn = new Button("Cancel");
            cancelBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                    ButtonVariant.LUMO_ERROR);
            Div btnDiv = new Div(stayBtn, cancelBtn);
            btnDiv.setId("notif-btns-div");
            Div info = new Div(title, text, btnDiv);
            HorizontalLayout layout = new HorizontalLayout(info);
            notification.add(layout);
            stayBtn.addClickListener(e -> notification.close());
            notification.open();
            cancelBtn.addClickListener(e -> {
                action.proceed();
                notification.close();
                reservationService.delete(reservation);
                UI.getCurrent().getSession().getSession().setAttribute("reservation", null);
                VaadinSession.getCurrent().getSession().setAttribute("payment-status", null);
            });
        }else action.proceed();

    }
}
