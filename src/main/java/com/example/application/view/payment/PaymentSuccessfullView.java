package com.example.application.view.payment;

import com.example.application.view.calendar.CalendarView;
import com.example.application.entities.Reservation;
import com.example.application.entities.Workspace;
import com.example.application.service.ReservationService;
import com.example.application.view.main.MainMenuView;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.*;

import javax.annotation.security.PermitAll;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@CssImport("./styles/payment-successfull.css")
@PermitAll
@PageTitle("Successfull | Bookingtool")
@Route(value = "/payment-successfull", layout = MainMenuView.class)
public class PaymentSuccessfullView extends VerticalLayout implements HasUrlParameter<String>,BeforeEnterObserver, BeforeLeaveObserver {
    Long reservationId;
    ReservationService reservationService;
    Image reservationImage;
    H2 paymentTitle;
    Icon successfullIcon;
    Text paymentText;
    Div content;
    Paragraph detailTitle;
    Accordion accordion;
    Paragraph btnDescription;
    Button backToCalendarBtn;
    Paragraph untilParagraph;
    Div untilDiv;
    DateTimeFormatter customFormatter;
    Paragraph workspaceDescription;
    Reservation reservation;
    private String updateStatus;
    private String upadateReservationId;
    Map<String, List<String>> parametersMap;
    public PaymentSuccessfullView(ReservationService reservationService) {
        this.reservationService = reservationService;
        successfullIcon = new Icon(VaadinIcon.CHECK_CIRCLE);
        successfullIcon.setId("icon");
        successfullIcon.setId("icon-success");
        paymentTitle = new H2("Payment complete!");
        paymentTitle.setId("payment-title-success");
        btnDescription= new Paragraph("View the reservation in the calendar now");
        btnDescription.setId("btn-p");
        backToCalendarBtn = new Button(new Icon(VaadinIcon.CALENDAR));
        backToCalendarBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        backToCalendarBtn.setId("btn");
        detailTitle= new Paragraph("Details");
        detailTitle.setId("h3-details-title");
        untilDiv = new Div();
        untilDiv.setId("text-div");
        Paragraph dateTitle = new Paragraph("We will see us at");
        dateTitle.setClassName("h3-title");
        untilDiv.add(dateTitle);
        customFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy 'at' hh:mm");
        UI.getCurrent().add(new Paragraph());
    }
    /**
     * Getting reservation id from url and set details
     */
    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter String parameter) {
        Location location = beforeEvent.getLocation();
        QueryParameters queryParameters = location
                .getQueryParameters();
         parametersMap =
                queryParameters.getParameters();
        reservationId = Long.valueOf(parametersMap.get("id").get(0));

        reservation= reservationService.findReservationById(reservationId);

        reservationService.update(reservation);
        Div infoDiv = new Div();
        paymentText= new Text("A confirmation email was sent to the following email address: "+reservation.getUser().getEmail());
        infoDiv.add(paymentText);
        infoDiv.setId("email-info-div");
        untilParagraph = new Paragraph(reservation.getStart_time().format(customFormatter));
        untilParagraph.setClassName("h3-title");
        untilDiv.add(untilParagraph);
        accordion = new Accordion();
        accordion.setId("accordion");

        if(!reservation.isRecurring()){
            Span dates = new Span(reservation.getStart_time().format(customFormatter) +" - "+reservation.getEnd_time().format(customFormatter));
            accordion.add("Dates",dates);
        }
        else{
            Paragraph dates = new Paragraph(reservation.getStart_time().format(customFormatter) +" - "+reservation.getEnd_time().format(customFormatter));
            Paragraph days = new Paragraph("Every: " +reservation.getReoccurringDays());
            Span dateWithRecDay = new Span(dates,days);
            accordion.add("Dates",dateWithRecDay);
        }
        if(reservation.isOnlyRoom()){
            reservationImage = new Image(reservation.getRoom().getImageName(),"");
            reservationImage.setId("reservation-img");
            Span room = new Span();
            Paragraph roomTitle = new Paragraph(reservation.getRoom().getBezeichnung());
            Paragraph roomDescription = new Paragraph(reservation.getRoom().getRoomDescription());
            Paragraph roomLocation = new Paragraph(reservation.getRoom().getRoomLocation());
            room.add(roomTitle,roomDescription,roomLocation);
            accordion.add("Room",room);
        }
        else{
            reservationImage = new Image("default_workspace.jpg","");
            reservationImage.setId("reservation-img");
            Span workspaces = new Span();
            for(Workspace workspace: reservation.getWorkspaceList()){
               workspaceDescription= new Paragraph(workspace.getBezeichnung()+": "+ workspace.getWorkspaceDescription());
                workspaces.add(workspaceDescription);
            }
            accordion.add("Workspace(s)",workspaces);
        }
        Div btnDiv = new Div(backToCalendarBtn);
        backToCalendarBtn.addClickListener(buttonClickEvent -> UI.getCurrent().navigate(CalendarView.class));
        btnDiv.setId("btn-div");
        Span costs = new Span(String.valueOf(reservation.getCosts()+"CHF.-"));
        accordion.add("Payment",costs);
        setId("layout");
        content= new Div(reservationImage,untilDiv,detailTitle,accordion,btnDescription,btnDiv);
        content.setId("content-div");
        add(successfullIcon,paymentTitle,paymentText,content);
    }
    /**
     * check if session attributes exist
     */
    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        VaadinSession.getCurrent().getSession();
        String status = (String) VaadinSession.getCurrent().getSession().getAttribute("payment-status");
        if ((status ==null || !status.equals("Authorised")) && !Objects.equals(reservation.getPaymentStatus(), "true")) {
            UI.getCurrent().getPage().setLocation("https://localhost:8443");
        }else{
            reservation.setPaymentStatus("completed");
        }
    }
    /**
     * Delete global session attributes
     */
    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        VaadinSession.getCurrent().getSession().setAttribute("payment-status",null);
        VaadinSession.getCurrent().getSession().setAttribute("reservation",null);
    }
}
