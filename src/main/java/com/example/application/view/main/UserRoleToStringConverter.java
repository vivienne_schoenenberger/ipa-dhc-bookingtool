package com.example.application.view.main;

import com.example.application.entities.UserRole;
import com.vaadin.flow.data.binder.Result;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.Converter;
import org.springframework.core.convert.ConversionException;

public class UserRoleToStringConverter implements Converter<String, UserRole> {

    @Override
    public Result<UserRole> convertToModel(String s, ValueContext valueContext) {
        return null;

    }

    @Override
    public String convertToPresentation(UserRole userRole, ValueContext valueContext)
               throws ConversionException {
            if (userRole == null) {
                return null;
            }
            return userRole.getBezeichnung();
    }
}
