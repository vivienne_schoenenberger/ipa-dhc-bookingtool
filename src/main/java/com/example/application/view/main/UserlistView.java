package com.example.application.view.main;

import com.example.application.entities.ApplicationUser;
import com.example.application.entities.UserRole;
import com.example.application.service.LazyDataProvider.UserDataProvider;
import com.example.application.service.UserService;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.crud.BinderCrudEditor;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.crud.CrudEditor;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.security.PermitAll;
import java.util.*;


@PermitAll
@Route(value = "Users", layout = MainMenuView.class)
@PageTitle("Users | Bookingtool")

public class UserlistView extends VerticalLayout {

    Crud<ApplicationUser> crud;
    private boolean enableUsernameValidation;

    TextField username;
    @Autowired
    private UserService userService;

    public UserlistView(UserService userService) {
        crud = new Crud<>(
                ApplicationUser.class,
                createEditor()

        );
        setupGrid();
        Span s = new Span("All new users will have \"defaultPassword\" as default password!");
        s.getElement().getStyle().set("font-size", "13px").set("color","#ed9e8c");
        add(s);
        add(
                new H1("All users"),
                crud,
                s

        );

        UserDataProvider userDataProvider = new UserDataProvider(userService);

        crud.setDataProvider(userDataProvider);

        crud.addDeleteListener(deleteEvent ->
                userDataProvider.delete(deleteEvent.getItem())
        );
        crud.addSaveListener(saveEvent ->
                userDataProvider.persist(saveEvent.getItem())
        );

    }


    private void setupGrid() {
        Grid<ApplicationUser> grid = crud.getGrid();

        // Only show these columns (all columns shown by default):
        String USERNAME = "username";
        String FIRST_NAME = "firstName";
        String LAST_NAME = "lastName";
        String EMAIL = "email";
        String USERROLE = "userRole";
        String EDIT_COLUMN = "vaadin-crud-edit-column";
        List<String> visibleColumns = Arrays.asList(
                USERNAME,
                FIRST_NAME,
                LAST_NAME,
                EMAIL,
                USERROLE,
                EDIT_COLUMN
        );
        grid.getColumns().forEach(column -> {
            String key = column.getKey();
            if (!visibleColumns.contains(key)) {
                grid.removeColumn(column);
            }
        });
        // Reorder the columns (alphabetical by default)
        grid.setColumnOrder(
                grid.getColumnByKey(USERNAME),
                grid.getColumnByKey(FIRST_NAME),
                grid.getColumnByKey(LAST_NAME),
                grid.getColumnByKey(EMAIL),
                grid.getColumnByKey(USERROLE),
                grid.getColumnByKey(EDIT_COLUMN)
        );


    }

    private CrudEditor<ApplicationUser> createEditor() {

        List<UserRole> userRoleList = new ArrayList<>();
        userRoleList.add(new UserRole(1L, "Admin"));
        userRoleList.add(new UserRole(2L, "Mitglied"));
        userRoleList.add(new UserRole(3L, "Kunde"));
        username = new TextField("Username");
        TextField firstName = new TextField("First name");
        TextField lastName = new TextField("Last name");
        EmailField email = new EmailField("Email");
        ComboBox<UserRole> userRole = new ComboBox<>("User Role");
        userRole.setItems(userRoleList);
        userRole.setItemLabelGenerator(UserRole::getBezeichnung);

        FormLayout form = new FormLayout(username,firstName, lastName, email, userRole);
        form.setColspan(email, 2);
        form.setColspan(userRole, 2);
        form.setMaxWidth("480px");
        form.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("30em", 2)
        );




        Binder<ApplicationUser> binder = new Binder<>(ApplicationUser.class);

        username.addValueChangeListener(e ->{
            enableUsernameValidation = true;
            //binder.validate();
        });

        binder.forField(username).asRequired().withValidator(this::checkIfUserUnique).bind(ApplicationUser::getUsername,ApplicationUser::setUsername).setAsRequiredEnabled(true);
        binder.forField(firstName).asRequired().bind(ApplicationUser::getFirstName, ApplicationUser::setFirstName).setAsRequiredEnabled(true);
        binder.forField(lastName).asRequired().bind(ApplicationUser::getLastName, ApplicationUser::setLastName).setAsRequiredEnabled(true);
        binder.forField(email).asRequired().bind(ApplicationUser::getEmail, ApplicationUser::setEmail).setAsRequiredEnabled(true);
        binder.forField(userRole).asRequired().bind((ApplicationUser::getUserRole), ApplicationUser::setUserRole).setAsRequiredEnabled(true);

        return new BinderCrudEditor<>(binder, form);

    }

    private ValidationResult checkIfUserUnique(String s, ValueContext valueContext) {
        if (!enableUsernameValidation) {
            // user hasn't visited the field yet, so don't validate just yet, but next time.
            enableUsernameValidation = true;
            return ValidationResult.ok();
        }

        if ( username.getValue().equals("") ){
            return ValidationResult.error("Username can't be empty");
        }
        if ( !userService.checkIfUsernameExists(username.getValue())){
            return ValidationResult.ok();
        }
        return ValidationResult.error("Desired username already assigned" );
    }


}

