package com.example.application.view.main;

import com.example.application.service.LazyDataProvider.UserSpecReservationProvider;
import com.example.application.entities.ApplicationUser;
import com.example.application.entities.Reservation;
import com.example.application.security.MyUserPrincipal;
import com.example.application.service.ReservationService;
import com.example.application.service.UserService;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.avatar.AvatarVariant;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.crud.BinderCrudEditor;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.crud.CrudEditor;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


import javax.annotation.security.PermitAll;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@PermitAll
@Route(value = "Account",layout = MainMenuView.class)
@PageTitle("Account | Bookingtool")
public class AccountView extends HorizontalLayout {

    H2 loggedInUser;
    private File file;
    private String originalFileName;
    private String mimeType;
    Crud<Reservation> crud;
    StreamResource streamResource;
    H3 userDetailTitle;
    Paragraph userName;
    Paragraph userMail;
    Paragraph userRole;
    Avatar profileAvatar;

    public AccountView(UserService userService, ReservationService reservationService) {

        crud = new Crud<>(
                Reservation.class,
                createEditor()
        );
        setupGrid();
        crud.setToolbarVisible(false);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        MyUserPrincipal customUser = (MyUserPrincipal ) authentication.getPrincipal();
        ApplicationUser currentUser = userService.findUserbyUsername(customUser.getUsername());

        if (getImageStreamIfAvailable(currentUser)!=null){
            streamResource = getImageStreamIfAvailable(currentUser);
            profileAvatar = new Avatar();
            profileAvatar.setImageResource(streamResource);

        }
        else{
            profileAvatar = new Avatar(currentUser.getFirstName()+" "+currentUser.getLastName());

        }

        TextField imageName = new TextField("");
        Upload upload = new Upload(this::receiveUpload);
        upload.setDropAllowed(false);
        upload.setId("upload-btn-account");
        upload.setAcceptedFileTypes(".jpg",".png",".jpeg","image/jpeg");
        Button uploadBtn = new Button(new Icon(VaadinIcon.UPLOAD));
        uploadBtn.addThemeVariants(ButtonVariant.LUMO_SMALL);
        upload.setUploadButton(uploadBtn);
        upload.addSucceededListener(event -> {
            upload.setAutoUpload(true);
            imageName.setValue(originalFileName);
            currentUser.setProfileImagePath(originalFileName);
            userService.update(currentUser);
            streamResource = getImageStreamIfAvailable(currentUser);
            profileAvatar.setImageResource(streamResource);

        });

        Button deleteProfilePic = new Button(new Icon(VaadinIcon.CLOSE));
        deleteProfilePic.addThemeVariants(ButtonVariant.LUMO_SMALL);
        deleteProfilePic.addClickListener(buttonClickEvent -> {
            if (getImageStreamIfAvailable(currentUser)!=null){
                deleteImageIfAvailable(currentUser);
                currentUser.setProfileImagePath(null);
                userService.update(currentUser);
                profileAvatar = new Avatar(currentUser.getFirstName()+" "+currentUser.getLastName());
            }
        });


        profileAvatar.setId("uploaded-prof-pic");
        profileAvatar.addThemeVariants(AvatarVariant.LUMO_LARGE);
        VerticalLayout accountInfo = new VerticalLayout();
        Div editBtns = new Div();
        editBtns.setId("edit-btns-account");
        editBtns.add(upload,deleteProfilePic);
        Accordion editBtnsAccordion = new Accordion();
        editBtnsAccordion.add("edit avatar",editBtns);
        editBtnsAccordion.close();
        //Userdetails
        Div userDetails = new Div();
        loggedInUser= new H2("Hello "+ currentPrincipalName +" !:)");
        loggedInUser.setId("title-logged-user");

        userDetailTitle = new H3("Account info");
        userName = new Paragraph(currentUser.getFirstName()+" "+currentUser.getLastName());
        userMail = new Paragraph(currentUser.getEmail());
        userRole = new Paragraph(currentUser.getUserRole().getBezeichnung());
        userDetails.add(loggedInUser,userDetailTitle,userName,userMail,userRole);
        userDetails.setId("div-user-details-account");
        Div profileInfoDiv = new Div();
        profileInfoDiv.add(profileAvatar,loggedInUser);
        profileInfoDiv.setId("div-profile-info-acc");
        accountInfo.add(profileInfoDiv,editBtnsAccordion,userDetails);
        accountInfo.getStyle().set("background-color","#F4F4FA");
        accountInfo.setId("account-info");
        VerticalLayout yourReservations = new VerticalLayout();
        yourReservations.add( new H1("Your Reservations"),crud);
        SplitLayout splitLayout = new SplitLayout(yourReservations,accountInfo);
        splitLayout.setSplitterPosition(70);
        setHeightFull();
        setWidthFull();
        splitLayout.setHeightFull();
        splitLayout.setWidthFull();
        add(splitLayout);

        UserSpecReservationProvider userSpecReservationProvider = new UserSpecReservationProvider(reservationService,currentUser);
        crud.setDataProvider(userSpecReservationProvider);

        crud.addDeleteListener(reservationDeleteEvent->
                userSpecReservationProvider.delete(reservationDeleteEvent.getItem())
        );
        crud.addSaveListener(reservationSaveEvent ->
                userSpecReservationProvider.persist(reservationSaveEvent.getItem())
        );
    }

    private CrudEditor<Reservation> createEditor(){
        TextField title = new TextField("Title");
        TextArea textArea = new TextArea("Description");

        FormLayout formLayout = new FormLayout(title,textArea);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("30em", 2)
        );

        Binder<Reservation> binder = new Binder<>(Reservation.class);
        binder.forField(title).asRequired().bind(Reservation::getTitle, Reservation::setTitle).setAsRequiredEnabled(true);
        binder.forField(textArea).asRequired().bind(Reservation::getDescription, Reservation::setDescription).setAsRequiredEnabled(true);

        return new BinderCrudEditor<>(binder, formLayout);
    }

    private void setupGrid(){
        Grid<Reservation> grid = crud.getGrid();

        String TITLE = "title";
        String DESCRIPTION = "description";
        String ROOM = "room";
        String WORKSPACE = "workspaceList";
        String START = "start_time";
        String END = "end_time";
        String EDIT_COLUMN = "vaadin-crud-edit-column";
        List<String> visibleColumns = Arrays.asList(
                TITLE,
                DESCRIPTION,
                ROOM,
                WORKSPACE,
                START,
                END,
                EDIT_COLUMN
        );
        grid.getColumns().forEach(column -> {
            String key = column.getKey();
            if (!visibleColumns.contains(key)) {
                grid.removeColumn(column);
            }
        });
        // Reorder the columns (alphabetical by default)
        grid.setColumnOrder(
                grid.getColumnByKey(TITLE),
                grid.getColumnByKey(DESCRIPTION),
                grid.getColumnByKey(ROOM),
                grid.getColumnByKey(WORKSPACE),
                grid.getColumnByKey(START),
                grid.getColumnByKey(END),
                grid.getColumnByKey(EDIT_COLUMN)
        );
    }
    public OutputStream receiveUpload(String originalFileName, String MIMEType) {

        this.originalFileName = originalFileName;
        this.mimeType = MIMEType;
        try {

            // Create a temporary file for example, you can provide your file here.
            this.file = new File("./src/main/resources/public/"+originalFileName);
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Failed to create InputStream for: '" + this.file.getAbsolutePath(), e);
        }

        return null;
    }
    @SneakyThrows
    public StreamResource getImageStreamIfAvailable(ApplicationUser user) {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select profile_image_path from bookingtool.application_user where id ='" + user.getId() +"'" ;
        ResultSet rs = statement.executeQuery(sql);
        String imageName = "";
        while (rs.next()) {
            imageName = rs.getString("profile_image_path");
        }
        String finalImageName = imageName;
        if(finalImageName!=null && !finalImageName.equals("")) {
            StreamResource imageResource = new StreamResource(finalImageName,
                    () -> getClass().getResourceAsStream("/public/" + finalImageName));
            c.close();
            return imageResource;
        }
        else {
            c.close();
            return null;
        }

    }
    @SneakyThrows
    public void deleteImageIfAvailable(ApplicationUser user) {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select profile_image_path from bookingtool.application_user where id ='" + user.getId() +"'" ;
        ResultSet rs = statement.executeQuery(sql);
        String imageName = "";
        while (rs.next()) {
            imageName = rs.getString("profile_image_path");
        }
        String finalImageName = imageName;
        if(finalImageName!=null && !finalImageName.equals("")) {
            Files.delete(Path.of("src/main/resources/public/" +finalImageName));

        }
        c.close();
    }
}
