package com.example.application.view.main;

import com.example.application.entities.ApplicationUser;
import com.example.application.entities.UserRole;
import com.example.application.security.MyUserPrincipal;
import com.example.application.service.UserService;
import com.example.application.view.calendar.CalendarView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.security.PermitAll;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@PermitAll
@Route(value = "",layout = MainMenuView.class)
@PageTitle("Menu | Bookingtool")
public class MainView extends HorizontalLayout{
    Long applicationUserId;

    public MainView(UserService userService){
        applicationUserId=getCurrenUserId(userService);
        setClassName("main-page-hLayout");
        Button calendarButton = new Button("Calendar");
        calendarButton.addClickListener(e->{
            UI.getCurrent().navigate(CalendarView.class);
        });
        Button workspacesButton = new Button("Workspaces");
        workspacesButton.addClickListener(e->{
            UI.getCurrent().navigate(WorkspaceList.class);
        });
        Button accountButton = new Button("Your Account");
        accountButton.addClickListener(e->{
            UI.getCurrent().navigate(AccountView.class);
        });
        Button mapButton = new Button("Map");
        accountButton.addClickListener(e->{
            UI.getCurrent().navigate(MapView.class);
        });

        calendarButton.setClassName("cal-btn");
        workspacesButton.setClassName("work-btn");
        accountButton.setClassName("acc-btn");
        mapButton.setClassName("map-btn");

        Icon calendarIcon = new Icon("calendar");
        Icon workspaceIcon = new Icon("grid-small");
        Icon accountIcon = new Icon("user");
        Icon mapIcon = new Icon(VaadinIcon.MAP_MARKER);

        calendarIcon.setClassName("icon-calDiv");
        workspaceIcon.setClassName("icon-workDiv");
        accountIcon.setClassName("icon-accDiv");
        mapIcon.setClassName("icon-mapDiv");

        H2 calendarH2= new H2("Calendar View");
        H2 workspaceH2= new H2("Workspaces");
        H2 accountH2= new H2("Your Account");
        H2 mapH2= new H2("Map View");

        calendarH2.setClassName("h2-calDiv");
        workspaceH2.setClassName("h2-workDiv");
        accountH2.setClassName("h2-accDiv");
        mapH2.setClassName("h2-mapDiv");

        Div calendarDiv = new Div();
        Div workspacesDiv = new Div();
        Div accountDiv = new Div();
        Div mapDiv = new Div();
        calendarDiv.setClassName("main-menu-calDiv");
        accountDiv.setClassName("main-menu-accDiv");
        workspacesDiv.setClassName("main-menu-workDiv");
        mapDiv.setClassName("main-menu-mapDiv");

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addClassName("main-menu-vertical");

        calendarDiv.add(calendarH2,calendarIcon,calendarButton);
        workspacesDiv.add(workspaceH2,workspaceIcon,workspacesButton);
        accountDiv.add(accountH2,accountIcon,accountButton);
        mapDiv.add(mapH2,mapIcon,mapButton);


        HorizontalLayout horizontalLayout = new HorizontalLayout(calendarDiv,workspacesDiv,accountDiv);
        horizontalLayout.setClassName("main-menu-horizontalLayout");
        setHeightFull();
        setWidthFull();
        add(verticalLayout);
        verticalLayout.getStyle().set("filter","8px");
        verticalLayout.add(horizontalLayout);
        sessionInit(VaadinSession.getCurrent());

        /**
         * Adding a session destroy listener to delete pending reservation
         *
         */
        VaadinSession.getCurrent().getService().addSessionDestroyListener(e -> {
            try {
                deleteTempData();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
    }
    /**
     * Initialize global session that lasts 65 sec until invalidation
     * and add Session expired message
     *
     */
    public void sessionInit(VaadinSession vaadinSession) {
        vaadinSession.getSession().setMaxInactiveInterval(180);
        vaadinSession.getService().setSystemMessagesProvider(
                (SystemMessagesProvider) systemMessagesInfo -> {
                    CustomizedSystemMessages msgs = new CustomizedSystemMessages();
                    msgs.setSessionExpiredMessage("Reload Page or click notification");
                    msgs.setSessionExpiredCaption("Session has expired");
                    msgs.setSessionExpiredNotificationEnabled(true);
                    msgs.setSessionExpiredURL("/login");
                    return msgs;
                });
    }
    public Long getCurrenUserId (UserService userService)  {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserPrincipal customUser = (MyUserPrincipal ) authentication.getPrincipal();
        ApplicationUser currentUser = userService.findUserbyUsername(customUser.getUsername());
        return currentUser.getId();
    }

    /**
     * Delete all temporary pending data in db
     *
     */
    public void deleteTempData() throws SQLException {
        String sql ="";
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "mysecretpassword");
        List<Long> reservationIdList = new ArrayList<>();
        Statement getIds = c.createStatement();
        String sql1 = "select * from bookingtool.reservation where payment_status ='pending' and user_id="+applicationUserId;
        ResultSet rs =getIds.executeQuery(sql1);
        while ( rs.next() )
        {
            reservationIdList.add(rs.getLong("id"));
        }
        for(Long resId:reservationIdList){
            Statement deleteEntryIds = c.createStatement();
            String sql2 = "delete from bookingtool.reservation_entry_ids where reservation_id = "+resId;
            deleteEntryIds.executeUpdate(sql2);
        }

        for(Long resId:reservationIdList){
            Statement deleteEntryIds = c.createStatement();
            String sql3 = "delete from bookingtool.reservation_res_entries where reservation_id = "+resId;
            deleteEntryIds.executeUpdate(sql3);
        }

        Statement statement = c.createStatement();
        sql = "delete from bookingtool.reservation where payment_status ='pending'and user_id="+applicationUserId;
        statement.executeUpdate(sql);

        c.close();
    }
}
