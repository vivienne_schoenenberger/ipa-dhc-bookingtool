package com.example.application.view.main;

import com.example.application.view.calendar.CalendarView;
import com.example.application.entities.ApplicationUser;
import com.example.application.entities.UserRole;
import com.example.application.security.MyUserPrincipal;
import com.example.application.security.SecurityService;

import com.example.application.service.UserService;
import com.vaadin.flow.component.*;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabVariant;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.StreamResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.security.PermitAll;

@PermitAll
public class MainMenuView extends AppLayout {

    private H1 viewTitle;

    private Tabs views;
    private Tabs tertieryViews;

    public Tab current;
    private Tabs subViews;
    private Tabs tertierTabs;

    public MainMenuView(SecurityService securityService, UserService userService)  {

        DrawerToggle toggle = new DrawerToggle();
        toggle.setClassName("main-toggle");

        H1 appTitle = new H1("Booking-Tool");
        appTitle.setClassName("app-title");
        StreamResource imageResource = new StreamResource("orginal_logo_dhc_weiss.png",
                () -> getClass().getResourceAsStream("/public/orginal_logo_dhc_weiss.png"));

        Image dhcLogo = new Image(imageResource,"DHC Logo");
        dhcLogo.setClassName("dhc-logo-weiss");
        views = getPrimaryNavigation();
        tertieryViews = getTertieryyNavigation();
        // Use the drawer for the menu
        //DrawerToggle toggle = new DrawerToggle();
        //
        H2 viewTitle = new H2("");
        viewTitle.getStyle()
                .set("font-size", "var(--lumo-font-size-l)")
                .set("margin", "0");


        HorizontalLayout wrapper = new HorizontalLayout(viewTitle);
        wrapper.setAlignItems(FlexComponent.Alignment.CENTER);
        wrapper.setSpacing(false);
        VerticalLayout viewHeader = new VerticalLayout(wrapper, getSecondaryNavigation(userService));
        viewHeader.getStyle().set("background-color","rgb(244, 244, 250)");
        addToNavbar(toggle);

        views.addSelectedChangeListener(selectedChangeEvent -> {

            views.getSelectedTab();
            if (views.getSelectedTab().getLabel().equals("Account")) {

                //addToDrawer(tertiaryMenu);
                viewHeader.setPadding(false);
                viewHeader.setSpacing(false);
                remove(viewHeader);
                addToNavbar(viewHeader);



            } else {
                remove(viewHeader);
                //remove(tertiaryMenu);
            }


        });
        Button logoutBtn = new Button("Logout");
        logoutBtn.addClickListener(buttonClickEvent -> {
           securityService.logout();
        });
        logoutBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        logoutBtn.setId("logout-btn");
        addToDrawer(dhcLogo,appTitle, views,logoutBtn);
        setPrimarySection(Section.DRAWER);
    }


    private static Tab createTab(String label, String text, Class<? extends Component> navigationTarget, VaadinIcon icon) {
        final Tab tab = new Tab();
        tab.addComponentAsFirst(icon.create());
        tab.add(label);
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);

        return tab;
    }

    private static Tab createSubTab(String text, Class<? extends Component> navigationTarget, VaadinIcon icon) {
        final Tab tab = new Tab();
        tab.addComponentAsFirst(icon.create());
        tab.addThemeVariants(TabVariant.LUMO_ICON_ON_TOP);
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        return tab;
    }

    private static Tab createTertierTab(String text, Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        return tab;
    }

    private Tabs getPrimaryNavigation() {
        Tabs tabs = new Tabs();
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.add(
                createTab("Menu", "", MainView.class, VaadinIcon.VAADIN_H),
                createTab("Account","",AccountView.class,VaadinIcon.USER),
                createTab("Calendar","", CalendarView.class,VaadinIcon.CALENDAR)
                //createTab("Map","", MapView.class,VaadinIcon.MAP_MARKER)

        );
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.setSelectedIndex(0);

        return tabs;
    }

    private Tabs getSecondaryNavigation(UserService userService) {


        Tabs tabs = new Tabs();
        if (checkUserPermission(userService)) {
            tabs.add(

                    createSubTab("Account", AccountView.class, VaadinIcon.USER),
                    createSubTab("Workspaces", WorkspaceList.class, VaadinIcon.GRID_SMALL),
                    createSubTab("Rooms", RoomListView.class, VaadinIcon.THIN_SQUARE),
                    createSubTab("Users", UserlistView.class, VaadinIcon.USERS)
            );
        }
        else {
            tabs.add(
                    createSubTab("Account", AccountView.class, VaadinIcon.USER)

            );
        }
        tabs.setSelectedIndex(0);
        return tabs;
    }

    private Tabs getTertieryyNavigation() {


        Tabs tabs = new Tabs();
        tabs.add(
                createTertierTab("Users",  UserlistView.class),
                createTertierTab("Workspaces",  WorkspaceList.class)
        );


        return tabs;
    }
    public boolean checkUserPermission (UserService userService)  {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserPrincipal customUser = (MyUserPrincipal ) authentication.getPrincipal();
        ApplicationUser currentUser = userService.findUserbyUsername(customUser.getUsername());
        UserRole userRoleOfCU = currentUser.getUserRole();

        return userRoleOfCU.getId() == 1L;
    }
}


