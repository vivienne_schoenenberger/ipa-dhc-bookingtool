package com.example.application.view.main;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.service.LazyDataProvider.WorkspaceDataProvider;
import com.example.application.service.RoomService;
import com.example.application.service.WorkspaceService;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.crud.BinderCrudEditor;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.crud.CrudEditor;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToFloatConverter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.dao.DataIntegrityViolationException;


import javax.annotation.security.PermitAll;
import java.util.Arrays;
import java.util.List;


@PermitAll
@Route(value = "Workspaces",layout = MainMenuView.class)
@PageTitle("Workspaces | Bookingtool")
public class WorkspaceList extends VerticalLayout {
    Crud<Workspace> crud;
    private final List<Room> roomList;

    Room roomInEditor;
    private Paragraph errorParagraph;

    public WorkspaceList(WorkspaceService workspaceService, RoomService roomService) {

        roomList = (List<Room>) roomService.findAll();
        errorParagraph = new Paragraph("Can't delete room as reservations depend on it");
        errorParagraph.getStyle().set("color","red");
        errorParagraph.setVisible(false);
        crud = new Crud<>(

                Workspace.class,
                createEditor()
        );
        setupGrid();
        add(

                new H1("All workspaces"),
                crud
        );

        WorkspaceDataProvider workspaceDataProvider = new WorkspaceDataProvider(workspaceService,roomService);
        crud.setDataProvider(workspaceDataProvider);

        crud.addDeleteListener(deleteEvent -> {
            try {
                workspaceDataProvider.delete(deleteEvent.getItem());
            }catch (DataIntegrityViolationException e){
                errorParagraph.setVisible(true);
                crud.getDeleteButton().setEnabled(false);
                try {
                    crud.wait(3000);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
            }
                }
        );
        crud.getCancelButton().addClickListener(e->{
            errorParagraph.setVisible(false);
            crud.getDeleteButton().setEnabled(true);
        });
        crud.addEditListener(editEvent ->{
            roomInEditor = crud.getEditor().getItem().getRoom();
        });
        crud.addSaveListener(saveEvent -> {

                    if((saveEvent.getItem().getRoom() == null) && (workspaceService.findWorkspaceById(saveEvent.getItem().getId())!=null)){
                        workspaceDataProvider.roomToUpdate(roomInEditor,saveEvent.getItem());
                    }
                    workspaceDataProvider.persist(saveEvent.getItem());
        });



    }

    private CrudEditor<Workspace> createEditor(){


        TextField bezeichnung = new TextField("Description");
        TextField cost = new TextField("Price (CHF/h)");
        cost.setPlaceholder("0.00");
        ComboBox<Room> comboBox = new ComboBox<>();
        comboBox.setLabel("Select Room");
        comboBox.setItems(
                roomList

        );
        comboBox.setItemLabelGenerator(Room::getBezeichnung);
        FormLayout form = new FormLayout(bezeichnung,comboBox,cost);
        form.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("30em", 2)
        );




        Binder<Workspace> binder = new Binder<>(Workspace.class);
        binder.forField(cost).withConverter(new StringToFloatConverter("Value must be decimal number")).asRequired().bind(Workspace::getPreis,Workspace::setPreis).setAsRequiredEnabled(true);
        binder.forField(bezeichnung).asRequired().bind(Workspace::getBezeichnung,Workspace::setBezeichnung).setAsRequiredEnabled(true);
        binder.forField(comboBox).asRequired().bind(Workspace::getRoom,Workspace::setRoom).setAsRequiredEnabled(false);

        return new BinderCrudEditor<>(binder, form);


    }
    private void setupGrid() {
        Grid<Workspace> grid = crud.getGrid();

        // Only show these columns (all columns shown by default):
        String ROOM = "room";
        String BEZEICHNUNG = "bezeichnung";
        String COST = "preis";
        String EDIT_COLUMN = "vaadin-crud-edit-column";
        List<String> visibleColumns = Arrays.asList(
                BEZEICHNUNG,
                ROOM,
                COST,
                EDIT_COLUMN
        );
        grid.getColumns().forEach(column -> {
            String key = column.getKey();
            if (!visibleColumns.contains(key)) {
                grid.removeColumn(column);
            }
        });
        // Reorder the columns (alphabetical by default)
        grid.setColumnOrder(
                grid.getColumnByKey(BEZEICHNUNG),
                grid.getColumnByKey(ROOM),
                grid.getColumnByKey(COST),
                grid.getColumnByKey(EDIT_COLUMN)
        );


    }


}
