package com.example.application.view.main;

import com.vaadin.flow.component.dnd.DragSource;
import com.vaadin.flow.component.dnd.DropEffect;
import com.vaadin.flow.component.dnd.DropTarget;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import javax.annotation.security.PermitAll;

@PermitAll
@Route(value = "Map",layout = MainMenuView.class)
@PageTitle("Map | Bookingtool")
public class MapView extends HorizontalLayout {
    public Div box1 = new Div();
    public Div box2 = new Div();

    // Make box 1 draggable and store reference to the configuration object
    public DragSource<Div> box1DragSource = DragSource.create(box1);

    // Access box 2 drag related configuration object without making it draggable
    public DragSource<Div> box2DragSource = DragSource.configure(box2);

// Make box 2 draggable
    VerticalLayout first = new VerticalLayout();
    VerticalLayout second = new VerticalLayout();

    // Make the first layout an active drop target
    DropTarget<VerticalLayout> dropTarget = DropTarget.create(first);

    // Provide access to drop target API for second layout,
// without setting it as an active drop target.
    DropTarget<VerticalLayout> dropTarget2 = DropTarget.configure(second);

    public MapView() {
        first.setId("first-drop-t");
        dropTarget.setDropEffect(DropEffect.MOVE);
        box2.setClassName("box2");
        box1.setClassName("box1");
        box2DragSource.setDraggable(true);
        box1DragSource.setDraggable(true);
        add(box1, box2,first);
        setId("map-view");
        dropTarget.addDropListener(event -> {
            // move the dragged component to inside the drop target component
            if (event.getDropEffect() == DropEffect.MOVE) {
                // the drag source is available only if the dragged component is from
                // the same UI as the drop target
                event.getDragSourceComponent().ifPresent(first::add);

                event.getDragData().ifPresent(data -> {
                    // the server side drag data is available if it has been set and the
                    // component was dragged from the same UI as the drop target
                });
            }
        });
    }


}
