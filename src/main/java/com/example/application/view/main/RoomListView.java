package com.example.application.view.main;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.service.LazyDataProvider.RoomDataProvider;
import com.example.application.service.RoomService;
import com.example.application.service.WorkspaceService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.crud.BinderCrudEditor;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.crud.CrudEditor;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.data.binder.Binder;

import com.vaadin.flow.data.converter.StringToFloatConverter;
import com.vaadin.flow.data.renderer.NumberRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.*;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.dao.DataIntegrityViolationException;
import org.vaadin.addons.tatu.ColorPicker;

import javax.annotation.security.PermitAll;

import java.io.*;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@PermitAll
@Route(value = "rooms", layout = MainMenuView.class)
@PageTitle("Rooms | Bookingtool")
public class RoomListView extends VerticalLayout {

    Crud<Room> crud;
    private final Set<Workspace> workspaceList;
    private File file;
    private String originalFileName;
    private String mimeType;
    private final MultiSelectComboBox<Workspace> multiselectComboBox;

    private Paragraph errorParagraph;


    @Autowired
    public  RoomListView(RoomService roomService, WorkspaceService workspaceService){


        multiselectComboBox = new MultiSelectComboBox<>();
        errorParagraph = new Paragraph("Can't delete room as reservations depend on it");
        errorParagraph.getStyle().set("color","red");
        errorParagraph.setVisible(false);
       workspaceList= workspaceService.findAllByRoom(null);
        crud = new Crud<>(
                Room.class,
                createEditor()
        );
        setupGrid();
        add(
                new H1("All rooms"),
                crud
        );

        RoomDataProvider roomDataProvider = new RoomDataProvider(roomService,workspaceService);
        if(roomDataProvider.updatedWorkspaceSelection().size()==0){
            multiselectComboBox.setHelperText("No workspaces available!");
        }
        else multiselectComboBox.setHelperText("");
        crud.setDataProvider(roomDataProvider);

            crud.addDeleteListener(roomDeleteEvent -> {
                try {
                roomDataProvider.delete(roomDeleteEvent.getItem());
            } catch (DataIntegrityViolationException e){
                    errorParagraph.setVisible(true);
                    crud.getDeleteButton().setEnabled(false);
                    try {
                        crud.getEditor().wait(3000);
                    } catch (InterruptedException ex) {
                        throw new RuntimeException(ex);
                    }
                }
        });
        crud.getCancelButton().addClickListener(e->{
            errorParagraph.setVisible(false);
            crud.getDeleteButton().setEnabled(true);
        });
        crud.addSaveListener(roomSaveEvent -> {
            roomDataProvider.persist(roomSaveEvent.getItem());
            multiselectComboBox.setItems(roomDataProvider.updatedWorkspaceSelection());


                }
        );

    }
    private CrudEditor<Room> createEditor() {
        TextField bezeichnung = new TextField("Description");
        TextField cost = new TextField ("Cost per hour in CHF");
        cost.setPlaceholder("0.00");
        ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue("#FFFFFF");
        colorPicker.setLabel("Color");
        multiselectComboBox.setLabel("Select Workspaces");
        multiselectComboBox.setItems(
                workspaceList
        );
        multiselectComboBox.setItemLabelGenerator(Workspace::getBezeichnung);
        colorPicker.setPresets(Arrays.asList(
                new ColorPicker.ColorPreset("#E75EFD", "Heliotrope"),
                new ColorPicker.ColorPreset("#A854E3", "Lavender Indigo"),
                new ColorPicker.ColorPreset("#9769FA", "Lavender Blue"),
                new ColorPicker.ColorPreset("#5A54E3", "Majorelle Blue"),
                new ColorPicker.ColorPreset("#00A9C5", "Blue-Green")
                )
        );
        TextField imageName = new TextField("");
        Upload upload = new Upload(this::receiveUpload);
        Div output = new Div(new Text("(no image file uploaded yet)"));
        upload.setAcceptedFileTypes("image/png",".jpg","image/jpeg",".jpeg");
        //upload.setMaxFileSize(512);
        upload.addSucceededListener(event -> {
            output.removeAll();
            output.add(new Text("Uploaded: "+originalFileName+" to "+ file.getAbsolutePath()+ "Type: "+mimeType));
            output.add(new Image(new StreamResource(this.originalFileName,this::loadFile),"Uploaded image"));
            imageName.setValue(originalFileName);
            //save path to user
        });
        imageName.setReadOnly(true);
        upload.addFailedListener(event -> {
            output.removeAll();
            output.add(new Text("Upload failed: " + event.getReason()));
        });
        FormLayout form = new FormLayout(bezeichnung,cost,colorPicker,multiselectComboBox,upload,imageName,errorParagraph,errorParagraph);
        form.setWidth("30em");

        Binder<Room> binder = new Binder<>(Room.class);
        binder.forField(cost).withConverter(new StringToFloatConverter("Value must be a decimal number"){
        }).asRequired().bind(Room::getPreis,Room::setPreis).setAsRequiredEnabled(true);
        binder.forField(bezeichnung).asRequired().bind(Room::getBezeichnung,Room::setBezeichnung).setAsRequiredEnabled(true);
        binder.forField(multiselectComboBox).asRequired().bind(Room::getWorkspaceList,Room::setWorkspaceList).setAsRequiredEnabled(false);
        binder.forField(colorPicker).asRequired().bind(Room::getRoomColor,Room::setRoomColor).setAsRequiredEnabled(true);
        binder.forField(imageName).asRequired().bind(Room::getImageName,Room::setImageName).setAsRequiredEnabled(false);
        return new BinderCrudEditor<>(binder, form);
    }

    private void setupGrid() {
        crud.getGrid().removeColumnByKey("id");
        crud.getGrid().removeColumnByKey("preis");
        crud.getGrid().removeColumnByKey("workspaceList");
        crud.getGrid().removeColumnByKey("bezeichnung");
        crud.getGrid().removeColumnByKey("roomColor");
        crud.getGrid().removeColumnByKey("imageName");
        Grid<Room> grid = crud.getGrid();
        grid.addClassName("room-grid");

        grid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        grid.addColumn(Room::getId).setHeader("");
        grid.addColumn(Room::getBezeichnung).setHeader("Bezeichnung");
        grid.addColumn(Room::getWorkspaceList).setHeader("Workspace List");
        grid.addComponentColumn(room -> createColorIcon(room.getRoomColor())).setHeader("Color");
        grid.addComponentColumn(this::getRoomImage).setHeader("");
        grid.addColumn(Room::getImageName).setHeader("Bild");
        grid.addColumn(new NumberRenderer<>(Room::getPreis, new DecimalFormat("0.00"))).setHeader("Price (CHF/h)");

    }

    /** Receive a uploaded file to a file.
     */
    public OutputStream receiveUpload(String originalFileName, String MIMEType) {

        this.originalFileName = originalFileName;
        this.mimeType = MIMEType;
        try {

            // Create a temporary file for example, you can provide your file here.
            this.file = new File("./src/main/resources/public/"+originalFileName);

            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Failed to create InputStream for: '" + this.file.getAbsolutePath(), e);
        }

        return null;
    }
    public InputStream loadFile() {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Failed to create InputStream for: '" + this.file.getAbsolutePath(), e);
        }
        return null;
    }

    @SneakyThrows
    public Component getRoomImage(Room room) {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select image_name from bookingtool.room where id ='" + room.getId() +"'" ;
        ResultSet rs = statement.executeQuery(sql);
        String imageName = "";
        while (rs.next()) {
            imageName = rs.getString("image_name");
        }
        String finalImageName = imageName;
        StreamResource imageResource = new StreamResource(finalImageName,
                () -> getClass().getResourceAsStream("/public/"+ finalImageName));
        Image image = new Image(imageResource,"");
        image.setMaxHeight("80px");
        c.close();
        return image;
    }
public Icon createColorIcon(String color){
        Icon icon = new Icon(VaadinIcon.CIRCLE);
        icon.setColor(color);
        return icon;
        }

}
