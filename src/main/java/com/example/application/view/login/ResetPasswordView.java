package com.example.application.view.login;

import com.example.application.entities.ApplicationUser;
import com.example.application.service.UserService;
import com.vaadin.flow.component.UI;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;


import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import net.bytebuddy.utility.RandomString;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;


@AnonymousAllowed
@Route("/reset")
@PageTitle("Reset | Vaadin CRM")

public class ResetPasswordView extends VerticalLayout {

    public ResetPasswordView(UserService userService) {
        ResetPasswordForm resetPasswordForm = new ResetPasswordForm();
        // Center the RegistrationForm
        setHorizontalComponentAlignment(Alignment.CENTER, resetPasswordForm);
        setSizeFull();
        setAlignItems(Alignment.CENTER);
        setId("new-pw-layout");
        setJustifyContentMode(JustifyContentMode.CENTER);
        resetPasswordForm.getResetBtn().addClickListener(buttonClickEvent -> {
            if (userService.findUserbyUsername(resetPasswordForm.getUsernameField().getValue()) != null) {
                String token = RandomString.make(10);
                ApplicationUser userWithNewToken = userService.findUserbyUsername(resetPasswordForm.getUsernameField().getValue());
                userWithNewToken.getEmail();
                try {
                    sendEmail(userWithNewToken, token);
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
                saveResetToken(userService,userWithNewToken,token);
                UI.getCurrent().navigate(CheckResetToken.class);
            }


        });
        add(resetPasswordForm);
    }

    public void sendEmail(ApplicationUser user, String token) throws MessagingException {

        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        prop.put("mail.smtp.ssl.protocols","TLSv1.2");

        prop.put("mail.transport.protocol","imap");
        prop.put("mail.smtp.starttls.required","true");
        //prop.put("mail.smtp.timeout","15000");
        //prop.put("mail.smtp.connectiontimeout","15000");
        prop.put("mail.smtp.socketFactory.fallback","true");
        prop.put("mail.debug","true");

        //spring.mail.properties.mail.smtp.ssl.enable=false


        String pw = System.getenv("MAIL_PASSWORD");
        String username = System.getenv("MAIL_USERNAME");
        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, pw);
            }
        });
        Message message = new MimeMessage(session);

        message.setRecipient(Message.RecipientType.TO,new InternetAddress(user.getEmail()));

        //TODO: Link has to be changed when going live
        String link = "https://localhost:8000/NewPassword";
        String msg =("<p>Hello <b>" + user.getFirstName() + " " + user.getLastName()+"<b>"
                + ",</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below and provide following code: </p>"
                + "\b"
                + token
                + "\b"
                + "<p><a href=\"" + link + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>");
        message.setSubject("Password Reset Bookingtool");
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(msg, "text/html; charset=utf-8");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        message.setContent(multipart);
        Transport.send(message);

    }
    public void saveResetToken(UserService userService,ApplicationUser user,String token){
        user.setResetPasswordToken(token);
        userService.update(user);
    }
}