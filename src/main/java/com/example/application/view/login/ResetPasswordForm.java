package com.example.application.view.login;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.textfield.TextField;

public class ResetPasswordForm extends FormLayout {
    private TextField usernameField;
    private Button resetBtn;

    private H3 title;
    public ResetPasswordForm(){
        usernameField = new TextField("Username");
        resetBtn = new Button("Reset your Password");
        resetBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        title = new H3("Password reset");

        add(title,usernameField,resetBtn);
    }

    public TextField getUsernameField() {
        return usernameField;
    }

    public void setUsernameField(TextField usernameField) {
        this.usernameField = usernameField;
    }

    public Button getResetBtn() {
        return resetBtn;
    }

    public void setResetBtn(Button resetBtn) {
        this.resetBtn = resetBtn;
    }

}
