package com.example.application.view.login;

import com.example.application.entities.ApplicationUser;
import com.example.application.service.UserService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;

import java.util.Objects;
import java.util.concurrent.TimeUnit;


@AnonymousAllowed
@Route("/newPassword")
@PageTitle("New Password | Vaadin CRM")
public class CheckResetToken  extends VerticalLayout implements BeforeLeaveObserver  {
    private TextField tokenField;
    private Button submitNewPasswordButton;
    private Button checkTokenValue;
    private PasswordField newPasswordField;

    private PasswordField repeatPasswordField;
    private Paragraph failedAccess;
    private Notification savedPassword;

    private HorizontalLayout buttonLayout;

    private Button notifBtn;
    private H3 title;
    private FormLayout mainLayout;
    private Div notifText;

    public CheckResetToken(UserService userService){
        mainLayout= new FormLayout();
        tokenField= new TextField("Code");
        newPasswordField = new PasswordField("New Password");
        repeatPasswordField = new PasswordField("Repeat Password");
        submitNewPasswordButton = new Button("Save Password");
        checkTokenValue = new Button("Validate");
        checkTokenValue.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        newPasswordField.setVisible(false);
        repeatPasswordField.setVisible(false);
        submitNewPasswordButton.setVisible(false);
        submitNewPasswordButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        failedAccess = new Paragraph("Wrong Code");
        failedAccess.setVisible(false);
        failedAccess.getStyle().set("color","red");
        savedPassword = new Notification("Password saved");
        buttonLayout = new HorizontalLayout();
        title = new H3("Set a new password");

        savedPassword.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
        checkTokenValue.addClickListener(buttonClickEvent -> {
            if(tokenField.getValue()!=null) {
                if (userService.findByResetPwToken(tokenField.getValue()) == null) {
                    tokenField.setInvalid(true);
                    failedAccess.setVisible(true);
                } else {
                    tokenField.setVisible(false);
                    checkTokenValue.setVisible(false);
                    failedAccess.setVisible(false);
                    newPasswordField.setVisible(true);
                    repeatPasswordField.setVisible(true);
                    submitNewPasswordButton.setVisible(true);
                }
            }
           submitNewPasswordButton.addClickListener(buttonClickEvent1 -> {
               if(Objects.equals(newPasswordField.getValue(), repeatPasswordField.getValue())) {
                   ApplicationUser userToUpdate = userService.findByResetPwToken(tokenField.getValue());
                   userService.updatePassword(userToUpdate, newPasswordField.getValue());
                   repeatPasswordField.setInvalid(false);
                   repeatPasswordField.setErrorMessage("");
                   savedPassword.setPosition(Notification.Position.TOP_CENTER);
                   savedPassword.open();

                   UI.getCurrent().navigate(LoginView.class);

               }
               else {
                   repeatPasswordField.setInvalid(true);
                   repeatPasswordField.setErrorMessage("Passwords don't watch");
               }
           });


        });
        mainLayout.add(title,tokenField,checkTokenValue,newPasswordField,repeatPasswordField,submitNewPasswordButton,failedAccess,savedPassword);

        setHorizontalComponentAlignment(Alignment.CENTER, mainLayout);
        setAlignItems(Alignment.CENTER);
        setSizeFull();
        mainLayout.setColspan(title,1);
        mainLayout.setColspan(tokenField,1);
        mainLayout.setColspan(checkTokenValue,1);
        mainLayout.setColspan(newPasswordField,1);
        mainLayout.setColspan(submitNewPasswordButton,1);
        mainLayout.setResponsiveSteps(
                // Use one column by default
                new FormLayout.ResponsiveStep("0", 1));
        mainLayout.setId("form-layout-new-pw");
        setId("reset-pw-layout");

        add(mainLayout);

    }

    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        BeforeLeaveEvent.ContinueNavigationAction action =
                beforeLeaveEvent.postpone();

        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        action.proceed();

    }
}
