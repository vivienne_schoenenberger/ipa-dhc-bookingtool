package com.example.application.view.calendar;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import org.vaadin.stefan.fullcalendar.FullCalendar;

@Tag("full-calendar-with-tooltip")
@JsModule("./full-calendar-with-tooltips.js")
@CssImport("tippy.js/dist/tippy.css")
@CssImport("tippy.js/themes/light.css")
public class FullCalendarWithTooltip extends FullCalendar {

    public FullCalendarWithTooltip() {
        super(5);
    }
}