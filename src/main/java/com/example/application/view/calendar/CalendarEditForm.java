package com.example.application.view.calendar;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.service.RoomService;
import com.example.application.service.WorkspaceService;
import com.vaadin.componentfactory.ToggleButton;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.server.StreamResource;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class CalendarEditForm extends FormLayout {
    private H2 formTitle;
    private TextField entryTitle;
    private TextArea description;
    private DateTimePicker startDateTime;
    private DatePicker startDate;
    private DateTimePicker endDateTime;
    private DatePicker endDate;
    private Button editSingularEntry;
    private Checkbox roomCheckbox;
    private RadioButtonGroup<String> radioButtonGroup;
    private Checkbox workspaceCheckbox;
    private List<Room> roomList;
    private  List<Workspace> workspaceList;
    private Room selectedRoom;
    private final Checkbox recurring;
    private final MultiSelectComboBox<DayOfWeek> reoccurringDays;
    private final Button submitButton;
    private final Button cancelButton;
    private Button deleteButton;
    private Button checkAvailabilityButton;
    private Hr divider;
    private Hr divider1;

    private Hr divider3;

    private Hr divider2;
    private H3 seriesTitle;

    private H3 singularEntryTitle;



    private ToggleButton prolongSeries;

    private ToggleButton editSeries;
    private DatePicker entryDate;


    private Grid<Room> roomGrid;


    private Grid<Workspace> workspaceGrid;
    public CalendarEditForm(RoomService roomService, WorkspaceService workspaceService){
        formTitle= new H2("Edit Entry");
        formTitle.getStyle().set("color","#00A9C5");
        entryDate= new DatePicker("Date of current day");
        entryTitle= new TextField("Entry title");
        description= new TextArea("Description");
        radioButtonGroup = new RadioButtonGroup<>();
        radioButtonGroup.setItems("Full Day","Specific Time");
        radioButtonGroup.setValue("Specific Time");
        roomCheckbox = new Checkbox("Rooms");
        workspaceCheckbox = new Checkbox("Workspaces");
        startDateTime = new DateTimePicker("Start");
        startDateTime.setMin(LocalDateTime.now());
        startDateTime.setMax(LocalDateTime.now().plusYears(1));
        startDateTime.setStep(Duration.ofMinutes((30)));
        endDateTime = new DateTimePicker("Until");
        endDateTime.setMax(LocalDateTime.now().plusYears(1));
        endDateTime.setMin(LocalDateTime.now());
        endDateTime.setHelperText("Available every day between 5:00AM - 10:00PM");
        startDate = new DatePicker("Start");
        startDate.setMin(LocalDate.now());
        startDate.setMax(LocalDate.now().plusYears(1));
        startDate.setVisible(false);
        endDate = new DatePicker("Until");
        endDate.setMax(LocalDate.now().plusYears(1));
        endDate.setMin(LocalDate.now());
        endDate.setVisible(false);
        endDate.setHelperText("Available every day between 5:00AM - 10:00PM");
        recurring= new Checkbox("Make recurring");
        editSingularEntry = new Button("Edit Reservation on this date");
        editSingularEntry.setEnabled(false);
        editSingularEntry.setId("edit-singular-entry-btn");
        editSingularEntry.setVisible(false);
        divider = new Hr();
        divider1 = new Hr();
        divider2 = new Hr();
        divider3 = new Hr();
        divider2.setVisible(false);
        divider3.setVisible(false);
        seriesTitle = new H3("Series:");
        seriesTitle.getStyle().set("color","#cdd0d3");
        seriesTitle.getStyle().set("margin-top","0px");
        seriesTitle.setVisible(false);
        singularEntryTitle = new H3("Singular Entry:");
        singularEntryTitle.getStyle().set("color","#cdd0d3");
        singularEntryTitle.getStyle().set("margin-top","0px");
        singularEntryTitle.setVisible(false);
        roomGrid = new Grid<>(Room.class);
        roomGrid.removeAllColumns();
        roomGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        roomGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        roomGrid.addComponentColumn(this::getRoomImage).setHeader("");
        roomGrid.addComponentColumn(room -> createColorIcon(room.getRoomColor())).setHeader("Color");
        roomGrid.addColumn(Room::getBezeichnung).setHeader("Name");
        roomGrid.addColumn(new NumberRenderer<>(Room::getPreis, new DecimalFormat("0.00"))).setHeader("Price (CHF/h)");
        roomGrid.addColumn(createRoomToggleRenderer(roomGrid)).setHeader("Details");
        roomGrid.setItemDetailsRenderer(createRoomDetailsRenderer());
        roomGrid.setDetailsVisibleOnClick(true);

        List<DayOfWeek> week = new ArrayList<>();
        week.add(DayOfWeek.MONDAY);
        week.add(DayOfWeek.TUESDAY);
        week.add(DayOfWeek.WEDNESDAY);
        week.add(DayOfWeek.THURSDAY);
        week.add(DayOfWeek.FRIDAY);
        reoccurringDays= new MultiSelectComboBox<>("Reoccurring Day");
        reoccurringDays.setItems(week);
        reoccurringDays.setVisible(false);

        radioButtonGroup.addValueChangeListener(event -> {
            switch (radioButtonGroup.getValue()){
                case "Full Day":
                    startDateTime.setVisible(false);
                    startDate.setVisible(true);
                    endDateTime.setVisible(false);
                    endDate.setVisible(true);

                    break;
                case "Specific Time":
                    startDateTime.setVisible(true);
                    endDateTime.setVisible(true);
                    startDate.setVisible(false);
                    endDate.setVisible(false);

                    break;

            }
        });
        recurring.addValueChangeListener(event -> {
            reoccurringDays.setVisible(recurring.getValue());
        });
        prolongSeries = new ToggleButton("Prolong reservation series");
        prolongSeries.setVisible(false);
        prolongSeries.setId("toggle-edit-series");
        editSeries = new ToggleButton("Edit series");
        editSeries.setVisible(false);
        roomList= (List<Room>) roomService.findAll();
        workspaceList = (List<Workspace>) workspaceService.findAll();

        entryDate.setVisible(false);
        entryDate.setReadOnly(true);
        submitButton = new Button("Update");
        submitButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                ButtonVariant.LUMO_SUCCESS);
        cancelButton = new Button("Cancel");
        deleteButton = new Button("Delete");
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        checkAvailabilityButton = new Button("Check Availability");
        checkAvailabilityButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        submitButton.setVisible(false);
        workspaceCheckbox.setEnabled(false);
        roomCheckbox.setEnabled(false);
        startDate.setReadOnly(true);
        endDate.setReadOnly(true);
        startDateTime.setReadOnly(true);
        endDateTime.setReadOnly(true);
        radioButtonGroup.setReadOnly(true);
        entryTitle.setReadOnly(true);
        description.setReadOnly(true);
        workspaceGrid = new Grid<>(Workspace.class);
        workspaceGrid.removeAllColumns();
        workspaceGrid.addColumn(Workspace::getBezeichnung).setHeader("Name");
        workspaceGrid.addColumn(new NumberRenderer<>(Workspace::getPreis, new DecimalFormat("0.00"))).setHeader("Price (CHF/h)");
        workspaceGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        workspaceGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        workspaceGrid.setVisible(false);
        workspaceGrid.addColumn(createWorkspaceToggleRenderer(workspaceGrid)).setHeader("Details");
        workspaceGrid.setItemDetailsRenderer(createWorkspaceDetailsRenderer());
        workspaceGrid.setDetailsVisibleOnClick(true);


        add(
                formTitle,
                divider1,
                seriesTitle,
                editSeries,
                divider3,
                radioButtonGroup,
                prolongSeries,
                startDate,
                startDateTime,
                endDate,
                endDateTime,
                roomCheckbox,
                workspaceCheckbox,
                roomGrid,
                workspaceGrid,
                entryTitle,
                description,
                recurring,
                reoccurringDays,
                divider,
                singularEntryTitle,
                entryDate,
                editSingularEntry,
                divider2,
                cancelButton,
                deleteButton,
                submitButton,
                checkAvailabilityButton);
        setMaxWidth("500px");
        setColspan(entryDate,4);
        setColspan(roomCheckbox,2);
        setColspan(workspaceCheckbox,2);
        setColspan(formTitle,4);
        setColspan(entryTitle,4);
        setColspan(description,4);
        setColspan(radioButtonGroup,4);
        setColspan(startDateTime,4);
        setColspan(endDateTime,4);
        setColspan(startDate,4);
        setColspan(endDate,4);
        setColspan(recurring,3);
        setColspan(reoccurringDays,2);
        setColspan(divider,4);
        setColspan(divider1,4);
        setColspan(divider2,4);
        setColspan(cancelButton,1);
        setColspan(deleteButton,1);
        setColspan(submitButton,2);
        setColspan(entryDate,2);
        setColspan(editSingularEntry,2);
        setColspan(checkAvailabilityButton,2);
        setColspan(seriesTitle,4);
        setColspan(singularEntryTitle,4);
        setColspan(prolongSeries,4);
        setColspan(editSeries,4);
        setColspan(divider3,4);
        setColspan(workspaceGrid,4);
        setColspan(roomGrid,4);

        setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("500px", 4));

    }
    private Renderer<Room> createRoomToggleRenderer(Grid<Room>grid) {
        return LitRenderer.<Room>of(
                        "<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\"><iron-icon icon=\"lumo:dropdown\"></iron-icon></vaadin-button>")
                .withFunction("handleClick",room -> grid.setDetailsVisible(room,
                        !grid.isDetailsVisible(room))
                );

    }
    private static ComponentRenderer<RoomDetailsFormLayout, Room> createRoomDetailsRenderer() {
        return new ComponentRenderer<>(RoomDetailsFormLayout::new,
                RoomDetailsFormLayout::setRoom);
    }
    private static class RoomDetailsFormLayout extends FormLayout {
        private final TextField description = new TextField("Description");
        private final TextField directions = new TextField("Directions");
        private final TextField seats = new TextField("Number of Seats");

        public RoomDetailsFormLayout() {
            Stream.of(description,seats, directions).forEach(field -> {
                field.setReadOnly(true);
                add(field);
            });

            setResponsiveSteps(new ResponsiveStep("0", 3));
            setColspan(description, 2);
            setColspan(seats, 1);
            setColspan(directions, 3);
        }
        public void setRoom(Room room) {
            description.setValue(room.getBezeichnung());
            seats.setValue(String.valueOf(room.getWorkspaceList().size()));


        }
    }
    private Renderer<Workspace> createWorkspaceToggleRenderer(Grid<Workspace>grid) {
        return LitRenderer.<Workspace>of(
                        "<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\"><iron-icon icon=\"lumo:dropdown\"></iron-icon></vaadin-button>")
                .withFunction("handleClick",workspace -> grid.setDetailsVisible(workspace,
                        !grid.isDetailsVisible(workspace))
                );

    }
    private static ComponentRenderer<WorkspaceDetailsFormLayout, Workspace> createWorkspaceDetailsRenderer() {
        return new ComponentRenderer<>(WorkspaceDetailsFormLayout::new,
                WorkspaceDetailsFormLayout::setWorkspace);
    }
    private static class WorkspaceDetailsFormLayout extends FormLayout {
        private final TextField description = new TextField("Description");
        private final TextField directions = new TextField("Directions");

        public WorkspaceDetailsFormLayout() {
            Stream.of(description, directions).forEach(field -> {
                field.setReadOnly(true);
                add(field);
            });

            setResponsiveSteps(new ResponsiveStep("0", 3));
            setColspan(description, 3);
            setColspan(directions, 3);

        }
        public void setWorkspace(Workspace workspace) {

        }
    }
    public H2 getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(H2 formTitle) {
        this.formTitle = formTitle;
    }

    public TextField getEntryTitle() {
        return entryTitle;
    }

    public void setEntryTitle(TextField entryTitle) {
        this.entryTitle = entryTitle;
    }

    public TextArea getDescription() {
        return description;
    }

    public void setDescription(TextArea description) {
        this.description = description;
    }

    public DateTimePicker getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(DateTimePicker startDateTime) {
        this.startDateTime = startDateTime;
    }

    public DatePicker getStartDate() {
        return startDate;
    }

    public void setStartDate(DatePicker startDate) {
        this.startDate = startDate;
    }

    public DateTimePicker getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(DateTimePicker endDateTime) {
        this.endDateTime = endDateTime;
    }

    public DatePicker getEndDate() {
        return endDate;
    }

    public void setEndDate(DatePicker endDate) {
        this.endDate = endDate;
    }

    public Button getEditSingularEntry() {
        return editSingularEntry;
    }

    public void setEditSingularEntry(Button editSingularEntry) {
        this.editSingularEntry = editSingularEntry;
    }

    public Checkbox getRoomCheckbox() {
        return roomCheckbox;
    }

    public void setRoomCheckbox(Checkbox roomCheckbox) {
        this.roomCheckbox = roomCheckbox;
    }

    public RadioButtonGroup<String> getRadioButtonGroup() {
        return radioButtonGroup;
    }

    public void setRadioButtonGroup(RadioButtonGroup<String> radioButtonGroup) {
        this.radioButtonGroup = radioButtonGroup;
    }

    public Checkbox getWorkspaceCheckbox() {
        return workspaceCheckbox;
    }

    public void setWorkspaceCheckbox(Checkbox workspaceCheckbox) {
        this.workspaceCheckbox = workspaceCheckbox;
    }

    public List<Room> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<Room> roomList) {
        this.roomList = roomList;
    }

    public List<Workspace> getWorkspaceList() {
        return workspaceList;
    }

    public void setWorkspaceList(List<Workspace> workspaceList) {
        this.workspaceList = workspaceList;
    }

    public Room getSelectedRoom() {
        return selectedRoom;
    }

    public void setSelectedRoom(Room selectedRoom) {
        this.selectedRoom = selectedRoom;
    }

    public Checkbox getRecurring() {
        return recurring;
    }

    public MultiSelectComboBox<DayOfWeek> getReoccurringDays() {
        return reoccurringDays;
    }


    public Button getSubmitButton() {
        return submitButton;
    }

    public Button getCancelButton() {
        return cancelButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }

    public Button getCheckAvailabilityButton() {
        return checkAvailabilityButton;
    }

    public void setCheckAvailabilityButton(Button checkAvailabilityButton) {
        this.checkAvailabilityButton = checkAvailabilityButton;
    }

    public DatePicker getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(DatePicker entryDate) {
        this.entryDate = entryDate;
    }

    public H3 getSeriesTitle() {
        return seriesTitle;
    }

    public void setSeriesTitle(H3 seriesTitle) {
        this.seriesTitle = seriesTitle;
    }

    public H3 getSingularEntryTitle() {
        return singularEntryTitle;
    }

    public void setSingularEntryTitle(H3 singularEntryTitle) {
        this.singularEntryTitle = singularEntryTitle;
    }
    public Hr getDivider2() {
        return divider2;
    }

    public void setDivider2(Hr divider2) {
        this.divider2 = divider2;
    }
    public ToggleButton getProlongSeries() {
        return prolongSeries;
    }

    public void setProlongSeries(ToggleButton prolongSeries) {
        this.prolongSeries = prolongSeries;
    }
    public Hr getDivider3() {
        return divider3;
    }

    public void setDivider3(Hr divider3) {
        this.divider3 = divider3;
    }

    public ToggleButton getEditSeries() {
        return editSeries;
    }

    public void setEditSeries(ToggleButton editSeries) {
        this.editSeries = editSeries;
    }

    public Grid<Workspace> getWorkspaceGrid() {
        return workspaceGrid;
    }

    public void setWorkspaceGrid(Grid<Workspace> workspaceGrid) {
        this.workspaceGrid = workspaceGrid;
    }

    public Grid<Room> getRoomGrid() {
        return roomGrid;
    }

    public void setRoomGrid(Grid<Room> roomGrid) {
        this.roomGrid = roomGrid;
    }
    @SneakyThrows
    public Component getRoomImage(Room room) {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select image_name from bookingtool.room where id ='" + room.getId() +"'" ;
        ResultSet rs = statement.executeQuery(sql);
        String imageName = "";
        while (rs.next()) {
            imageName = rs.getString("image_name");
        }
        String finalImageName = imageName;
        StreamResource imageResource = new StreamResource(finalImageName,
                () -> getClass().getResourceAsStream("/public/"+ finalImageName));
        com.vaadin.flow.component.html.Image image = new Image(imageResource,"");
        image.setMaxHeight("80px");
        c.close();
        return image;
    }

    public Icon createColorIcon(String color){
        Icon icon = new Icon(VaadinIcon.CIRCLE);
        icon.setColor(color);
        return icon;
    }
}
