package com.example.application.view.calendar;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.service.RoomService;
import com.example.application.service.WorkspaceService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.server.StreamResource;
import lombok.SneakyThrows;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;
@CssImport(value="themes/mytodo/highlightweekend.css",themeFor = "vaadin-month-calendar")

public class CalendarForm extends FormLayout {
    private H2 formTitle;
    private TextField entryTitle;
    private TextArea description;
    private DateTimePicker startDateTime;
    private DatePicker startDate;
    private DateTimePicker endDateTime;
    private DatePicker endDate;
    private Button editSingularEntry;
    private Checkbox roomCheckbox;
    private RadioButtonGroup<String> radioButtonGroup;
    private Checkbox workspaceCheckbox;
    private List<Room> roomList;
    private List<Workspace> workspaceList;
    private Room selectedRoom;
    private final Checkbox recurring;
    private final MultiSelectComboBox<DayOfWeek> reoccurringDays;
    private final Button submitButton;
    private Hr divider;
    private Button deleteButton;
    private Button checkAvailabilityButton;
    private DatePicker entryDate;
    private Grid<Room> roomGrid;
    private Grid<Workspace> workspaceGrid;


    public CalendarForm(RoomService roomService, WorkspaceService workspaceService){
        formTitle= new H2("New Entry");
        formTitle.getStyle().set("color","#00A9C5");
        entryDate= new DatePicker("Date of current day");
        entryTitle= new TextField("Entry title");
        description= new TextArea("Description");
        radioButtonGroup = new RadioButtonGroup<>();
        radioButtonGroup.setItems("Full Day","Specific Time");
        radioButtonGroup.setValue("Specific Time");
        roomCheckbox = new Checkbox("Rooms");
        workspaceCheckbox = new Checkbox("Workspaces");
        startDateTime = new DateTimePicker("Start");
        startDateTime.setMin(LocalDateTime.now());
        startDateTime.setMax(LocalDateTime.now().plusYears(1));
        startDateTime.setStep(Duration.ofMinutes((30)));
        startDateTime.getElement().getThemeList().add("weekend-highlight");
        endDateTime = new DateTimePicker("Until");
        endDateTime.setMax(LocalDateTime.now().plusYears(1));
        endDateTime.setMin(LocalDateTime.now());
        endDateTime.setHelperText("Available every day between 5:00AM - 10:00PM");
        endDateTime.getElement().getThemeList().add("weekend-highlight");
        startDate = new DatePicker("Start");
        startDate.setMin(LocalDate.now());
        startDate.setMax(LocalDate.now().plusYears(1));
        startDate.setVisible(false);
        startDate.getElement().getThemeList().add("weekend-highlight");
        endDate = new DatePicker("Until");
        endDate.setMax(LocalDate.now().plusYears(1));
        endDate.setMin(LocalDate.now());
        endDate.setVisible(false);
        endDate.setHelperText("Available every day between 5:00AM - 10:00PM");
        endDate.getElement().getThemeList().add("weekend-highlight");
        recurring= new Checkbox("Make recurring");
        editSingularEntry = new Button("Edit Reservation on this date");
        editSingularEntry.setEnabled(false);
        editSingularEntry.setVisible(false);
        divider = new Hr();


        List<DayOfWeek> week = new ArrayList<>();
        week.add(DayOfWeek.MONDAY);
        week.add(DayOfWeek.TUESDAY);
        week.add(DayOfWeek.WEDNESDAY);
        week.add(DayOfWeek.THURSDAY);
        week.add(DayOfWeek.FRIDAY);
        reoccurringDays= new MultiSelectComboBox<>("Reoccurring Day");
        reoccurringDays.setItems(week);
        reoccurringDays.setVisible(false);

        radioButtonGroup.addValueChangeListener(event -> {
        switch (radioButtonGroup.getValue()){
            case "Full Day":
                startDateTime.setVisible(false);
                startDate.setVisible(true);
                endDateTime.setVisible(false);
                endDate.setVisible(true);

                break;
            case "Specific Time":
                startDateTime.setVisible(true);
                endDateTime.setVisible(true);
                startDate.setVisible(false);
                endDate.setVisible(false);

                break;

        }
        });
        recurring.addValueChangeListener(event -> {
            reoccurringDays.setVisible(recurring.getValue());

        });

        roomList= (List<Room>) roomService.findAll();
        workspaceList = (List<Workspace>) workspaceService.findAll();

        entryDate.setVisible(false);
        entryDate.setReadOnly(true);
        submitButton = new Button("Book");
        submitButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                ButtonVariant.LUMO_SUCCESS);
        deleteButton = new Button("Delete");
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        checkAvailabilityButton = new Button("Confirm Reservation");
        checkAvailabilityButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        submitButton.setVisible(false);
        roomGrid = new Grid<>(Room.class);
        roomGrid.removeAllColumns();
        roomGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        roomGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        roomGrid.addComponentColumn(this::getRoomImage).setHeader("");
        roomGrid.addComponentColumn(room -> createColorIcon(room.getRoomColor()));
        roomGrid.addColumn(Room::getBezeichnung).setHeader("Name");
        roomGrid.setVisible(false);
        roomGrid.addColumn(new NumberRenderer<>(Room::getPreis, new DecimalFormat("0.00"))).setHeader("Price (CHF/h)");
        roomGrid.addColumn(createRoomToggleRenderer(roomGrid)).setHeader("Details");
        roomGrid.setItemDetailsRenderer(createRoomDetailsRenderer());
        roomGrid.setDetailsVisibleOnClick(true);

        workspaceGrid = new Grid<>(Workspace.class);
        workspaceGrid.removeAllColumns();
        workspaceGrid.addColumn(Workspace::getBezeichnung).setHeader("Name");
        workspaceGrid.addColumn(new NumberRenderer<>(Workspace::getPreis, new DecimalFormat("0.00"))).setHeader("Price (CHF/h)");
        workspaceGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        workspaceGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        workspaceGrid.setVisible(false);
        workspaceGrid.addColumn(createWorkspaceToggleRenderer(workspaceGrid)).setHeader("Details");
        workspaceGrid.setItemDetailsRenderer(createWorkspaceDetailsRenderer());
        workspaceGrid.setDetailsVisibleOnClick(true);


        add(
                formTitle,
                radioButtonGroup,
                startDate,
                startDateTime,
                endDate,
                endDateTime,
                roomCheckbox,
                workspaceCheckbox,
                workspaceGrid,
                roomGrid,
                entryTitle,
                description,
                recurring,
                reoccurringDays,
                entryDate,
                divider,
                editSingularEntry,
                deleteButton,
                submitButton,
                checkAvailabilityButton);
        setMaxWidth("500px");
        setColspan(entryDate,4);
        setColspan(roomCheckbox,2);
        setColspan(workspaceCheckbox,2);
        setColspan(formTitle,4);
        setColspan(entryTitle,4);
        setColspan(description,4);
        setColspan(radioButtonGroup,4);
        setColspan(startDateTime,4);
        setColspan(endDateTime,4);
        setColspan(startDate,4);
        setColspan(endDate,4);
        setColspan(recurring,3);
        setColspan(reoccurringDays,2);
        setColspan(divider,4);
        setColspan(deleteButton,2);
        setColspan(submitButton,2);
        setColspan(entryDate,2);
        setColspan(editSingularEntry,2);
        setColspan(checkAvailabilityButton,2);
        setColspan(workspaceGrid,4);
        setColspan(roomGrid,4);



        setResponsiveSteps(
                new ResponsiveStep("0", 1),
                new ResponsiveStep("500px", 4));

    }

    private Renderer<Workspace> createWorkspaceToggleRenderer(Grid<Workspace>grid) {
        return LitRenderer.<Workspace>of(
                        "<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\"><iron-icon icon=\"lumo:dropdown\"></iron-icon></vaadin-button>")
                .withFunction("handleClick",workspace -> grid.setDetailsVisible(workspace,
                        !grid.isDetailsVisible(workspace))
        );

    }
    private Renderer<Room> createRoomToggleRenderer(Grid<Room>grid) {
        return LitRenderer.<Room>of(
                        "<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\"><iron-icon icon=\"lumo:dropdown\"></iron-icon></vaadin-button>")
                .withFunction("handleClick",room -> grid.setDetailsVisible(room,
                        !grid.isDetailsVisible(room))
                );

    }
    private static ComponentRenderer<WorkspaceDetailsFormLayout, Workspace> createWorkspaceDetailsRenderer() {
        return new ComponentRenderer<>(WorkspaceDetailsFormLayout::new,
                WorkspaceDetailsFormLayout::setWorkspace);
    }

    private static ComponentRenderer<RoomDetailsFormLayout, Room> createRoomDetailsRenderer() {
        return new ComponentRenderer<>(RoomDetailsFormLayout::new,
                RoomDetailsFormLayout::setRoom);
    }

    private static class RoomDetailsFormLayout extends FormLayout {
        private final TextField description = new TextField("Description");
        private final TextField directions = new TextField("Directions");
        private final TextField seats = new TextField("Number of Seats");

        public RoomDetailsFormLayout() {
            Stream.of(description,seats, directions).forEach(field -> {
                field.setReadOnly(true);
                add(field);
            });

            setResponsiveSteps(new ResponsiveStep("0", 3));
            setColspan(description, 2);
            setColspan(seats, 1);
            setColspan(directions, 3);


        }
        public void setRoom(Room room) {
            description.setValue(room.getBezeichnung());
            seats.setValue(String.valueOf(room.getWorkspaceList().size()));


        }
    }

    private static class WorkspaceDetailsFormLayout extends FormLayout {
        private final TextField description = new TextField("Description");
        private final TextField directions = new TextField("Directions");

        public WorkspaceDetailsFormLayout() {
            Stream.of(description, directions).forEach(field -> {
                field.setReadOnly(true);
                add(field);
            });

            setResponsiveSteps(new ResponsiveStep("0", 3));
            setColspan(description, 3);
            setColspan(directions, 3);

        }
        public void setWorkspace(Workspace workspace) {

        }
    }


    public Room getSelectedRoom() {
        return selectedRoom;
    }

    public void setSelectedRoom(Room selectedRoom) {
        this.selectedRoom = selectedRoom;
    }

    public H2 getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(H2 formTitle) {
        this.formTitle = formTitle;
    }
    public TextField getEntryTitle() {
        return entryTitle;
    }


    public void setEntryTitle(TextField entryTitle) {
        this.entryTitle = entryTitle;
    }

    public TextArea getDescription() {
        return description;
    }

    public void setDescription(TextArea description) {
        this.description = description;
    }

    public DateTimePicker getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(DateTimePicker startDateTime) {
        this.startDateTime = startDateTime;
    }

    public DateTimePicker getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(DateTimePicker endDateTime) {
        this.endDateTime = endDateTime;
    }
    public Button getSubmitButton(){return submitButton;}


    public Button getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }

    public DatePicker getStartDate() {
        return startDate;
    }

    public void setStartDate(DatePicker startDate) {
        this.startDate = startDate;
    }

    public DatePicker getEndDate() {
        return endDate;
    }

    public void setEndDate(DatePicker endDate) {
        this.endDate = endDate;
    }

    public Checkbox getRoomCheckbox() {
        return roomCheckbox;
    }

    public void setRoomCheckbox(Checkbox roomCheckbox) {
        this.roomCheckbox = roomCheckbox;
    }

    public RadioButtonGroup<String> getRadioButtonGroup() {
        return radioButtonGroup;
    }

    public void setRadioButtonGroup(RadioButtonGroup<String> radioButtonGroup) {
        this.radioButtonGroup = radioButtonGroup;
    }

    public Checkbox getWorkspaceCheckbox() {
        return workspaceCheckbox;
    }

    public void setWorkspaceCheckbox(Checkbox workspaceCheckbox) {
        this.workspaceCheckbox = workspaceCheckbox;
    }

    public MultiSelectComboBox<DayOfWeek> getReoccurringDays() {
        return reoccurringDays;
    }
    public Checkbox getRecurring() {
        return recurring;
    }
    public Button getEditSingularEntry() {
        return editSingularEntry;
    }

    public void setEditSingularEntry(Button editSingularEntry) {
        this.editSingularEntry = editSingularEntry;
    }

    public DatePicker getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(DatePicker entryDate) {
        this.entryDate = entryDate;
    }

    public Button getCheckAvailabilityButton() {
        return checkAvailabilityButton;
    }

    public void setCheckAvailabilityButton(Button checkAvailabilityButton) {
        this.checkAvailabilityButton = checkAvailabilityButton;
    }

    public Grid<Room> getRoomGrid() {
        return roomGrid;
    }

    public void setRoomGrid(Grid<Room> roomGrid) {
        this.roomGrid = roomGrid;
    }

    public Grid<Workspace> getWorkspaceGrid() {
        return workspaceGrid;
    }

    public void setWorkspaceGrid(Grid<Workspace> workspaceGrid) {
        this.workspaceGrid = workspaceGrid;
    }
    @SneakyThrows
    public Component getRoomImage(Room room) {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select image_name from bookingtool.room where id ='" + room.getId() +"'" ;
        ResultSet rs = statement.executeQuery(sql);
        String imageName = "";
        while (rs.next()) {
            imageName = rs.getString("image_name");
        }
        String finalImageName = imageName;
        StreamResource imageResource = new StreamResource(finalImageName,
                () -> getClass().getResourceAsStream("/public/"+ finalImageName));
        com.vaadin.flow.component.html.Image image = new Image(imageResource,"");
        image.setMaxHeight("80px");
        c.close();
        return image;
    }
    public Icon createColorIcon(String color){
        Icon icon = new Icon(VaadinIcon.CIRCLE);
        icon.setColor(color);
        return icon;
    }
}