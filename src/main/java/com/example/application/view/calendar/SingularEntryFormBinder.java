package com.example.application.view.calendar;

import com.example.application.entities.ResEntry;
import com.example.application.entities.Reservation;
import com.example.application.entities.Workspace;
import com.example.application.filter.TimeFilter;
import com.example.application.service.ResEntryService;
import com.example.application.service.ReservationService;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import org.vaadin.stefan.fullcalendar.Entry;

import java.sql.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class SingularEntryFormBinder {


    final SingularEntryCalendarEditorForm singularEntryCalendarEditorForm;

    private final ReservationService reservationService;

    private final ResEntryService resEntryService;


    public SingularEntryFormBinder(SingularEntryCalendarEditorForm singularEntryCalendarEditorForm, ReservationService reservationService, ResEntryService resEntryService) {
        this.singularEntryCalendarEditorForm = singularEntryCalendarEditorForm;
        this.reservationService = reservationService;
        this.resEntryService = resEntryService;

    }

    public void addBindingAndValidationToExistingResEntry(Entry entry) {

        Binder<ResEntry> resEntryBinder = new Binder<>(ResEntry.class);

        resEntryBinder.bindInstanceFields(singularEntryCalendarEditorForm);
        resEntryBinder.forField(singularEntryCalendarEditorForm.getEntryTitle()).bind("title");
        resEntryBinder.forField(singularEntryCalendarEditorForm.getDescription()).bind("description");
        resEntryBinder.forField(singularEntryCalendarEditorForm.getRoomCheckbox()).bind("onlyRoom").getField().addValueChangeListener(event -> {
            if (singularEntryCalendarEditorForm.getRoomCheckbox().getValue()) {
                singularEntryCalendarEditorForm.getWorkspaceCheckbox().setValue(false);
                singularEntryCalendarEditorForm.getRoomComboBox().setVisible(true);
            } else {
                singularEntryCalendarEditorForm.getRoomComboBox().setVisible(false);
                singularEntryCalendarEditorForm.getRoomComboBox().setValue(null);
            }

            singularEntryCalendarEditorForm.getWorkspaceMultiselectComboBox().setVisible(false);
        });
        resEntryBinder.forField(singularEntryCalendarEditorForm.getWorkspaceCheckbox()).bind("onlyWorkspace").getField().addValueChangeListener(event -> {
            if (singularEntryCalendarEditorForm.getWorkspaceCheckbox().getValue()) {
                singularEntryCalendarEditorForm.getRoomCheckbox().setValue(false);
                singularEntryCalendarEditorForm.getWorkspaceMultiselectComboBox().setVisible(true);
            } else {
                singularEntryCalendarEditorForm.getWorkspaceMultiselectComboBox().setVisible(false);
                singularEntryCalendarEditorForm.getWorkspaceMultiselectComboBox().setValue((Set<Workspace>) null);
            }
            singularEntryCalendarEditorForm.getRoomComboBox().setVisible(false);

        });

        resEntryBinder.forField(singularEntryCalendarEditorForm.getRoomComboBox()).bind("room");
        resEntryBinder.forField(singularEntryCalendarEditorForm.getWorkspaceMultiselectComboBox()).bind("workspaceList");
        resEntryBinder.forField(singularEntryCalendarEditorForm.getStartDateTime()).withValidator(startDateTime -> {
            boolean validWeekDay = startDateTime.getDayOfWeek().getValue() >= 1
                    && startDateTime.getDayOfWeek().getValue() <= 5;
            return validWeekDay;
        }, "The selected day of week is not available").withValidator(startDateTime -> {
            LocalTime startTime = LocalTime.of(startDateTime.getHour(), startDateTime.getMinute());
            boolean validTime = !(LocalTime.of(5, 0).isAfter(startTime)
                    || LocalTime.of(22, 0).isBefore(startTime));
            return validTime;
        }, "The selected time is not available").bind("startTime");

        resEntryBinder.forField(singularEntryCalendarEditorForm.getEndDateTime()).withValidator(endDateTime -> {
            boolean validWeekDay = endDateTime.getDayOfWeek().getValue() >= 1
                    && endDateTime.getDayOfWeek().getValue() <= 5;
            return validWeekDay;
        }, "The selected day of week is not available").withValidator(startDateTime -> {
            LocalTime startTime = LocalTime.of(startDateTime.getHour(), startDateTime.getMinute());
            boolean validTime = !(LocalTime.of(5, 0).isAfter(startTime)
                    || LocalTime.of(22, 0).isBefore(startTime));
            return validTime;
        }, "The selected time is not available").bind("endTime");

        singularEntryCalendarEditorForm.getRadioButtonGroup().addValueChangeListener(e -> {
            singularEntryCalendarEditorForm.getStartDateTime().setVisible(e.getValue().equals("Specific Time"));
            singularEntryCalendarEditorForm.getEndDateTime().setVisible(e.getValue().equals("Specific Time"));
            singularEntryCalendarEditorForm.getEndDate().setVisible(e.getValue().equals("Full Day"));
            singularEntryCalendarEditorForm.getStartDate().setVisible(e.getValue().equals("Full Day"));

        });

        singularEntryCalendarEditorForm.getSubmitButton().addClickListener(event -> {
            ResEntry resEntryBean = new ResEntry();

            try {

                resEntryBinder.writeBean(resEntryBean);
                ResEntry resEntryToUpdate = resEntryService.findResEntryByEntryId(entry.getId());
                if(singularEntryCalendarEditorForm.getRadioButtonGroup().getValue().equals("Full Day")){
                    resEntryBean.setStartTime(singularEntryCalendarEditorForm.getStartDate().getValue().atTime(5,0));
                    resEntryBean.setStartTime(singularEntryCalendarEditorForm.getEndDate().getValue().atTime(22,0));
                }
                resEntryToUpdate.setRoom(resEntryBean.getRoom());
                resEntryToUpdate.setWorkspaceList(resEntryBean.getWorkspaceList());
                resEntryToUpdate.setStartTime(resEntryBean.getStartTime());
                resEntryToUpdate.setEndTime(resEntryBean.getEndTime());
                resEntryToUpdate.setTitle(resEntryBean.getTitle());
                resEntryToUpdate.setDescription(resEntryBean.getDescription());
                resEntryToUpdate.setEntryID(entry.getId());
                if (resEntryBean.getRoom() != null) {
                    resEntryToUpdate.setColor(resEntryBean.getRoom().getRoomColor());
                }

                resEntryService.update(resEntryToUpdate);
            } catch (ValidationException e) {
                throw new RuntimeException(e);
            }

            showUpdateSuccess(resEntryBean);


        });
        singularEntryCalendarEditorForm.getCheckAvailabilityBtn().addClickListener(e -> {
                    resEntryBinder.validate();
                    boolean isAvailable = true;
                    TimeFilter timeFilter = new TimeFilter(resEntryService);
                    ResEntry resEntryBean = new ResEntry();
                    try {

                        resEntryBinder.writeBean(resEntryBean);
                        if(singularEntryCalendarEditorForm.getRadioButtonGroup().getValue().equals("Full Day")){
                            resEntryBean.setStartTime(singularEntryCalendarEditorForm.getStartDate().getValue().atTime(5,0));
                            resEntryBean.setStartTime(singularEntryCalendarEditorForm.getEndDate().getValue().atTime(22,0));
                        }
                        Reservation currentReservation = reservationService.findReservationByEntryId(entry.getId());
                        ResEntry oldResEntry = resEntryService.findResEntryByEntryId(entry.getId());
                        resEntryBean.setResEntryId(oldResEntry.getResEntryId());

                        if (!timeFilter.checkIfResAvailable(resEntryBean, currentReservation)) {
                            isAvailable = false;
                        }

                        if (isAvailable) {
                            openSuccessNotification(resEntryBean);
                            singularEntryCalendarEditorForm.getCheckAvailabilityBtn().setVisible(false);
                            singularEntryCalendarEditorForm.getSubmitButton().setVisible(true);
                            singularEntryCalendarEditorForm.getSubmitButton().addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                        } else {
                            openNotification(resEntryBean, timeFilter.getResEntryOccupying());
                            singularEntryCalendarEditorForm.getCheckAvailabilityBtn().addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                                    ButtonVariant.LUMO_ERROR);
                        }

                    } catch (ValidationException ex) {
                        throw new RuntimeException(ex);
                    }
                }
        );
        singularEntryCalendarEditorForm.getDeleteButton().addClickListener(event1 -> {

            ResEntry currentResEntry = resEntryService.findResEntryByEntryId(entry.getId());
            try {
                Reservation reservationToUpdate = reservationService.findReservationById((long) getReservationDetailsOfEntry(entry));
                Set<ResEntry> resEntrySet = new HashSet<>(reservationToUpdate.getResEntries());
                resEntrySet.removeIf(resEntry -> resEntry.getResEntryId().equals(currentResEntry.getResEntryId()));
                reservationToUpdate.setResEntries(resEntrySet);
                reservationService.update(reservationToUpdate);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            resEntryService.delete(currentResEntry);


        });
    }

    private void showUpdateSuccess(ResEntry resEntry) {

        Notification notification =
                Notification.show("Reservation updated: " + resEntry.getTitle());
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);

    }

    private int getReservationDetailsOfEntry(Entry entry) throws SQLException {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select reservation_id from bookingtool.reservation_entry_ids where entry_ids ='" + entry.getId() + "'";
        ResultSet rs = statement.executeQuery(sql);
        int resID = 0;
        while (rs.next()) {
            resID = rs.getInt("reservation_id");
        }
        c.close();
        return resID;
    }

    public void openSuccessNotification(ResEntry resEntry) {
        Paragraph p;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");
        if (resEntry.getRoom() != null) {
            p = new Paragraph("Room " + resEntry.getRoom().getBezeichnung()
                    + " is available from: " + resEntry.getStartTime().format(formatter) +
                    "\n" + " until: " + resEntry.getEndTime().format(formatter));
            p.setId("occupation");
            p.getStyle().set("color", "green");
            p.getStyle().set("white-space", "pre-line");
            singularEntryCalendarEditorForm.setColspan(p, 4);
            singularEntryCalendarEditorForm.add(p);
        }
        if (resEntry.getWorkspaceList() != null) {
            for (Workspace workspace : resEntry.getWorkspaceList()) {
                p = new Paragraph("Workspace: " + workspace.getBezeichnung()
                        + " is available from: " + resEntry.getStartTime().format(formatter) +
                        "\n" + " until: " + resEntry.getEndTime().format(formatter));
                p.setId("occupation");
                p.getStyle().set("color", "green");
                p.getStyle().set("white-space", "pre-line");
                singularEntryCalendarEditorForm.setColspan(p, 4);
                singularEntryCalendarEditorForm.add(p);
            }

        } else {

        }


        singularEntryCalendarEditorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("occupation"))) {
                singularEntryCalendarEditorForm.remove(child);
            }

        });


        Notification notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
        notification.setDuration(0);

        Div text = new Div(new Text("Reservation available"));
        com.vaadin.flow.component.button.Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> {
            notification.close();
        });

        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        notification.setPosition(Notification.Position.BOTTOM_END);
        notification.add(layout);
        notification.open();

    }

    public void openNotification(ResEntry resEntry, ResEntry exResEntry) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");
        Paragraph p;
        if (resEntry.getRoom() != null) {
            p = new Paragraph("Room " + resEntry.getRoom().getBezeichnung() + " is occupied and not available from: " + exResEntry.getStartTime().format(formatter) + "\n" + " until: " + exResEntry.getEndTime().format(formatter));
        } else {

            List<Workspace> workspaceList = getOccupyingWorkspaces(resEntry, exResEntry);
            String result = workspaceList.stream()
                    .map(n -> String.valueOf(n))
                    .collect(Collectors.joining(",", "(", ")"));
            p = new Paragraph("Workspace(s) " + result + " occupied and not available from: " + exResEntry.getStartTime().format(formatter) + "\n" + " until: " + exResEntry.getEndTime().format(formatter));

        }
        singularEntryCalendarEditorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("occupation"))) {
                singularEntryCalendarEditorForm.remove(child);
            }

        });
        p.setId("occupation");
        p.getStyle().set("color", "red");
        p.getStyle().set("white-space", "pre-line");
        singularEntryCalendarEditorForm.setColspan(p, 4);
        singularEntryCalendarEditorForm.add(p);
        Notification notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        Div text = new Div(new Text("Reservation failed because of: " + exResEntry.getTitle()));
        com.vaadin.flow.component.button.Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> {
            notification.close();
        });
        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        notification.setPosition(Notification.Position.BOTTOM_END);
        notification.add(layout);
        notification.open();

    }

    private List<Workspace> getOccupyingWorkspaces(ResEntry resEntry, ResEntry exResEntry) {
        List<Workspace> filteredWorkspaces = new ArrayList<>();
        for (Workspace workspace : resEntry.getWorkspaceList()) {
            for (Workspace existWorkspace : exResEntry.getWorkspaceList()) {
                if (Objects.equals(workspace.getId(), existWorkspace.getId())) {
                    filteredWorkspaces.add(workspace);
                }
            }

        }
        return filteredWorkspaces;
    }
}
