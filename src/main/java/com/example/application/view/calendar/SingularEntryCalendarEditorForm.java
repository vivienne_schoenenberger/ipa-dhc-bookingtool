package com.example.application.view.calendar;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.service.RoomService;
import com.example.application.service.WorkspaceService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;


import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class SingularEntryCalendarEditorForm extends FormLayout {
    private H2 formTitle;
    private TextField entryTitle;
    private TextArea description;
    private DateTimePicker startDateTime;
    private DateTimePicker endDateTime;
    private List<Room> roomList;
    private MultiSelectComboBox<Workspace> workspaceMultiselectComboBox;
    private List<Workspace> workspaceList;
    private ComboBox<Room> roomComboBox;
    private final Button submitButton;
    private final Button cancelButton;
    private final Button checkAvailabilityBtn;
    private Button deleteButton;
    private Checkbox roomCheckbox;
    private Checkbox workspaceCheckbox;
    private DatePicker startDate;
    private DatePicker endDate;
    private RadioButtonGroup<String> radioButtonGroup;
    public SingularEntryCalendarEditorForm(RoomService roomService, WorkspaceService workspaceService) {
        formTitle= new H2("Edit singular entry");
        formTitle.getStyle().set("color","#00A9C5");
        entryTitle= new TextField("Entry title");
        description= new TextArea("Description");
        radioButtonGroup = new RadioButtonGroup<>();
        radioButtonGroup.setItems("Full Day","Specific Time");
        startDateTime = new DateTimePicker("Start");
        startDateTime.setMin(LocalDateTime.now());
        startDateTime.setMax(LocalDateTime.now().plusYears(1));
        startDateTime.setStep(Duration.ofMinutes((30)));
        endDateTime = new DateTimePicker("Until");
        endDateTime.setMax(LocalDateTime.now().plusYears(1));
        endDateTime.setHelperText("Available every day between 5:00AM - 10:00PM");
        startDate = new DatePicker("Start");
        startDate.setMin(LocalDate.now());
        startDate.setMax(LocalDate.now().plusYears(1));
        startDate.setVisible(false);
        endDate = new DatePicker("Until");
        endDate.setMax(LocalDate.now().plusYears(1));
        endDate.setMin(LocalDate.now());
        endDate.setVisible(false);
        roomList= (List<Room>) roomService.findAll();
        roomComboBox = new ComboBox<>("Rooms");
        roomComboBox.setItems(roomList);
        roomComboBox.setItemLabelGenerator(Room::getBezeichnung);
        workspaceList = (List<Workspace>) workspaceService.findAll();
        workspaceMultiselectComboBox = new MultiSelectComboBox<>("Workspaces");
        workspaceMultiselectComboBox.setItems(workspaceList);
        workspaceMultiselectComboBox.setItemLabelGenerator(Workspace::getBezeichnung);
        submitButton = new Button("Save");
        submitButton.addThemeVariants(ButtonVariant.LUMO_SUCCESS);
        submitButton.setVisible(false);
        checkAvailabilityBtn = new Button("Check Availability");
        checkAvailabilityBtn.addThemeVariants((ButtonVariant.LUMO_PRIMARY));
        cancelButton = new Button("Cancel");
        deleteButton = new Button("Delete");
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        roomCheckbox = new Checkbox("Rooms");
        workspaceCheckbox = new Checkbox("Workspaces");

        add(
                formTitle,
                entryTitle,
                description,
                radioButtonGroup,
                startDate,
                endDate,
                startDateTime,
                endDateTime,
                roomCheckbox,
                workspaceCheckbox,
                roomComboBox,
                workspaceMultiselectComboBox,
                checkAvailabilityBtn,
                submitButton,
                cancelButton,
                deleteButton
        );
        setMaxWidth("500px");
    }
    public H2 getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(H2 formTitle) {
        this.formTitle = formTitle;
    }

    public TextField getEntryTitle() {
        return entryTitle;
    }

    public void setEntryTitle(TextField entryTitle) {
        this.entryTitle = entryTitle;
    }

    public TextArea getDescription() {
        return description;
    }

    public void setDescription(TextArea description) {
        this.description = description;
    }

    public DateTimePicker getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(DateTimePicker startDateTime) {
        this.startDateTime = startDateTime;
    }

    public DateTimePicker getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(DateTimePicker endDateTime) {
        this.endDateTime = endDateTime;
    }

    public List<Room> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<Room> roomList) {
        this.roomList = roomList;
    }

    public MultiSelectComboBox<Workspace> getWorkspaceMultiselectComboBox() {
        return workspaceMultiselectComboBox;
    }

    public void setWorkspaceMultiselectComboBox(MultiSelectComboBox<Workspace> workspaceMultiselectComboBox) {
        this.workspaceMultiselectComboBox = workspaceMultiselectComboBox;
    }

    public List<Workspace> getWorkspaceList() {
        return workspaceList;
    }

    public void setWorkspaceList(List<Workspace> workspaceList) {
        this.workspaceList = workspaceList;
    }

    public ComboBox<Room> getRoomComboBox() {
        return roomComboBox;
    }

    public void setRoomComboBox(ComboBox<Room> roomComboBox) {
        this.roomComboBox = roomComboBox;
    }

    public Button getSubmitButton() {
        return submitButton;
    }

    public Button getCancelButton() {
        return cancelButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }
    public Button getCheckAvailabilityBtn() {
        return checkAvailabilityBtn;
    }

    public Checkbox getRoomCheckbox() {
        return roomCheckbox;
    }

    public void setRoomCheckbox(Checkbox roomCheckbox) {
        this.roomCheckbox = roomCheckbox;
    }

    public Checkbox getWorkspaceCheckbox() {
        return workspaceCheckbox;
    }

    public void setWorkspaceCheckbox(Checkbox workspaceCheckbox) {
        this.workspaceCheckbox = workspaceCheckbox;
    }

    public DatePicker getStartDate() {
        return startDate;
    }

    public void setStartDate(DatePicker startDate) {
        this.startDate = startDate;
    }

    public DatePicker getEndDate() {
        return endDate;
    }

    public void setEndDate(DatePicker endDate) {
        this.endDate = endDate;
    }

    public RadioButtonGroup<String> getRadioButtonGroup() {
        return radioButtonGroup;
    }

    public void setRadioButtonGroup(RadioButtonGroup<String> radioButtonGroup) {
        this.radioButtonGroup = radioButtonGroup;
    }
}
