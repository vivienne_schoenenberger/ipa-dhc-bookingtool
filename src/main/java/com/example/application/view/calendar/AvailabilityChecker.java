package com.example.application.view.calendar;

import com.example.application.entities.ResEntry;
import com.example.application.entities.Reservation;
import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.filter.ResEntryConverter;
import com.example.application.filter.TimeFilter;
import com.example.application.service.ResEntryService;
import com.example.application.service.ReservationService;
import com.example.application.service.RoomService;
import com.example.application.service.WorkspaceService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.server.StreamResource;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.*;

public class AvailabilityChecker extends VerticalLayout {


    private DateTimePicker startDateTime;
    private DatePicker startDate;
    private DateTimePicker endDateTime;
    private DatePicker endDate;
    private Checkbox recurring;
    private Button checkAvailabilityButton;
    private RadioButtonGroup<String> radioButtonGroup;
    private final MultiSelectComboBox<DayOfWeek> reoccurringDays;
    private Grid<Room> roomGrid;
    private Grid<Workspace> workspaceGrid;
    private Checkbox roomCheckbox;
    private Checkbox workspaceCheckbox;


    public AvailabilityChecker(RoomService roomService, WorkspaceService workspaceService, ReservationService reservationService, ResEntryService resEntryService) {
        startDateTime = new DateTimePicker("Start");
        startDate = new DatePicker("Start");
        startDate.setVisible(false);
        endDateTime = new DateTimePicker("End");
        endDate = new DatePicker("End");
        endDate.setVisible(false);
        recurring = new Checkbox("Recurrence");
        checkAvailabilityButton = new Button("Check Availability");
        reoccurringDays= new MultiSelectComboBox<>("Reoccurring Day");
        reoccurringDays.setVisible(false);
        List<DayOfWeek> week = new ArrayList<>();
        week.add(DayOfWeek.MONDAY);
        week.add(DayOfWeek.TUESDAY);
        week.add(DayOfWeek.WEDNESDAY);
        week.add(DayOfWeek.THURSDAY);
        week.add(DayOfWeek.FRIDAY);
        week.add(DayOfWeek.SATURDAY);
        reoccurringDays.setItems(week);
        radioButtonGroup = new RadioButtonGroup<>("Specify date");
        radioButtonGroup.setItems("Specific Time","All Day");
        radioButtonGroup.setValue("Specific Time");
        radioButtonGroup.addValueChangeListener(event -> {
            switch (radioButtonGroup.getValue()){
                case "All Day":
                    startDateTime.setVisible(false);
                    startDate.setVisible(true);
                    endDateTime.setVisible(false);
                    endDate.setVisible(true);

                    break;
                case "Specific Time":
                    startDateTime.setVisible(true);
                    endDateTime.setVisible(true);
                    startDate.setVisible(false);
                    endDate.setVisible(false);

                    break;

            }
        });
        recurring.addClickListener(event -> {
            reoccurringDays.setVisible(recurring.getValue());

        });
        roomCheckbox = new Checkbox("Rooms");
        workspaceCheckbox = new Checkbox("Workspaces");
        roomGrid = new Grid<>(Room.class);
        roomGrid.setVisible(false);
        roomGrid.removeColumnByKey("id");
        roomGrid.removeColumnByKey("workspaceList");
        roomGrid.removeColumnByKey("bezeichnung");
        roomGrid.removeColumnByKey("roomColor");
        roomGrid.removeColumnByKey("imageName");
        roomGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        roomGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        roomGrid.addColumn(Room::getId).setHeader("");
        roomGrid.addComponentColumn(this::getRoomImage).setHeader("");
        roomGrid.addComponentColumn(room -> createColorIcon(room.getRoomColor())).setHeader("Color");
        roomGrid.addColumn(Room::getBezeichnung).setHeader("Bezeichnung");
        roomGrid.addColumn(Room::getWorkspaceList).setHeader("Workspace List");



        roomCheckbox.addClickListener(checkboxClickEvent -> {
            roomGrid.setVisible(roomCheckbox.getValue());
            if(startDate!=null&&endDate!=null&&startDateTime.getValue()==null&&endDateTime.getValue()==null){
                startDateTime.setValue(startDate.getValue().atStartOfDay().plusHours(5));
                endDateTime.setValue(endDate.getValue().atTime(22,0));
            }
            if(startDateTime.getValue()!=null&&endDateTime.getValue()!=null) {

                roomGrid.setItems(getAvailableRooms(recurring.getValue(),reoccurringDays.getSelectedItems(), radioButtonGroup.getValue(), startDateTime.getValue(), endDateTime.getValue(), roomService, reservationService, resEntryService));
            }
            else {
                Collection<Room> rooms = roomService.findAll();
                roomGrid.setItems(rooms);
            }
        });
        workspaceCheckbox.addClickListener(checkboxClickEvent -> {
            workspaceGrid.setVisible(workspaceCheckbox.getValue());
            if(startDate!=null&&endDate!=null&&startDateTime.getValue()==null&&endDateTime.getValue()==null){
                startDateTime.setValue(startDate.getValue().atStartOfDay().plusHours(5));
                endDateTime.setValue(endDate.getValue().atTime(22,0));
            }
            if(startDateTime.getValue()!=null&&endDateTime.getValue()!=null){
                workspaceGrid.setItems(getAvailableWorkspaces(recurring.getValue(),reoccurringDays.getSelectedItems(),radioButtonGroup.getValue(),startDateTime.getValue(),endDateTime.getValue(),workspaceService,reservationService,resEntryService));
            }
        });
        startDateTime.addValueChangeListener(e->{
            if(endDateTime.getValue()!=null && roomCheckbox.getValue()!=null)
                roomGrid.setItems(getAvailableRooms(recurring.getValue(),reoccurringDays.getSelectedItems(), radioButtonGroup.getValue(), startDateTime.getValue(), endDateTime.getValue(), roomService, reservationService, resEntryService));
            if(endDateTime.getValue()!=null && workspaceCheckbox.getValue()!=null)
                workspaceGrid.setItems(getAvailableWorkspaces(recurring.getValue(),reoccurringDays.getSelectedItems(),radioButtonGroup.getValue(),startDateTime.getValue(),endDateTime.getValue(),workspaceService,reservationService,resEntryService));
        });
        endDateTime.addValueChangeListener(e->{
            if(startDateTime.getValue()!=null && roomCheckbox.getValue()!=null)
                roomGrid.setItems(getAvailableRooms(recurring.getValue(),reoccurringDays.getSelectedItems(), radioButtonGroup.getValue(), startDateTime.getValue(), endDateTime.getValue(), roomService, reservationService, resEntryService));
            if(startDateTime.getValue()!=null && workspaceCheckbox.getValue()!=null)
                workspaceGrid.setItems(getAvailableWorkspaces(recurring.getValue(),reoccurringDays.getSelectedItems(),radioButtonGroup.getValue(),startDateTime.getValue(),endDateTime.getValue(),workspaceService,reservationService,resEntryService));
        });
        roomGrid.addSelectionListener(selection -> {
            // System.out.printf("Number of selected people: %s%n",
            // selection.getAllSelectedItems().size());
        });
        startDate.addValueChangeListener(e->{
            if(endDate.getValue()!=null && roomCheckbox.getValue()!=null)
                roomGrid.setItems(getAvailableRooms(recurring.getValue(),reoccurringDays.getSelectedItems(), radioButtonGroup.getValue(), startDate.getValue().atStartOfDay(),endDate.getValue().atTime(22,0), roomService, reservationService, resEntryService));
            if(endDate.getValue()!=null && workspaceCheckbox.getValue()!=null)
                workspaceGrid.setItems(getAvailableWorkspaces(recurring.getValue(),reoccurringDays.getSelectedItems(),radioButtonGroup.getValue(),startDate.getValue().atStartOfDay(),endDate.getValue().atTime(22,0),workspaceService,reservationService,resEntryService));
        });
        endDate.addValueChangeListener(e->{
            if(startDate.getValue()!=null && roomCheckbox.getValue()!=null)
                roomGrid.setItems(getAvailableRooms(recurring.getValue(),reoccurringDays.getSelectedItems(), radioButtonGroup.getValue(), startDate.getValue().atStartOfDay(), endDate.getValue().atTime(22,0), roomService, reservationService, resEntryService));
            if(startDate.getValue()!=null && workspaceCheckbox.getValue()!=null)
                workspaceGrid.setItems(getAvailableWorkspaces(recurring.getValue(),reoccurringDays.getSelectedItems(),radioButtonGroup.getValue(),startDate.getValue().atStartOfDay(),endDate.getValue().atTime(22,0),workspaceService,reservationService,resEntryService));
        });
        reoccurringDays.addSelectionListener(e->{
            if(radioButtonGroup.getValue().equals("Specific Time")){
                if(startDateTime.getValue()!=null&&endDateTime!=null){
                    if(roomCheckbox.getValue())
                        roomGrid.setItems(getAvailableRooms(recurring.getValue(),reoccurringDays.getSelectedItems(), radioButtonGroup.getValue(), startDateTime.getValue(), endDateTime.getValue(), roomService, reservationService, resEntryService));
                    if(workspaceCheckbox.getValue())
                        workspaceGrid.setItems(getAvailableWorkspaces(recurring.getValue(),reoccurringDays.getSelectedItems(),radioButtonGroup.getValue(),startDateTime.getValue(),endDateTime.getValue(),workspaceService,reservationService,resEntryService));
                }
            }
            else{
                if(roomCheckbox.getValue())
                    roomGrid.setItems(getAvailableRooms(recurring.getValue(),reoccurringDays.getSelectedItems(), radioButtonGroup.getValue(), startDate.getValue().atStartOfDay(), endDate.getValue().atTime(22,0), roomService, reservationService, resEntryService));
                if(workspaceCheckbox.getValue())
                    workspaceGrid.setItems(getAvailableWorkspaces(recurring.getValue(),reoccurringDays.getSelectedItems(),radioButtonGroup.getValue(),startDate.getValue().atStartOfDay(),endDate.getValue().atTime(22,0),workspaceService,reservationService,resEntryService));
            }
        });

        workspaceGrid = new Grid<>(Workspace.class);
        workspaceGrid.setVisible(false);
        workspaceGrid.removeColumnByKey("id");
        workspaceGrid.removeColumnByKey("workspaceColor");
        workspaceGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        workspaceGrid.addColumn(Workspace::getBezeichnung);
        workspaceGrid.addColumn(Workspace::getRoom);
        Collection<Workspace> workspaces = workspaceService.findAll();
        workspaceGrid.setItems(workspaces);


        add(radioButtonGroup,startDate,startDateTime,endDate,endDateTime,recurring,reoccurringDays,roomCheckbox,workspaceCheckbox,roomGrid,workspaceGrid,checkAvailabilityButton);
        setWidth("800px");

    }
    @SneakyThrows
    public Component getRoomImage(Room room) {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select image_name from bookingtool.room where id ='" + room.getId() +"'" ;
        ResultSet rs = statement.executeQuery(sql);
        String imageName = "";
        while (rs.next()) {
            imageName = rs.getString("image_name");
        }
        String finalImageName = imageName;
        StreamResource imageResource = new StreamResource(finalImageName,
                () -> getClass().getResourceAsStream("/public/"+ finalImageName));
        Image image = new Image(imageResource,"");
        image.setMaxHeight("80px");
        c.close();
        return image;
    }

    public Icon createColorIcon(String color){
        Icon icon = new Icon(VaadinIcon.CIRCLE);
        icon.setColor(color);
        return icon;
    }
    public DateTimePicker getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(DateTimePicker startDateTime) {
        this.startDateTime = startDateTime;
    }

    public DatePicker getStartDate() {
        return startDate;
    }

    public void setStartDate(DatePicker startDate) {
        this.startDate = startDate;
    }

    public DateTimePicker getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(DateTimePicker endDateTime) {
        this.endDateTime = endDateTime;
    }

    public DatePicker getEndDate() {
        return endDate;
    }

    public void setEndDate(DatePicker endDate) {
        this.endDate = endDate;
    }

    public Checkbox getRecurring() {
        return recurring;
    }

    public void setRecurring(Checkbox recurring) {
        this.recurring = recurring;
    }

    public Button getCheckAvailabilityButton() {
        return checkAvailabilityButton;
    }

    public void setCheckAvailabilityButton(Button checkAvailabilityButton) {
        this.checkAvailabilityButton = checkAvailabilityButton;
    }

    public RadioButtonGroup<String> getRadioButtonGroup() {
        return radioButtonGroup;
    }

    public void setRadioButtonGroup(RadioButtonGroup<String> radioButtonGroup) {
        this.radioButtonGroup = radioButtonGroup;
    }

    public MultiSelectComboBox<DayOfWeek> getReoccurringDays() {
        return reoccurringDays;
    }

    public Grid<Room> getRoomGrid() {
        return roomGrid;
    }

    public void setRoomGrid(Grid<Room> roomGrid) {
        this.roomGrid = roomGrid;
    }

    public Checkbox getRoomCheckbox() {
        return roomCheckbox;
    }

    public void setRoomCheckbox(Checkbox roomCheckbox) {
        this.roomCheckbox = roomCheckbox;
    }

    public Checkbox getWorkspaceCheckbox() {
        return workspaceCheckbox;
    }

    public void setWorkspaceCheckbox(Checkbox workspaceCheckbox) {
        this.workspaceCheckbox = workspaceCheckbox;
    }

    public Collection<Room> getAvailableRooms(boolean isRecurring,Set<DayOfWeek> reoccurringDays, String radioBoxResult,LocalDateTime start, LocalDateTime end,RoomService roomService, ReservationService reservationService, ResEntryService resEntryService){
        Collection<Room>roomCollection = roomService.findAll();
        Reservation possibleRes = new Reservation();
        Collection<Room>filteredRooms = new ArrayList<>();
        boolean isSpecificTime;
        isSpecificTime= radioBoxResult.equals("Specific Time");
        for(Room room :roomCollection){

            possibleRes.setRoom(room);
            possibleRes.setStart_time(start);
            possibleRes.setStart_date(start.toLocalDate());
            possibleRes.setEnd_time(end);
            possibleRes.setEnd_date(end.toLocalDate());
            possibleRes.setSpecificTime(isSpecificTime);
            possibleRes.setRecurring(isRecurring);
            possibleRes.setReoccurringDays(reoccurringDays);
            if (checkIfResAvailable(possibleRes,reservationService,resEntryService)){
                filteredRooms.add(possibleRes.getRoom());
            }
        }

        return filteredRooms;
    }
    public Collection<Workspace> getAvailableWorkspaces(boolean isRecurring,Set<DayOfWeek> reoccurringDays, String radioBoxResult,LocalDateTime start, LocalDateTime end,WorkspaceService workspaceService, ReservationService reservationService, ResEntryService resEntryService){
        Set<Workspace>workspaceCollection = new HashSet<>(workspaceService.findAll());
        Reservation possibleRes = new Reservation();
        Set<Workspace>filteredWorkspaces = new HashSet<>();
        boolean isSpecificTime;
        isSpecificTime= radioBoxResult.equals("Specific Time");
        //check for each existing workspace
        for(Workspace workspace :workspaceCollection) {
            Set<Workspace>workspaceSet= new HashSet<>();
            workspaceSet.add(workspace);
            possibleRes.setWorkspaceList(workspaceSet);
            possibleRes.setStart_time(start);
            possibleRes.setStart_date(start.toLocalDate());
            possibleRes.setEnd_time(end);
            possibleRes.setEnd_date(end.toLocalDate());
            possibleRes.setSpecificTime(isSpecificTime);
            possibleRes.setRecurring(isRecurring);
            possibleRes.setReoccurringDays(reoccurringDays);
            if (checkIfResAvailable(possibleRes, reservationService, resEntryService)) {
                filteredWorkspaces.addAll(possibleRes.getWorkspaceList());
            }
        }
        return filteredWorkspaces;
    }
    public boolean checkIfResAvailable(Reservation reservationToCheck,ReservationService reservationService, ResEntryService resEntryService){
        ResEntryConverter resEntryConverter = new ResEntryConverter(reservationService,resEntryService);
        TimeFilter timeFilter = new TimeFilter(resEntryService);
        List<Boolean>checkAll= new ArrayList<>();

            List<ResEntry>resEntries= resEntryConverter.convertResToEntry(reservationToCheck);
            for(ResEntry resEntry: resEntries){
                if(!timeFilter.checkIfResAvailable(resEntry,reservationToCheck)){
                    checkAll.add(false);
                }
                else checkAll.add(true);

            }


        return !checkAll.contains(false);
    }

}
