package com.example.application.view.calendar;

import com.example.application.entities.*;
import com.example.application.filter.ResEntryConverter;
import com.example.application.filter.TimeFilter;
import com.example.application.security.MyUserPrincipal;
import com.example.application.service.*;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class CalendarFormBinder {

    final CalendarForm editorForm;
    final SingularEntryCalendarEditorForm singularEntryCalendarEditorForm;
    private final ReservationService reservationService;

    private final ResEntryService resEntryService;
    private final UserService userService;
    boolean ReservationSuccessful;
    Notification notification;
    private Room selectedRoom;
    private Set<Workspace> selectedWorkspaces;
    private ResEntry newResEntry;

    private  TimeFilter timeFilter;
    private  ResEntryConverter resEntryConverter;
    private final RoomService roomService;
    private final WorkspaceService workspaceService;

    float totalPriceRoom;
    float totalPriceWorkspaces;
    Long totalTimeWithoutWeekend = 0L;
    float totalCalcTime =0;

    public CalendarFormBinder(CalendarForm editorForm, SingularEntryCalendarEditorForm singularEntryCalendarEditorForm, ReservationService reservationService, UserService userService, ResEntryService resEntryService,RoomService roomService, WorkspaceService workspaceService) {
        this.editorForm = editorForm;
        this.singularEntryCalendarEditorForm = singularEntryCalendarEditorForm;
        this.reservationService = reservationService;
        this.userService = userService;
        this.resEntryService = resEntryService;
        this.roomService= roomService;
        this.workspaceService= workspaceService;

    }

    public void setDefaultDate(LocalDateTime start, LocalDateTime end) {

        editorForm.getStartDateTime().setValue(start);
        editorForm.getEndDateTime().setValue(end);
        editorForm.getStartDate().setValue(start.toLocalDate());
        editorForm.getEndDate().setValue(end.toLocalDate());
        editorForm.getRoomGrid().setItems(getInitAvailableRooms(roomService,start,end));
        editorForm.getWorkspaceGrid().setItems(getInitAvailableWorkspaces(workspaceService,start,end));

    }

    public void addBindingAndValidation() {

        editorForm.getSubmitButton().setVisible(false);
        Binder<Reservation>reservationBinder= new Binder<>(Reservation.class);
        reservationBinder.bindInstanceFields(editorForm);
        AtomicBoolean isSpecificTime = new AtomicBoolean(true);
        editorForm.getRadioButtonGroup().addValueChangeListener(e->
                isSpecificTime.set(e.getValue().equals("Specific Time")));

        reservationBinder.forField(editorForm.getEntryTitle()).withValidator(title->{
            if(Objects.equals(title, ""))
                return false;
            else return true;
        },"This field can't be empty").bind("title");
        reservationBinder.forField(editorForm.getDescription()).withValidator(description->{
            if(Objects.equals(description, ""))
                return false;
            else return true;
        },"This field can't be empty").bind("description");
        reservationBinder.forField(editorForm.getStartDateTime()).withValidator(startDateTime->{
            if (isSpecificTime.get()) {
                boolean validWeekDay = startDateTime.getDayOfWeek().getValue() >= 1
                        && startDateTime.getDayOfWeek().getValue() <= 5;
                return validWeekDay;
            }
            else return true;
        }, "The selected day of week is not available").withValidator(startDateTime->{
            if (isSpecificTime.get()) {
                boolean validDate = !startDateTime.isBefore(LocalDateTime.now());
                return validDate;
            }
            else return true;
        }, "The selected date is in the past").withValidator(startDateTime -> {
            if (isSpecificTime.get()) {
                LocalTime startTime = LocalTime.of(startDateTime.getHour(), startDateTime.getMinute());
                boolean validTime = !(LocalTime.of(5, 0).isAfter(startTime)
                        || LocalTime.of(22, 0).isBefore(startTime));
                return validTime;
            }
            else return true;
        }, "The selected time is not available").bind("start_time").getField().addValueChangeListener(event -> {
            editorForm.getSubmitButton().setVisible(false);
            editorForm.getCheckAvailabilityButton().setVisible(true);
        });

        reservationBinder.forField(editorForm.getStartDate()).withValidator(startDate->{
            if (!isSpecificTime.get()) {
                boolean validWeekDay = startDate.getDayOfWeek().getValue() >= 1
                        && startDate.getDayOfWeek().getValue() <= 5;
                return validWeekDay;
            }
            else return true;
        }, "The selected day of week is not available").withValidator(startDate->{
            if (!isSpecificTime.get()) {
                boolean validDate = !startDate.isBefore(LocalDate.now());
                return validDate;
            }
            else return true;
        }, "The selected date is in the past").bind("start_date").getField().addValueChangeListener(event -> {
            editorForm.getSubmitButton().setVisible(false);
            editorForm.getCheckAvailabilityButton().setVisible(true);
        });
        reservationBinder.forField(editorForm.getEndDate()).withValidator(endDate->{
            if (!isSpecificTime.get()) {
                boolean validWeekDay = endDate.getDayOfWeek().getValue() >= 1
                        && endDate.getDayOfWeek().getValue() <= 5;
                return validWeekDay;
            }
            else return true;
        }, "The selected day of week is not available").withValidator(endDate->{
            if (!isSpecificTime.get()) {
                boolean validDate = !endDate.isBefore(LocalDate.now());
                return validDate;
            }
            else return true;
        }, "The selected date is in the past").bind("end_date").getField().addValueChangeListener(event -> {
            editorForm.getSubmitButton().setVisible(false);
            editorForm.getCheckAvailabilityButton().setVisible(true);
        });

        reservationBinder.forField(editorForm.getEndDateTime()).withValidator(endDateTime->{
            if (isSpecificTime.get()) {
                boolean validWeekDay = endDateTime.getDayOfWeek().getValue() >= 1
                        && endDateTime.getDayOfWeek().getValue() <= 5 ;
                return validWeekDay;
            }
            else return true;
        }, "The selected day of week is not available").withValidator(endDateTime->{
            if (isSpecificTime.get()) {
                boolean validDate = !endDateTime.isBefore(LocalDateTime.now());
                return validDate;
            }
            else return true;
        }, "The selected date is in the past").withValidator(endDateTime -> {
            if (isSpecificTime.get()) {
                LocalTime startTime = LocalTime.of(endDateTime.getHour(), endDateTime.getMinute());
                boolean validTime = !(LocalTime.of(5, 0).isAfter(startTime)
                        || LocalTime.of(22, 0).isBefore(startTime));
                return validTime;
            }
            else return true;
        }, "The selected time is not available").bind("end_time").getField().addValueChangeListener(event -> {
                editorForm.getSubmitButton().setVisible(false);
                editorForm.getCheckAvailabilityButton().setVisible(true);

        });

        reservationBinder.forField(editorForm.getReoccurringDays()).withValidator(recur -> {
            if(editorForm.getRecurring().getValue())
                return recur.size()>0;
            else return true;
        }, "Select a repeating day").bind("reoccurringDays").getField().addValueChangeListener(event -> {
            editorForm.getSubmitButton().setVisible(false);
            editorForm.getCheckAvailabilityButton().setVisible(true);
        });
        reservationBinder.forField(editorForm.getRoomCheckbox()).bind("onlyRoom").getField().addValueChangeListener(event -> {
            if(editorForm.getRoomCheckbox().getValue()) {
                editorForm.getWorkspaceCheckbox().setValue(false);
                editorForm.getRoomGrid().setVisible(true);
                editorForm.getWorkspaceGrid().setVisible(false);
                editorForm.getWorkspaceGrid().deselectAll();
            }
            else {
                editorForm.getRoomGrid().setVisible(false);
                editorForm.getWorkspaceGrid().setVisible(true);
                editorForm.getRoomGrid().deselectAll();
            }

                });

        reservationBinder.forField(editorForm.getWorkspaceCheckbox()).bind("onlyWorkspace").getField().addValueChangeListener(event -> {
            if(editorForm.getWorkspaceCheckbox().getValue()) {
                editorForm.getRoomCheckbox().setValue(false);
                editorForm.getWorkspaceGrid().setVisible(true);
                editorForm.getRoomGrid().setVisible(false);
                editorForm.getRoomGrid().deselectAll();
            }
            else {
                editorForm.getWorkspaceGrid().setVisible(false);
                editorForm.getRoomGrid().setVisible(true);
                editorForm.getWorkspaceGrid().deselectAll();
            }

        });
        editorForm.getRoomGrid().addSelectionListener(selection -> {
            selectedRoom = selection.getFirstSelectedItem().get();

        });
        editorForm.getWorkspaceGrid().addSelectionListener(selectionEvent -> {
            selectedWorkspaces= selectionEvent.getAllSelectedItems();


        });

        editorForm.getCheckAvailabilityButton().addClickListener(event -> {
            reservationBinder.validate();
            boolean isAvailable=true;
            TimeFilter timeFilter = new TimeFilter(resEntryService);

            Reservation reservationBean = new Reservation();
            ResEntryConverter resEntryConverter = new ResEntryConverter(reservationService, resEntryService);
                try {
                        reservationBinder.writeBean(reservationBean);
                        if(reservationBean.isOnlyWorkspace()) {
                            reservationBean.setWorkspaceList(selectedWorkspaces);
                        }
                        else reservationBean.setRoom(selectedRoom);
                        if (Objects.equals(editorForm.getRadioButtonGroup().getValue(), "Full Day")) {
                            reservationBean.setFullDay(true);
                            reservationBean.setSpecificTime(false);
                            reservationBean.setEnd_time(editorForm.getEndDate().getValue().atTime(22,0));
                            reservationBean.setStart_time(editorForm.getStartDate().getValue().atTime(5,0));
                        } else {
                            reservationBean.setFullDay(false);
                            reservationBean.setSpecificTime(true);
                            reservationBean.setEnd_date(editorForm.getEndDateTime().getValue().toLocalDate());
                            reservationBean.setStart_date(editorForm.getStartDateTime().getValue().toLocalDate());
                        }
                        List<ResEntry> convertedResEntries= resEntryConverter.convertResToEntry(reservationBean);
                        if(convertedResEntries.size()>0) {
                            editorForm.getChildren().forEach(child -> {
                                if (child.getId().equals(Optional.of("repDays"))) {
                                    editorForm.remove(child);
                                }

                            });
                            if(selectedRoom==null&&selectedWorkspaces==null){
                                nothingSelectedErrorMsg();
                                isAvailable=false;
                                editorForm.getCheckAvailabilityButton().addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                                        ButtonVariant.LUMO_ERROR);
                            } else {
                                editorForm.getChildren().forEach(child -> {
                                    if (child.getId().equals(Optional.of("selectionError"))) {
                                        editorForm.remove(child);
                                    }});
                            }

                            for (ResEntry resEntry : convertedResEntries) {
                                newResEntry = resEntry;
                                if (!timeFilter.checkIfResAvailable(resEntry,reservationBean)) {
                                    isAvailable = false;

                                }
                                totalCalcTime += resEntry.getResEntryTime();


                            }
                            if(isAvailable){
                                Paragraph totalPrice;

                                reservationBean.setTotalTime((long) totalCalcTime);
                                if(reservationBean.isOnlyRoom()){
                                    editorForm.getChildren().forEach(child -> {
                                        if (child.getId().equals(Optional.of("totalPrice"))) {
                                            editorForm.remove(child);
                                        }});
                                    totalPriceRoom=getCalcPriceTotal(reservationBean.getRoom().getPreis(),totalCalcTime);
                                    if(weekendBetweenDates(reservationBean.getStart_date(),reservationBean.getEnd_date())&& !reservationBean.isRecurring()){
                                        totalPriceRoom = getTotalPriceWithoutWeekends(reservationBean.getStart_time(),reservationBean.getEnd_time(),totalPriceRoom,reservationBean.getRoom().getPreis());
                                        totalTimeWithoutWeekend=((long)(totalCalcTime-48));
                                    }

                                    NumberFormat formatter = new DecimalFormat("#.00");
                                    String formatedPrice = formatter.format(totalPriceRoom);
                                    totalPrice= new Paragraph("Total: "+formatedPrice+" CHF.-");
                                    totalPrice.setId("total-price");
                                    editorForm.add(totalPrice);
                                    editorForm.setColspan(totalPrice,2);
                                }
                                else {
                                    editorForm.getChildren().forEach(child -> {
                                        if (child.getId().equals(Optional.of("totalPrice"))) {
                                            editorForm.remove(child);
                                        }});
                                    for(Workspace workspace:reservationBean.getWorkspaceList()){
                                       totalPriceWorkspaces+= getCalcPriceTotal(workspace.getPreis(),totalCalcTime);
                                    }
                                    if(weekendBetweenDates(reservationBean.getStart_date(),reservationBean.getEnd_date())&& !reservationBean.isRecurring()) {
                                        float workspacePrice=0;
                                        for(Workspace workspace:reservationBean.getWorkspaceList()){
                                                workspacePrice=+ workspace.getPreis();
                                        }
                                        totalTimeWithoutWeekend=((long) (totalCalcTime-48));
                                        totalPriceWorkspaces = getTotalPriceWithoutWeekends(reservationBean.getStart_time(),reservationBean.getEnd_time(),totalPriceWorkspaces,workspacePrice);
                                    }
                                    NumberFormat formatter = new DecimalFormat("#.00");
                                    String formatedPriceWorkspace = formatter.format(totalPriceWorkspaces);
                                    totalPrice= new Paragraph("Total: "+formatedPriceWorkspace+" CHF.-");
                                    totalPrice.setId("total-price");
                                    editorForm.add(totalPrice);
                                    editorForm.setColspan(totalPrice,2);
                                }

                                openSuccessNotification(newResEntry);
                                editorForm.getCheckAvailabilityButton().setVisible(false);
                                editorForm.getSubmitButton().setVisible(true);
                                editorForm.getSubmitButton().addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                            }
                            else{
                                openNotification(newResEntry,timeFilter.getResEntryOccupying());
                                editorForm.getCheckAvailabilityButton().addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                                        ButtonVariant.LUMO_ERROR);
                            }
                        }
                        else  {
                            if(reservationBean.isSpecificTime()){

                                noRepeatingDays(reservationBean.getStart_time().toLocalDate(),reservationBean.getEnd_time().toLocalDate(),reservationBean.getReoccurringDays());
                            }
                            else{
                                noRepeatingDays(reservationBean.getStart_date(),reservationBean.getEnd_date(),reservationBean.getReoccurringDays());
                            }
                            editorForm.getCheckAvailabilityButton().addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                                    ButtonVariant.LUMO_ERROR);
                        }
                    /**
                     * The reservationBean is saved in db with status pending
                     */
                    reservationBean.setPaymentStatus("pending");

                    }catch (ValidationException e) {
                        throw new RuntimeException(e);
                    }
                });

        editorForm.getStartDateTime().addValueChangeListener(e -> {
            if (editorForm.getEndDateTime().getValue() != null && editorForm.getRoomCheckbox().getValue() != null)
                editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), roomService, reservationService));
            if (editorForm.getEndDateTime().getValue() != null && editorForm.getWorkspaceCheckbox().getValue() != null)
                editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), workspaceService, reservationService, resEntryService));
        });
        editorForm.getEndDateTime().addValueChangeListener(e -> {
            if (editorForm.getStartDateTime().getValue() != null && editorForm.getRoomCheckbox().getValue() != null)
                editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), roomService, reservationService));
            if (editorForm.getStartDateTime().getValue() != null && editorForm.getWorkspaceCheckbox().getValue() != null)
                editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), workspaceService, reservationService, resEntryService));
        });
        editorForm.getRoomGrid().addSelectionListener(selection -> {
            selectedRoom = selection.getFirstSelectedItem().get();
        });
        editorForm.getWorkspaceGrid().addSelectionListener(selectionEvent -> {
            selectedWorkspaces= selectionEvent.getAllSelectedItems();
        });
        editorForm.getStartDate().addValueChangeListener(e -> {
            if (editorForm.getEndDate().getValue() != null && editorForm.getRoomCheckbox().getValue() != null)
                editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(),editorForm.getEndDate().getValue().atTime(22,0), roomService, reservationService));
            if (editorForm.getEndDate().getValue() != null && editorForm.getWorkspaceCheckbox().getValue() != null)
                editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22, 0), workspaceService, reservationService, resEntryService));
        });
        editorForm.getEndDate().addValueChangeListener(e -> {
            if (editorForm.getStartDate().getValue() != null && editorForm.getRoomCheckbox().getValue() != null)
                editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22,0), roomService, reservationService));
            if (editorForm.getStartDate().getValue() != null && editorForm.getWorkspaceCheckbox().getValue() != null)
                editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22, 0), workspaceService, reservationService, resEntryService));
        });
        editorForm.getReoccurringDays().addSelectionListener(e -> {
            if (editorForm.getRadioButtonGroup().getValue().equals("Specific Time")) {
                if (editorForm.getStartDateTime().getValue() != null && editorForm.getEndDateTime() != null) {
                    if (editorForm.getRoomCheckbox().getValue())
                        editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), roomService, reservationService));
                    if (editorForm.getWorkspaceCheckbox().getValue())
                        editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), workspaceService, reservationService, resEntryService));
                }
            } else {
                if (editorForm.getRoomCheckbox().getValue())
                    editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22,0), roomService, reservationService));
                if (editorForm.getWorkspaceCheckbox().getValue())
                    editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22, 0), workspaceService, reservationService, resEntryService));
            }

        });
        editorForm.getSubmitButton().addClickListener(event -> {
            Reservation reservationBean = new Reservation();
                try {
                    reservationBinder.writeBean(reservationBean);
                    if(reservationBean.isOnlyWorkspace()) {
                        reservationBean.setWorkspaceList(selectedWorkspaces);
                    }
                    else reservationBean.setRoom(selectedRoom);

                    if (Objects.equals(editorForm.getRadioButtonGroup().getValue(), "Full Day")) {
                        reservationBean.setFullDay(true);
                        reservationBean.setSpecificTime(false);
                        reservationBean.setEnd_time(editorForm.getEndDate().getValue().atTime(22,0));
                        reservationBean.setStart_time(editorForm.getStartDate().getValue().atTime(5,0));
                    } else {
                        reservationBean.setFullDay(false);
                        reservationBean.setSpecificTime(true);
                        reservationBean.setEnd_date(editorForm.getEndDateTime().getValue().toLocalDate());
                        reservationBean.setStart_date(editorForm.getStartDateTime().getValue().toLocalDate());
                    }
                    if (reservationBean.getRoom() != null) {
                        reservationBean.setCosts(totalPriceRoom);
                        reservationBean.setColor(reservationBean.getRoom().getRoomColor());
                    }
                    if(reservationBean.isOnlyWorkspace()){
                        reservationBean.setCosts(totalPriceWorkspaces);
                        reservationBean.setColor("#000000");
                    }
                    if(weekendBetweenDates(reservationBean.getStart_date(),reservationBean.getEnd_date()))
                        reservationBean.setTotalTime(totalTimeWithoutWeekend);
                    else reservationBean.setTotalTime((long) totalCalcTime);
                    //user
                    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                    MyUserPrincipal customUser = (MyUserPrincipal) authentication.getPrincipal();
                    ApplicationUser currentUser = userService.findUserbyUsername(customUser.getUsername());
                    reservationBean.setUser(currentUser);
                    /**
                     * A reservation object in session is added
                     */
                    WrappedSession session = VaadinSession.getCurrent().getSession();
                    session.setAttribute("reservation",reservationBean);
                    reservationBean.setPaymentStatus("pending");
                    reservationService.update(reservationBean);
                    setReservationSuccessful(true);

                } catch (ValidationException e) {
                    setReservationSuccessful(false);
                    throw new RuntimeException(e);
                }
        });

    }

    public void openNotification(ResEntry resEntry,ResEntry exResEntry){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");
        Paragraph p;
        if(resEntry.getRoom()!=null) {
             p= new Paragraph("Room " + resEntry.getRoom().getBezeichnung() + " is occupied and not available\nfrom: " + exResEntry.getStartTime().format(formatter) + "\n" + " until: " + exResEntry.getEndTime().format(formatter));
        }
        else{

            List<Workspace> workspaceList = getOccupyingWorkspaces(resEntry,exResEntry);
            String result = workspaceList.stream()
                    .map(n -> String.valueOf(n))
                    .collect(Collectors.joining(",", "(", ")"));
            p= new Paragraph("Workspace(s) "+ result+ " occupied and not available from: "+ exResEntry.getStartTime().format(formatter) + "\n" + " until: " + exResEntry.getEndTime().format(formatter));

        }


        editorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("occupation")) || child.getId().equals(Optional.of("repDays"))) {
                editorForm.remove(child);
            }

        });
        p.setId("occupation");
        p.getStyle().set("color", "red");
        p.getStyle().set("white-space", "pre-line");
        editorForm.setColspan(p,4);
        editorForm.add(p);
        notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        Div text = new Div(new Text("Reservation failed because of: "+exResEntry.getTitle() ));
        com.vaadin.flow.component.button.Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> {
            notification.close();
        });
        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        notification.setPosition(Notification.Position.BOTTOM_END);
        notification.add(layout);
        notification.open();

    }
    public void nothingSelectedErrorMsg(){
        Paragraph selectionError = new Paragraph("Select a Workspace or Room!");
        selectionError.getStyle().set("color","red");
        selectionError.setId("selectionError");
        editorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("selectionError"))) {
                editorForm.remove(child);
            }});
        editorForm.setColspan(selectionError,4);
        editorForm.add(selectionError);
    }
    public void noRepeatingDays(LocalDate startTime, LocalDate endTime, Set<DayOfWeek>reoccurringDays){
        String result = reoccurringDays.stream()
                .map(n -> String.valueOf(n))
                .collect(Collectors.joining(","));
        Paragraph p = new Paragraph("No "+result+ " between "+ startTime + " - "+ endTime);
        editorForm.setColspan(p,4);
        p.getStyle().set("color","red");
        p.setId("repDays");
        editorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("repDays"))) {
                editorForm.remove(child);
            }});
        editorForm.add(p);



    }

    private List<Workspace> getOccupyingWorkspaces(ResEntry resEntry,ResEntry exResEntry) {
        List<Workspace>filteredWorkspaces = new ArrayList<>();
        for(Workspace workspace:resEntry.getWorkspaceList()){
           for(Workspace existWorkspace:exResEntry.getWorkspaceList()){
               if(Objects.equals(workspace.getId(), existWorkspace.getId())){
                   filteredWorkspaces.add(workspace);
               }
           }

        }
        return filteredWorkspaces;
    }

    public void openSuccessNotification(ResEntry resEntry){
        Paragraph p;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");
        if(resEntry.getRoom()!=null){
            p = new Paragraph("Room " + resEntry.getRoom().getBezeichnung()
                    + " is available from: " + resEntry.getStartTime().format(formatter)+
                    "\n"+" until: " + resEntry.getEndTime().format(formatter) );
            p.setId("occupation");
            p.getStyle().set("color", "green");
            p.getStyle().set("white-space", "pre-line");
            editorForm.setColspan(p,4);
            editorForm.add(p);
        }
        if(resEntry.getWorkspaceList()!=null){
            for(Workspace workspace:resEntry.getWorkspaceList() ){
                p = new Paragraph("Workspace: " + workspace.getBezeichnung()
                        + " is available from: " + resEntry.getStartTime().format(formatter)+
                        "\n"+" until: " + resEntry.getEndTime().format(formatter) );
                p.setId("occupation");
                p.getStyle().set("color", "green");
                p.getStyle().set("white-space", "pre-line");
                editorForm.setColspan(p,4);
                editorForm.add(p);
            }

        }
        else{

        }


        editorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("occupation"))) {
                editorForm.remove(child);
            }

        });




        Notification notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
        notification.setDuration(0);

        Div text = new Div(new Text("Reservation available"));
        com.vaadin.flow.component.button.Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> {
            notification.close();
        });

        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        notification.setPosition(Notification.Position.BOTTOM_END);
        notification.add(layout);
        notification.open();

    }
    public Collection<Room> getInitAvailableRooms(RoomService roomService,LocalDateTime startTime, LocalDateTime endTime){
        Collection<Room>initAvailableRooms = new HashSet<>();
        timeFilter = new TimeFilter(resEntryService);
        resEntryConverter = new ResEntryConverter(reservationService,resEntryService);
        for(Room room: roomService.findAll()){
            Reservation initialRes = new Reservation();
            initialRes.setStart_time(startTime);
            initialRes.setStart_date(startTime.toLocalDate());
            initialRes.setEnd_time(endTime);
            initialRes.setEnd_date(endTime.toLocalDate());
            initialRes.setRoom(room);
            for(ResEntry resEntry:resEntryConverter.convertResToEntry(initialRes)){
                if (timeFilter.checkIfResAvailable(resEntry,initialRes)){
                    initAvailableRooms.add(resEntry.getRoom());
                }
            }
        }

        return initAvailableRooms;
    }
    public Collection<Workspace> getInitAvailableWorkspaces(WorkspaceService workspaceService,LocalDateTime startTime, LocalDateTime endTime){
        Collection<Workspace>initAvailableWorkspace = new HashSet<>();
        timeFilter = new TimeFilter(resEntryService);

        for(Workspace workspace: workspaceService.findAll()){
            Reservation initialRes = new Reservation();
            initialRes.setStart_time(startTime);
            initialRes.setStart_date(startTime.toLocalDate());
            initialRes.setEnd_time(endTime);
            initialRes.setEnd_date(endTime.toLocalDate());
            Set<Workspace>initWorkspace = new HashSet<>();
            initWorkspace.add(workspace);
            initialRes.setWorkspaceList(initWorkspace);
            for(ResEntry resEntry:resEntryConverter.convertResToEntry(initialRes)){
                if (timeFilter.checkIfResAvailable(resEntry,initialRes)){
                    initAvailableWorkspace.addAll(resEntry.getWorkspaceList());
                }
            }
        }
        return initAvailableWorkspace;
    }
    public Collection<Room> getAvailableRooms(boolean isRecurring, Set<DayOfWeek> reoccurringDays, String radioBoxResult, LocalDateTime start, LocalDateTime end, RoomService roomService, ReservationService reservationService){
        TimeFilter timeFilter = new TimeFilter(resEntryService);

        resEntryConverter = new ResEntryConverter(reservationService,resEntryService);
        Collection<Room>roomCollection = roomService.findAll();
        Reservation possibleRes = new Reservation();
        Collection<Room>filteredRooms = new ArrayList<>();
        boolean isSpecificTime;
        isSpecificTime= radioBoxResult.equals("Specific Time");
        for(Room room :roomCollection){
            List<Boolean> roomAvailability = new ArrayList<>();
            possibleRes.setRoom(room);
            possibleRes.setStart_time(start);
            possibleRes.setStart_date(start.toLocalDate());
            possibleRes.setEnd_time(end);
            possibleRes.setEnd_date(end.toLocalDate());
            possibleRes.setSpecificTime(isSpecificTime);
            possibleRes.setRecurring(isRecurring);
            possibleRes.setReoccurringDays(reoccurringDays);
            for (ResEntry resEntry:resEntryConverter.convertResToEntry(possibleRes)){
                //Availabilty list per room
                roomAvailability.add( timeFilter.checkIfResAvailable(resEntry,new Reservation()));
            };
            if(!roomAvailability.contains(false)){
                filteredRooms.add(room);
            }
        }

        return filteredRooms;
    }
    public Collection<Workspace> getAvailableWorkspaces(boolean isRecurring, Set<DayOfWeek> reoccurringDays, String radioBoxResult, LocalDateTime start, LocalDateTime end, WorkspaceService workspaceService, ReservationService reservationService, ResEntryService resEntryService) {
        Set<Workspace> workspaceCollection = new HashSet<>(workspaceService.findAll());
        TimeFilter timeFilter = new TimeFilter(resEntryService);
        ResEntryConverter resEntryConverter = new ResEntryConverter(reservationService,resEntryService);
        Reservation possibleRes = new Reservation();
        Set<Workspace> filteredWorkspaces = new HashSet<>();
        boolean isSpecificTime;
        isSpecificTime = radioBoxResult.equals("Specific Time");
        //check for each existing workspace
        for (Workspace workspace : workspaceCollection) {
            List<Boolean> workspaceAvail = new ArrayList<>();
            Set<Workspace> workspaceSet = new HashSet<>();
            workspaceSet.add(workspace);
            possibleRes.setWorkspaceList(workspaceSet);
            possibleRes.setStart_time(start);
            possibleRes.setStart_date(start.toLocalDate());
            possibleRes.setEnd_time(end);
            possibleRes.setEnd_date(end.toLocalDate());
            possibleRes.setSpecificTime(isSpecificTime);
            possibleRes.setRecurring(isRecurring);
            possibleRes.setReoccurringDays(reoccurringDays);
            for (ResEntry resEntry:resEntryConverter.convertResToEntry(possibleRes)) {
                workspaceAvail.add(timeFilter.checkIfResAvailable(resEntry, new Reservation()));
            }
            if(!workspaceAvail.contains(false)){
                filteredWorkspaces.add(workspace);
            }

        }
        return filteredWorkspaces;
    }

    public float getTotalPriceWithoutWeekends(LocalDateTime startTime, LocalDateTime endTime,float totalPrice,float pricePerHour){
        int timeInHoursBetween = endTime.getHour()-startTime.getHour();
        float substrationPrice= pricePerHour *timeInHoursBetween*2;
        return totalPrice-substrationPrice;
    }
    public boolean weekendBetweenDates(LocalDate startDate, LocalDate endDate){
        for(LocalDate date= startDate; date.isBefore(endDate); date=date.plusDays(1)){
            if (date.getDayOfWeek()==DayOfWeek.SATURDAY)
                return true;
        }
        return false;
    }
    public float getCalcPriceTotal(float price,float calcTime){
        return price*calcTime;
    }

    public boolean isReservationSuccessful() {
        return ReservationSuccessful;
    }

    public void setReservationSuccessful(boolean reservationSuccessful) {
        ReservationSuccessful = reservationSuccessful;
    }

    boolean freeDate;


}
