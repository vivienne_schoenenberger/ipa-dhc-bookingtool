package com.example.application.view.calendar;

import com.example.application.entities.ResEntry;
import com.example.application.entities.Reservation;
import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.filter.ResEntryConverter;
import com.example.application.filter.TimeFilter;
import com.example.application.service.*;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import org.vaadin.stefan.fullcalendar.Entry;

import java.sql.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MINUTES;

public class CalendarEditFormBinder {
    final CalendarEditForm editorForm;
    final SingularEntryCalendarEditorForm singularEntryCalendarEditorForm;
    private final ReservationService reservationService;
    private final ResEntryService resEntryService;
    private final WorkspaceService workspaceService;

    private final RoomService roomService;
    private final UserService userService;
    boolean ReservationSuccessful;
    Notification notification;
    private ResEntry newResEntry;

    private Room selectedRoom;

    private Set<Workspace> selectedWorkspaces;



    public CalendarEditFormBinder(CalendarEditForm editorForm, SingularEntryCalendarEditorForm singularEntryCalendarEditorForm, ReservationService reservationService, UserService userService, ResEntryService resEntryService, WorkspaceService workspaceService, RoomService roomService) {
        this.editorForm = editorForm;
        this.singularEntryCalendarEditorForm = singularEntryCalendarEditorForm;
        this.reservationService = reservationService;
        this.userService = userService;
        this.resEntryService = resEntryService;
        this.workspaceService = workspaceService;
        this.roomService= roomService;


    }

    public void addBindingAndValidationToExistingEntry(Entry entry) {
        Long roomIdCurrent= 0L;
        if(reservationService.findReservationByEntryId(entry.getId()).getRoom()!=null){
            roomIdCurrent= reservationService.findReservationByEntryId(entry.getId()).getRoom().getId();
        }

        Reservation currentReservationOfEntry = reservationService.findReservationByEntryId(entry.getId());
        if (editorForm.getRoomCheckbox().getValue() && editorForm.getStartDateTime().getValue() != null && editorForm.getEndDateTime().getValue() != null) {
            Long finalRoomIdCurrent = roomIdCurrent;
            editorForm.getRoomGrid().setClassNameGenerator(room -> {
                if(Objects.equals(room.getId(), finalRoomIdCurrent))
                    return "current-reservation";
                return null;
            });
            editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), roomService, reservationService, resEntryService,currentReservationOfEntry ));

        }
        if (editorForm.getWorkspaceCheckbox().getValue() && editorForm.getStartDateTime().getValue() != null && editorForm.getEndDateTime().getValue() != null) {
            Set<Workspace>availableWorkspaces = new HashSet<>(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), workspaceService, reservationService, resEntryService, currentReservationOfEntry));
            editorForm.getWorkspaceGrid().setItems(availableWorkspaces);
            editorForm.getWorkspaceGrid().setClassNameGenerator(workspace -> {
                for(Workspace currentWorkspace: currentReservationOfEntry.getWorkspaceList()){
                        if(currentWorkspace.getId().equals(workspace.getId())) {
                            return "current-reservation";
                        }
                    }
                return null;
            });
        }

        Binder<Reservation> binder = new Binder<>(Reservation.class);
        binder.bindInstanceFields(editorForm);
        binder.forField(editorForm.getEntryTitle()).bind("title");
        binder.forField(editorForm.getDescription()).bind("description");

        binder.forField(editorForm.getReoccurringDays()).bind("reoccurringDays");
        binder.forField(editorForm.getStartDateTime()).withValidator(startDateTime -> {
            boolean validWeekDay = startDateTime.getDayOfWeek().getValue() >= 1
                    && startDateTime.getDayOfWeek().getValue() <= 5;
            return validWeekDay;
        }, "The selected day of week is not available").withValidator(startDateTime -> {
            boolean validDate = !startDateTime.isBefore(LocalDateTime.now());
            return validDate;
        }, "The selected date is in the past").withValidator(startDateTime -> {
            LocalTime startTime = LocalTime.of(startDateTime.getHour(), startDateTime.getMinute());
            boolean validTime = !(LocalTime.of(5, 0).isAfter(startTime)
                    || LocalTime.of(22, 0).isBefore(startTime));
            return validTime;
        }, "The selected time is not available").bind("start_time").getField().addValueChangeListener(e -> binder.validate());
        binder.forField(editorForm.getStartDate()).withValidator(startDate -> {
            if (!editorForm.getRadioButtonGroup().getValue().equals("Specific Time")) {
                boolean validWeekDay = startDate.getDayOfWeek().getValue() >= 1
                        && startDate.getDayOfWeek().getValue() <= 5;
                return validWeekDay;
            } else return true;
        }, "The selected day of week is not available").withValidator(startDate -> {
            if (!editorForm.getRadioButtonGroup().getValue().equals("Specific Time")) {
                boolean validDate = !startDate.isBefore(LocalDate.now());
                return validDate;
            } else return true;
        }, "The selected date is in the past").bind("start_date").getField().addValueChangeListener(event -> {
            editorForm.getSubmitButton().setVisible(false);
            editorForm.getCheckAvailabilityButton().setVisible(true);
        });
        binder.forField(editorForm.getEndDate()).withValidator(endDate -> {
            if (!editorForm.getRadioButtonGroup().getValue().equals("Specific Time")) {
                boolean validWeekDay = endDate.getDayOfWeek().getValue() >= 1
                        && endDate.getDayOfWeek().getValue() <= 5;
                return validWeekDay;
            } else return true;
        }, "The selected day of week is not available").withValidator(endDate -> {
            if (!editorForm.getRadioButtonGroup().getValue().equals("Specific Time")) {
                boolean validDate = !endDate.isBefore(LocalDate.now());
                return validDate;
            } else return true;
        }, "The selected date is in the past").bind("end_date").getField().addValueChangeListener(event -> {
            editorForm.getSubmitButton().setVisible(false);
            editorForm.getCheckAvailabilityButton().setVisible(true);
        });
        binder.forField(editorForm.getEndDateTime()).withValidator(endDateTime -> {
            boolean validWeekDay = endDateTime.getDayOfWeek().getValue() >= 1
                    && endDateTime.getDayOfWeek().getValue() <= 5;
            return validWeekDay;
        }, "The selected day of week is not available").withValidator(startDateTime -> {
            LocalTime startTime = LocalTime.of(startDateTime.getHour(), startDateTime.getMinute());
            boolean validTime = !(LocalTime.of(5, 0).isAfter(startTime)
                    || LocalTime.of(22, 0).isBefore(startTime));
            return validTime;
        }, "The selected time is not available").bind("end_time").getField().addValueChangeListener(e -> binder.validate());
        binder.forField(editorForm.getRoomCheckbox()).bind("onlyRoom").getField().addValueChangeListener(event -> {
            if (editorForm.getRoomCheckbox().getValue()) {
                editorForm.getRoomGrid().setVisible(true);
                editorForm.getWorkspaceCheckbox().setValue(false);
                //editorForm.getRoomComboBox().setVisible(true);
                if (editorForm.getStartDateTime().getValue() != null && editorForm.getEndDateTime().getValue() != null) {
                    editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), roomService, reservationService, resEntryService,reservationService.findReservationByEntryId(entry.getId())));
                    editorForm.getRoomGrid().select(currentReservationOfEntry.getRoom());
                }
            } else {
                editorForm.getRoomGrid().setVisible(false);
                editorForm.getRoomGrid().setVisible(false);
                //editorForm.getRoomComboBox().setValue(null);
            }
        });
        binder.forField(editorForm.getWorkspaceCheckbox()).bind("onlyWorkspace").getField().addValueChangeListener(event -> {
            if (editorForm.getWorkspaceCheckbox().getValue()) {
                editorForm.getWorkspaceGrid().setVisible(true);
                editorForm.getRoomGrid().setVisible(false);
                editorForm.getRoomCheckbox().setValue(false);
                //editorForm.getWorkspaceMultiselectComboBox().setVisible(true);
                if (editorForm.getStartDateTime().getValue() != null && editorForm.getEndDateTime().getValue() != null) {
                    editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), workspaceService, reservationService, resEntryService,currentReservationOfEntry));
                }
            } else {
                editorForm.getWorkspaceGrid().setVisible(false);
                editorForm.getRoomGrid().setVisible(true);
            }

        });

        editorForm.getCheckAvailabilityButton().addClickListener(event -> {
            binder.validate();
            boolean isAvailable = true;
            TimeFilter timeFilter = new TimeFilter(resEntryService);
            Reservation reservationBean = new Reservation();
            ResEntryConverter resEntryConverter = new ResEntryConverter(reservationService, resEntryService);
            try {
                if (Objects.equals(editorForm.getRadioButtonGroup().getValue(), "Full Day")) {
                    reservationBean.setFullDay(true);
                    reservationBean.setSpecificTime(false);
                } else {
                    reservationBean.setFullDay(false);
                    reservationBean.setSpecificTime(true);
                }


                binder.writeBean(reservationBean);
                reservationBean.setWorkspaceList(selectedWorkspaces);
                reservationBean.setRoom(selectedRoom);
                Reservation oldReservation = reservationService.findReservationById((long) getReservationDetailsOfEntry(entry));
                reservationBean.setId(oldReservation.getId());
                if (editorForm.getRecurring().getValue()) {
                    if (editorForm.getProlongSeries().getValue()) {
                        for (ResEntry resEntry : prolongedSeriesOfEntries(reservationBean, entry)) {
                            newResEntry = resEntry;
                            if (!timeFilter.checkIfResAvailable(resEntry, reservationBean)) {
                                isAvailable = false;
                            }
                        }
                    } else {
                        for (ResEntry resEntry : resEntryConverter.convertResToEntry(reservationBean)) {
                            newResEntry = resEntry;
                            if (!timeFilter.checkIfResAvailable(resEntry, reservationBean)) {
                                isAvailable = false;
                            }
                        }
                    }
                } else {
                    newResEntry = resEntryService.findResEntryByEntryId(entry.getId());
                    if (!timeFilter.checkIfResAvailable(newResEntry, reservationBean)) {
                        isAvailable = false;
                    }
                }
                if (isAvailable) {
                    openSuccessNotification(newResEntry);
                    editorForm.getCheckAvailabilityButton().setVisible(false);
                    editorForm.getSubmitButton().setVisible(true);
                    editorForm.getSubmitButton().addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                } else {
                    openNotification(newResEntry, timeFilter.getResEntryOccupying());
                    editorForm.getCheckAvailabilityButton().addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                            ButtonVariant.LUMO_ERROR);
                }

            } catch (ValidationException e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });

        //events for grid:
        editorForm.getStartDateTime().addValueChangeListener(e -> {
            if (editorForm.getEndDateTime().getValue() != null && editorForm.getRoomCheckbox().getValue() != null)
                editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), roomService, reservationService, resEntryService,currentReservationOfEntry));
            if (editorForm.getEndDateTime().getValue() != null && editorForm.getWorkspaceCheckbox().getValue() != null)
                editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), workspaceService, reservationService, resEntryService,currentReservationOfEntry));
        });
        editorForm.getEndDateTime().addValueChangeListener(e -> {
            if (editorForm.getStartDateTime().getValue() != null && editorForm.getRoomCheckbox().getValue() != null)
                editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), roomService, reservationService, resEntryService,currentReservationOfEntry));
            if (editorForm.getStartDateTime().getValue() != null && editorForm.getWorkspaceCheckbox().getValue() != null)
                editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), workspaceService, reservationService, resEntryService,currentReservationOfEntry));
        });
        editorForm.getRoomGrid().addSelectionListener(selection -> {
            selectedRoom = selection.getFirstSelectedItem().get();
        });
        editorForm.getWorkspaceGrid().addSelectionListener(selectionEvent -> {
            selectedWorkspaces= selectionEvent.getAllSelectedItems();
        });
        editorForm.getStartDate().addValueChangeListener(e -> {
            if (editorForm.getEndDate().getValue() != null && editorForm.getRoomCheckbox().getValue() != null)
                editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(),editorForm.getEndDate().getValue().atTime(22,0), roomService, reservationService, resEntryService,currentReservationOfEntry));
            if (editorForm.getEndDate().getValue() != null && editorForm.getWorkspaceCheckbox().getValue() != null)
                editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22, 0), workspaceService, reservationService, resEntryService,currentReservationOfEntry));
        });
        editorForm.getEndDate().addValueChangeListener(e -> {
            if (editorForm.getStartDate().getValue() != null && editorForm.getRoomCheckbox().getValue() != null)
                editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22,0), roomService, reservationService, resEntryService,currentReservationOfEntry));
            if (editorForm.getStartDate().getValue() != null && editorForm.getWorkspaceCheckbox().getValue() != null)
                    editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22, 0), workspaceService, reservationService, resEntryService,currentReservationOfEntry));
        });
        editorForm.getReoccurringDays().addSelectionListener(e -> {
            if (editorForm.getRadioButtonGroup().getValue().equals("Specific Time")) {
                if (editorForm.getStartDateTime().getValue() != null && editorForm.getEndDateTime() != null) {
                    if (editorForm.getRoomCheckbox().getValue())
                        editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), roomService, reservationService, resEntryService,currentReservationOfEntry));
                    if (editorForm.getWorkspaceCheckbox().getValue())
                            editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDateTime().getValue(), editorForm.getEndDateTime().getValue(), workspaceService, reservationService, resEntryService,currentReservationOfEntry));
                }
            } else {
                if (editorForm.getRoomCheckbox().getValue())
                    editorForm.getRoomGrid().setItems(getAvailableRooms(editorForm.getRecurring().getValue(),editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22,0), roomService, reservationService, resEntryService,currentReservationOfEntry));
                if (editorForm.getWorkspaceCheckbox().getValue())
                        editorForm.getWorkspaceGrid().setItems(getAvailableWorkspaces(editorForm.getRecurring().getValue(), editorForm.getReoccurringDays().getSelectedItems(), editorForm.getRadioButtonGroup().getValue(), editorForm.getStartDate().getValue().atStartOfDay(), editorForm.getEndDate().getValue().atTime(22, 0), workspaceService, reservationService, resEntryService,currentReservationOfEntry));
            }
        });
        editorForm.getSubmitButton().addClickListener(event -> {
            Reservation reservationBean = new Reservation();
            try {
                if (Objects.equals(editorForm.getRadioButtonGroup().getValue(), "Full Day")) {
                    reservationBean.setFullDay(true);
                    reservationBean.setSpecificTime(false);
                } else {
                    reservationBean.setFullDay(false);
                    reservationBean.setSpecificTime(true);
                }
                binder.writeBean(reservationBean);
                Reservation reservationToUpdate = reservationService.findReservationById((long) getReservationDetailsOfEntry(entry));
                //reservationToUpdate.setRoom(reservationBean.getRoom());

                reservationToUpdate.setWorkspaceList(reservationBean.getWorkspaceList());
                reservationToUpdate.setDescription(reservationBean.getDescription());
                reservationToUpdate.setStart_time(reservationBean.getStart_time());
                reservationToUpdate.setEnd_time(reservationBean.getEnd_time());
                reservationToUpdate.setStart_date(reservationBean.getStart_date());
                reservationToUpdate.setEnd_date(reservationBean.getEnd_date());
                long daysBetween = DAYS.between(reservationBean.getStart_time(), reservationBean.getEnd_time());
                long minutesBetween = MINUTES.between(reservationBean.getStart_time(), reservationBean.getEnd_time());

                reservationToUpdate.setTitle(reservationBean.getTitle());
                reservationToUpdate.setOnlyRoom(reservationBean.isOnlyRoom());
                reservationToUpdate.setOnlyWorkspace(reservationBean.isOnlyWorkspace());
                reservationToUpdate.setRecurring(reservationBean.isRecurring());
                reservationToUpdate.setReoccurringDays(reservationBean.getReoccurringDays());
                reservationToUpdate.setSpecificTime(reservationBean.isSpecificTime());
                reservationToUpdate.setFullDay(reservationBean.isFullDay());
                if(reservationBean.isOnlyRoom()){
                    reservationToUpdate.setRoom(selectedRoom);
                    reservationToUpdate.setColor(selectedRoom.getRoomColor());
                }
                if(reservationBean.isOnlyWorkspace()){
                    reservationToUpdate.setWorkspaceList(selectedWorkspaces);
                    reservationToUpdate.setColor("#000000");
                }
                Set<ResEntry> resEntrySet;
                if (editorForm.getProlongSeries().getValue()) {
                    resEntrySet = prolongedSeriesOfEntries(reservationBean, entry);
                } else
                    resEntrySet = reservationToUpdate.getResEntries();
                reservationToUpdate.setResEntries(null);
                reservationService.update(reservationToUpdate);
                if (resEntrySet != null) {
                    for (ResEntry resEntry : resEntrySet) {

                        resEntryService.delete(resEntry);
                    }
                }
                reservationToUpdate.setEntryIds(null);

                reservationService.update(reservationToUpdate);
                showUpdateSuccess(reservationBean);


            } catch (ValidationException e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

        });
        editorForm.getDeleteButton().addClickListener(event1 -> {
            Reservation currentReservation;
            try {
                currentReservation = reservationService.findReservationById((long) getReservationDetailsOfEntry(entry));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            reservationService.delete(currentReservation);
            showDeleteSuccess(currentReservation);
        });
        //events for grid done:
    }

    public void setValuesOfCurrentEntry(Entry entry) {
        try {
            Reservation currentReservation = reservationService.findReservationById((long) getReservationDetailsOfEntry(entry));
            if (currentReservation.isRecurring()) {
                editorForm.getEditSeries().setVisible(true);
                editorForm.getProlongSeries().setVisible(true);
                editorForm.getRecurring().setReadOnly(true);
                editorForm.getReoccurringDays().setReadOnly(true);

                editorForm.getEditSeries().addValueChangeListener(e -> {
                    boolean enabled;
                    enabled = e.getValue();
                    editorForm.getProlongSeries().setReadOnly(!enabled);
                    editorForm.getWorkspaceCheckbox().setEnabled(enabled);
                    editorForm.getRoomCheckbox().setEnabled(enabled);
                    editorForm.getRadioButtonGroup().setReadOnly(!enabled);
                    //editorForm.getWorkspaceMultiselectComboBox().setReadOnly(!enabled);
                    //editorForm.getRoomComboBox().setReadOnly(!enabled);
                    editorForm.getEntryTitle().setReadOnly(!enabled);
                    editorForm.getDescription().setReadOnly(!enabled);
                });
                editorForm.getProlongSeries().addValueChangeListener(e -> {
                    boolean enabled;
                    enabled = e.getValue();
                    editorForm.getStartDate().setReadOnly(!enabled);
                    editorForm.getEndDate().setReadOnly(!enabled);
                    editorForm.getStartDateTime().setReadOnly(!enabled);
                    editorForm.getEndDateTime().setReadOnly(!enabled);
                });
                editorForm.getSingularEntryTitle().setVisible(true);
                editorForm.getSeriesTitle().setVisible(true);
                editorForm.getDivider2().setVisible(true);
                editorForm.getDivider3().setVisible(true);

                editorForm.getEditSingularEntry().setEnabled(true);
                editorForm.getEditSingularEntry().setVisible(true);
                editorForm.getEntryDate().setVisible(true);
                editorForm.getEntryDate().setValue(entry.getStart().toLocalDate());
                editorForm.getRecurring().setValue(currentReservation.isRecurring());
                if (currentReservation.isSpecificTime()) {
                    editorForm.getRadioButtonGroup().setValue("Specific Time");
                    editorForm.getStartDateTime().setValue(currentReservation.getStart_time());
                    editorForm.getEndDateTime().setValue(currentReservation.getEnd_time());
                    editorForm.getEndDate().setValue(currentReservation.getEnd_time().toLocalDate());
                    editorForm.getStartDate().setValue(currentReservation.getStart_time().toLocalDate());
                } else {
                    editorForm.getRadioButtonGroup().setValue("Full Day");
                    editorForm.getStartDate().setValue(currentReservation.getStart_date());
                    editorForm.getStartDateTime().setValue(currentReservation.getStart_date().atStartOfDay().plusHours(5));
                    editorForm.getEndDateTime().setValue(currentReservation.getEnd_date().atTime(22, 0));
                    editorForm.getEndDate().setValue(currentReservation.getEnd_date());
                }

                ResEntry currentResEntry = resEntryService.findResEntryByEntryId(entry.getId());
                singularEntryCalendarEditorForm.getEntryTitle().setValue(currentResEntry.getTitle());
                singularEntryCalendarEditorForm.getDescription().setValue(currentResEntry.getDescription());
                singularEntryCalendarEditorForm.getStartDateTime().setValue(currentResEntry.getStartTime());
                singularEntryCalendarEditorForm.getEndDateTime().setValue(currentResEntry.getEndTime());
                singularEntryCalendarEditorForm.getStartDate().setValue(currentResEntry.getStartTime().toLocalDate());
                singularEntryCalendarEditorForm.getEndDate().setValue(currentResEntry.getEndTime().toLocalDate());
                singularEntryCalendarEditorForm.getRoomComboBox().setValue(currentResEntry.getRoom());
                singularEntryCalendarEditorForm.getRoomCheckbox().setValue(currentResEntry.isOnlyRoom());
                singularEntryCalendarEditorForm.getWorkspaceCheckbox().setValue(currentResEntry.isOnlyWorkspace());
                singularEntryCalendarEditorForm.getRadioButtonGroup().setValue("Specific Time");
                singularEntryCalendarEditorForm.getWorkspaceMultiselectComboBox().setValue(currentResEntry.getWorkspaceList());

            } else {
                editorForm.getWorkspaceCheckbox().setEnabled(true);
                editorForm.getRoomCheckbox().setEnabled(true);
                editorForm.getStartDate().setReadOnly(false);
                editorForm.getEndDate().setReadOnly(false);
                editorForm.getStartDateTime().setReadOnly(false);
                editorForm.getEndDateTime().setReadOnly(false);
                editorForm.getRadioButtonGroup().setReadOnly(false);
                editorForm.getEntryTitle().setReadOnly(false);
                editorForm.getDescription().setReadOnly(false);

            }
            if (currentReservation.isSpecificTime()) {
                editorForm.getRadioButtonGroup().setValue("Specific Time");
                editorForm.getStartDateTime().setValue(currentReservation.getStart_time());
                editorForm.getEndDateTime().setValue(currentReservation.getEnd_time());
                editorForm.getEndDate().setValue(currentReservation.getEnd_time().toLocalDate());
                editorForm.getStartDate().setValue(currentReservation.getStart_time().toLocalDate());
            } else {
                editorForm.getRadioButtonGroup().setValue("Full Day");
                editorForm.getStartDate().setValue(currentReservation.getStart_date());
                editorForm.getStartDateTime().setValue(currentReservation.getStart_date().atStartOfDay().plusHours(5));
                editorForm.getEndDateTime().setValue(currentReservation.getEnd_date().atTime(22, 0));
                editorForm.getEndDate().setValue(currentReservation.getEnd_date());
            }
            editorForm.getEntryTitle().setValue(currentReservation.getTitle());
            editorForm.getDescription().setValue(currentReservation.getDescription());
            editorForm.getRoomCheckbox().setValue(currentReservation.isOnlyRoom());
            editorForm.getWorkspaceCheckbox().setValue(currentReservation.isOnlyWorkspace());
            editorForm.getRoomGrid().setVisible(currentReservation.isOnlyRoom());
            editorForm.getWorkspaceGrid().setVisible(currentReservation.isOnlyWorkspace());

            //editorForm.getRoomComboBox().setValue(currentReservation.getRoom());
            editorForm.getReoccurringDays().setValue(currentReservation.getReoccurringDays());
            //editorForm.getWorkspaceMultiselectComboBox().setValue(currentReservation.getWorkspaceList());

            if (editorForm.getWorkspaceCheckbox().getValue()) {
                //editorForm.getWorkspaceMultiselectComboBox().setVisible(true);
            } //else editorForm.getWorkspaceMultiselectComboBox().setVisible(false);
            if (editorForm.getRoomCheckbox().getValue()) {
                //editorForm.getRoomComboBox().setVisible(true);
            } //else editorForm.getRoomComboBox().setVisible(false);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    private int getReservationDetailsOfEntry(Entry entry) throws SQLException {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select reservation_id from bookingtool.reservation_entry_ids where entry_ids ='" + entry.getId() + "'";
        ResultSet rs = statement.executeQuery(sql);
        int resID = 0;
        while (rs.next()) {
            resID = rs.getInt("reservation_id");
        }
        c.close();
        return resID;
    }


    private void showUpdateSuccess(Reservation reservationBean) {

        notification =
                Notification.show("Reservation updated: " + reservationBean.getTitle());
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);

    }

    private void showDeleteSuccess(Reservation reservationBean) {

        notification =
                Notification.show("Reservation deleted: " + reservationBean.getTitle());
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

    }

    public void openSuccessNotification(ResEntry resEntry) {
        Paragraph p;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");
        if (resEntry.getRoom() != null) {
            p = new Paragraph("Room " + resEntry.getRoom().getBezeichnung()
                    + " is available from: " + resEntry.getStartTime().format(formatter) +
                    "\n" + " until: " + resEntry.getEndTime().format(formatter));
            p.setId("occupation");
            p.getStyle().set("color", "green");
            p.getStyle().set("white-space", "pre-line");
            editorForm.setColspan(p, 4);
            editorForm.add(p);
        }
        if (resEntry.getWorkspaceList() != null) {
            for (Workspace workspace : resEntry.getWorkspaceList()) {
                p = new Paragraph("Workspace: " + workspace.getBezeichnung()
                        + " is available from: " + resEntry.getStartTime().format(formatter) +
                        "\n" + " until: " + resEntry.getEndTime().format(formatter));
                p.setId("occupation");
                p.getStyle().set("color", "green");
                p.getStyle().set("white-space", "pre-line");
                editorForm.setColspan(p, 4);
                editorForm.add(p);
            }

        } else {

        }
        editorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("occupation"))) {
                editorForm.remove(child);
            }

        });


        Notification notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
        notification.setDuration(0);

        Div text = new Div(new Text("Reservation available"));
        com.vaadin.flow.component.button.Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> {
            notification.close();
        });

        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        notification.setPosition(Notification.Position.BOTTOM_END);
        notification.add(layout);
        notification.open();

    }

    public void openNotification(ResEntry resEntry, ResEntry exResEntry) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");
        Paragraph p;
        if (resEntry.getRoom() != null) {
            p = new Paragraph("Room " + resEntry.getRoom().getBezeichnung() + " is occupied and not available from: " + exResEntry.getStartTime().format(formatter) + "\n" + " until: " + exResEntry.getEndTime().format(formatter));
        } else {

            List<Workspace> workspaceList = getOccupyingWorkspaces(resEntry, exResEntry);
            String result = workspaceList.stream()
                    .map(n -> String.valueOf(n))
                    .collect(Collectors.joining(",", "(", ")"));
            p = new Paragraph("Workspace(s) " + result + " occupied and not available from: " + exResEntry.getStartTime().format(formatter) + "\n" + " until: " + exResEntry.getEndTime().format(formatter));

        }


        editorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("occupation"))) {
                editorForm.remove(child);
            }

        });
        p.setId("occupation");
        p.getStyle().set("color", "red");
        p.getStyle().set("white-space", "pre-line");
        editorForm.setColspan(p, 4);
        editorForm.add(p);
        notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        Div text = new Div(new Text("Reservation failed because of: " + exResEntry.getTitle()));
        com.vaadin.flow.component.button.Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> {
            notification.close();
        });
        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        notification.setPosition(Notification.Position.BOTTOM_END);
        notification.add(layout);
        notification.open();

    }

    private List<Workspace> getOccupyingWorkspaces(ResEntry resEntry, ResEntry exResEntry) {
        List<Workspace> filteredWorkspaces = new ArrayList<>();
        for (Workspace workspace : resEntry.getWorkspaceList()) {
            for (Workspace existWorkspace : exResEntry.getWorkspaceList()) {
                if (Objects.equals(workspace.getId(), existWorkspace.getId())) {
                    filteredWorkspaces.add(workspace);
                }
            }

        }
        return filteredWorkspaces;
    }

    public Set<ResEntry> prolongedSeriesOfEntries(Reservation reservation, Entry entry) {
        Reservation oldReservation = reservationService.findReservationByEntryId(entry.getId());
        Set<ResEntry> shortEntriesSet = oldReservation.getResEntries();
        reservation.setStart_time(oldReservation.getEnd_time().plusDays(1));
        reservation.setEnd_time(reservation.getEnd_time());
        reservation.setColor(oldReservation.getColor());
        ResEntryConverter converter = new ResEntryConverter(reservationService, resEntryService);
        HashSet<ResEntry> longEntriesSet = new HashSet<>(converter.convertResToEntry(reservation));
        longEntriesSet.addAll(shortEntriesSet);
        return longEntriesSet;
    }

    public Collection<Workspace> getAvailableWorkspaces(boolean isRecurring, Set<DayOfWeek> reoccurringDays, String radioBoxResult, LocalDateTime start, LocalDateTime end, WorkspaceService workspaceService, ReservationService reservationService, ResEntryService resEntryService,Reservation currentRes) {
        Set<Workspace> workspaceCollection = new HashSet<>(workspaceService.findAll());
        TimeFilter timeFilter = new TimeFilter(resEntryService);
        ResEntryConverter resEntryConverter = new ResEntryConverter(reservationService,resEntryService);
        Reservation possibleRes = new Reservation();
        Set<Workspace> filteredWorkspaces = new HashSet<>();
        boolean isSpecificTime;
        isSpecificTime = radioBoxResult.equals("Specific Time");
        //check for each existing workspace
        for (Workspace workspace : workspaceCollection) {
            Set<Workspace> workspaceSet = new HashSet<>();
            workspaceSet.add(workspace);
            possibleRes.setWorkspaceList(workspaceSet);
            possibleRes.setStart_time(start);
            possibleRes.setStart_date(start.toLocalDate());
            possibleRes.setEnd_time(end);
            possibleRes.setEnd_date(end.toLocalDate());
            possibleRes.setSpecificTime(isSpecificTime);
            possibleRes.setRecurring(isRecurring);
            possibleRes.setReoccurringDays(reoccurringDays);
            for (ResEntry resEntry:resEntryConverter.convertResToEntry(possibleRes)){
                if (timeFilter.checkIfResAvailable(resEntry,currentRes)) {
                filteredWorkspaces.addAll(possibleRes.getWorkspaceList());
            }}

        }
        return filteredWorkspaces;
    }


    public Collection<Room> getAvailableRooms(boolean isRecurring,Set<DayOfWeek> reoccurringDays, String radioBoxResult,LocalDateTime start, LocalDateTime end,RoomService roomService, ReservationService reservationService, ResEntryService resEntryService,Reservation currentReservation){
        TimeFilter timeFilter = new TimeFilter(resEntryService);
        ResEntryConverter resEntryConverter = new ResEntryConverter(reservationService,resEntryService);
        Collection<Room>roomCollection = roomService.findAll();
        Reservation possibleRes = new Reservation();
        Collection<Room>filteredRooms = new ArrayList<>();
        boolean isSpecificTime;
        isSpecificTime= radioBoxResult.equals("Specific Time");
        for(Room room :roomCollection){
            Set<Boolean> available=new HashSet<>();
            possibleRes.setRoom(room);
            possibleRes.setStart_time(start);
            possibleRes.setStart_date(start.toLocalDate());
            possibleRes.setEnd_time(end);
            possibleRes.setEnd_date(end.toLocalDate());
            possibleRes.setSpecificTime(isSpecificTime);
            possibleRes.setRecurring(isRecurring);
            possibleRes.setReoccurringDays(reoccurringDays);
            for (ResEntry resEntry:resEntryConverter.convertResToEntry(possibleRes)){
                if (timeFilter.checkIfResAvailable(resEntry,currentReservation)){
                    available.add(true);

                }
            };
            if(!available.contains(false))
                filteredRooms.add(possibleRes.getRoom());
        }

        return filteredRooms;
    }


}
