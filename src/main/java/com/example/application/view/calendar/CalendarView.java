package com.example.application.view.calendar;

import com.example.application.service.LazyDataProvider.EntryDataProvider;
import com.example.application.entities.ApplicationUser;
import com.example.application.entities.Reservation;
import com.example.application.entities.UserRole;
import com.example.application.security.MyUserPrincipal;
import com.example.application.service.*;
import com.example.application.view.main.MainMenuView;
import com.example.application.view.payment.CheckoutView;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.vaadin.stefan.fullcalendar.*;


import javax.annotation.security.PermitAll;
import java.sql.*;
import java.time.*;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@PermitAll
@Route(value = "Calendar", layout = MainMenuView.class)
@PageTitle("Calendar | Bookingtool")

public class CalendarView extends HorizontalLayout {
    private final ComboBox<Month> monthPicker;
    private final ComboBox<Integer> yearPicker;
    public CalendarView(ReservationService reservationService, RoomService roomService, WorkspaceService workspaceService, UserService userService, ResEntryService resEntryService) {
        VerticalLayout parent = new VerticalLayout();
        HorizontalLayout controllLayout = new HorizontalLayout();
        HorizontalLayout timeControllLayout = new HorizontalLayout();
        FullCalendar calendar = new FullCalendarWithTooltip();


        calendar.setHeightByParent(); // calculate the height by parent
        calendar.getElement().getStyle().set("flex-grow", "1");
        calendar.scrollIntoView();
        calendar.setWeekends(false);
        parent.setFlexGrow(1, calendar);
        parent.setHorizontalComponentAlignment(Alignment.STRETCH);
        parent.setHeight("80vh");
        calendar.gotoDate(LocalDate.now());
        calendar.setFixedWeekCount(false);

        calendar.setSizeFull(); // also set the size full to take all the content

        // Data Provider

// Create a initial sample entry
        Entry entry = new Entry();
        entry.setTitle("Some event");
        entry.setColor("#ff3333");

// the given times will be interpreted as utc based - useful when the times are fetched from your database
        entry.setStart(LocalDate.now().withDayOfMonth(3).atTime(10, 0));
        entry.setEnd(entry.getStart().plusHours(2));
        List<Entry> collect = new ArrayList<>();
        collect.add(entry);
        add(controllLayout,parent);
        EntryDataProvider entryDataProvider = new EntryDataProvider(reservationService,resEntryService);
        calendar.setEntryProvider(entryDataProvider);
        calendar.addTimeslotsSelectedListener((event) -> {
            // react on the selected timeslot, for instance create a new instance and let the user edit it
            // ... show and editor
            CalendarForm calendarFormAdd = new CalendarForm(roomService,workspaceService);
            Dialog dialog = new Dialog();
            SingularEntryCalendarEditorForm singularEntryCalendarEditorForm = new SingularEntryCalendarEditorForm(roomService, workspaceService);
            dialog.add(calendarFormAdd);
            CalendarFormBinder calendarEditorFormBinder = new CalendarFormBinder(calendarFormAdd,singularEntryCalendarEditorForm, reservationService, userService,resEntryService,roomService,workspaceService);
            calendarFormAdd.getDeleteButton().setVisible(false);
            calendarEditorFormBinder.setDefaultDate(event.getStartDate().atStartOfDay().plusHours(5), event.getStartDate().atStartOfDay().plusHours(20));
            calendarEditorFormBinder.addBindingAndValidation();
            dialog.setDraggable(true);
            add(dialog);
            dialog.open();
            calendarFormAdd.getSubmitButton().addClickListener((e) -> {
             /**
             * Navigate to checkout view after submit
             */
                UI.getCurrent().navigate(CheckoutView.class);

            });
            Button closeButton = new Button(new Icon("lumo", "cross"),
                    (e) -> dialog.close());
            closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
            dialog.getHeader().add(closeButton);

        });
        calendar.addEntryClickedListener((event) -> {

            try {
                if (checkUserPermission(userService,event.getEntry(),reservationService)){
                    CalendarEditForm calendarFormExistEntry = new CalendarEditForm(roomService,workspaceService);
                    Entry currentEntry = event.getEntry();
                    Dialog editDialog = new Dialog();
                    SingularEntryCalendarEditorForm singularEntryCalendarEditorForm = new SingularEntryCalendarEditorForm(roomService, workspaceService);
                    editDialog.setDraggable(true);
                    editDialog.add(calendarFormExistEntry);
                    CalendarEditFormBinder calendarFormBinder = new CalendarEditFormBinder(calendarFormExistEntry,singularEntryCalendarEditorForm, reservationService,userService,resEntryService,workspaceService,roomService);
                    calendarFormBinder.setValuesOfCurrentEntry(currentEntry);
                    calendarFormBinder.addBindingAndValidationToExistingEntry(currentEntry);

                    try {
                        Reservation currentReservation = reservationService.findReservationById((long) getReservationDetailsOfEntry(currentEntry));
                        if(currentReservation.isRecurring()){
                            calendarFormBinder.editorForm.getEditSingularEntry().setVisible(true);
                        }
                        else{
                            calendarFormBinder.editorForm.getEditSingularEntry().setVisible(false);
                        }
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }

                    Dialog singularEditDialog = new Dialog();
                    calendarFormBinder.editorForm.getEditSingularEntry().addClickListener(event1 -> {
                        singularEditDialog.add(singularEntryCalendarEditorForm);
                        SingularEntryFormBinder singularEntryFormBinder = new SingularEntryFormBinder(singularEntryCalendarEditorForm,reservationService,resEntryService);
                        singularEntryFormBinder.addBindingAndValidationToExistingResEntry(currentEntry);
                        singularEditDialog.setDraggable(true);
                        add(singularEditDialog);
                        singularEditDialog.open();

                    });

                    add(editDialog);
                    editDialog.open();
                    calendarFormExistEntry.getSubmitButton().addClickListener((e) -> {
                            editDialog.close();
                            entryDataProvider.refreshAll();


                    });
                    calendarFormExistEntry.getCancelButton().addClickListener((e) ->editDialog.close());

                    calendarFormExistEntry.getDeleteButton().addClickListener((e) -> {
                        editDialog.close();
                        entryDataProvider.refreshAll();

                    });
                    singularEntryCalendarEditorForm.getSubmitButton().addClickListener(e -> {
                        singularEditDialog.close();
                        editDialog.close();
                        entryDataProvider.refreshAll();
                    });

                    singularEntryCalendarEditorForm.getCancelButton().addClickListener((e) ->singularEditDialog.close());

                    singularEntryCalendarEditorForm.getDeleteButton().addClickListener(e -> {
                        singularEditDialog.close();
                        editDialog.close();
                        entryDataProvider.refreshAll();
                    });
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

        });

        HasText intervalLabel = new Span();
        // combo box to select a view for the calendar, like "monthly", "weekly", ...
        Set<CalendarViewImpl> calendarViewSet = new HashSet<>();
        calendarViewSet.add(CalendarViewImpl.DAY_GRID_MONTH);
        calendarViewSet.add(CalendarViewImpl.TIME_GRID_WEEK);
        calendarViewSet.add(CalendarViewImpl.LIST_YEAR);
        ComboBox<CalendarViewImpl> viewBox = new ComboBox<>("", calendarViewSet);

        viewBox.setItemLabelGenerator(item-> item.getName().replace("DAY_GRID","").replace("TIME_GRID","").replace("LIST","").replace("_"," "));

        viewBox.addValueChangeListener(e -> {
            CalendarViewImpl value = e.getValue();
            calendar.changeView(value == null ? CalendarViewImpl.DAY_GRID_MONTH : value);
        });
        viewBox.setValue(CalendarViewImpl.DAY_GRID_MONTH);

        /*
         * The view rendered listener is called when the view has been rendererd on client side
         * and FC is aware of the current shown interval. Might be accessible more directly in
         * future.
         */

        calendar.addDatesRenderedListener(event -> {
            LocalDate intervalStart = event.getIntervalStart();
            CalendarViewImpl cView = viewBox.getValue();

        String formattedInterval = String.valueOf(intervalStart);

        intervalLabel.setText(formattedInterval);
    });
        LocalDate now = LocalDate.now(ZoneId.systemDefault());
        List<Integer> selectableYears = IntStream
                .range(now.getYear() - 1, now.getYear() + 99).boxed()
                .collect(Collectors.toList());
        yearPicker=new ComboBox<>("", selectableYears);
        yearPicker.setValue(now.getYear());
        monthPicker= new ComboBox<>("", Month.values());

       monthPicker.setValue(now.getMonth());
        monthPicker.setItemLabelGenerator(
                m -> m.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        monthPicker.setWidth(9, Unit.EM);
        monthPicker.addValueChangeListener(e -> {
            LocalDate selectedDate = LocalDate.of(Year.now().getValue(),e.getValue().getValue(),1);
            calendar.gotoDate(selectedDate);
        });

        calendar.setFirstDay(DayOfWeek.MONDAY);
        Button previousMonth = new Button(new Icon(VaadinIcon.CHEVRON_LEFT));
        previousMonth.addClickListener(event-> {

            monthPicker.setValue(monthPicker.getValue().minus(1));
            if(monthPicker.getValue().getValue()==12){
                int currentYear= yearPicker.getValue();
                int yearReset= currentYear-1;
                yearPicker.setValue(yearReset);
            }
            calendar.gotoDate(LocalDate.of(yearPicker.getValue(),monthPicker.getValue(),1));
        });

        Button nextMonth = new Button(new Icon(VaadinIcon.CHEVRON_RIGHT));
        nextMonth.addClickListener(event -> {

            monthPicker.setValue(monthPicker.getValue().plus(1));
            if(monthPicker.getValue().getValue()==1){
                int currentYear= yearPicker.getValue();
                yearPicker.setValue(currentYear+1);
            }
            calendar.gotoDate(LocalDate.of(yearPicker.getValue(),monthPicker.getValue(),1));
        });


        Alignment centered = Alignment.CENTER;
        parent.setAlignItems(centered);
        parent.add(calendar);
        controllLayout.add(timeControllLayout);
        timeControllLayout.add(viewBox,previousMonth,yearPicker,monthPicker,nextMonth);
        setId("calendar-view");
        timeControllLayout.setId("time-control-cal-view");
        controllLayout.setId("control-cal-view");
    }


    private int getReservationDetailsOfEntry(Entry entry) throws SQLException {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select reservation_id from bookingtool.reservation_entry_ids where entry_ids ='" + entry.getId() +"'" ;
        ResultSet rs = statement.executeQuery(sql);
        int resID = 0;
        while (rs.next()) {
            resID = rs.getInt("reservation_id");
        }
        c.close();
        return resID;
    }

    public boolean checkUserPermission (UserService userService,Entry entry, ReservationService reservationService) throws SQLException {
        Reservation clickedRes = reservationService.findReservationById((long) getReservationDetailsOfEntry(entry));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserPrincipal customUser = (MyUserPrincipal ) authentication.getPrincipal();
        ApplicationUser currentUser = userService.findUserbyUsername(customUser.getUsername());
        UserRole userRoleOfCU = currentUser.getUserRole();

        return userRoleOfCU.getId() != 3L || Objects.equals(clickedRes.getUser().getId(), currentUser.getId());
    }

}


