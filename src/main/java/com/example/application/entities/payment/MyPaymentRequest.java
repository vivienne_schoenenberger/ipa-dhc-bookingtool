package com.example.application.entities.payment;


import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyPaymentRequest  {
    @SerializedName("amount")
    private MyAmount amount;
    @SerializedName("reference")
    private String reference;
    @SerializedName("paymentMethod")
    private MyPaymentMethod paymentMethod;
    @SerializedName("returnUrl")
    private String returnUrl;
    @SerializedName("merchantAccount")
    private String merchantAccount;
    @SerializedName("countryCode")
    private String countryCode;

}
