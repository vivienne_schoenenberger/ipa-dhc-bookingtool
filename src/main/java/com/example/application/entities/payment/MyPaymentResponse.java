package com.example.application.entities.payment;

import com.example.application.entities.Reservation;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyPaymentResponse {
    @SerializedName("resultCode")
    private String resultCode;
    @SerializedName("action")
    private MyPaymentResponseAction action;
    private Reservation reservation;
    private Exception error;
}
