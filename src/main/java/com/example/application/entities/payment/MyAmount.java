package com.example.application.entities.payment;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyAmount {
    @SerializedName("currency")
    private String currency;
    @SerializedName("value")
    private float value;
}
