package com.example.application.entities.payment;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyPaymentMethod {
    @SerializedName("type")
    private String type;
}
