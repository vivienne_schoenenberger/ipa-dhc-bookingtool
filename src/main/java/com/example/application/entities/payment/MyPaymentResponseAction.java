package com.example.application.entities.payment;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyPaymentResponseAction {
    @SerializedName("paymentMethodType")
    private String paymentMethodType;
    @SerializedName("url")
    private String url;
    @SerializedName("method")
    private String method;
    @SerializedName("type")
    private String type;
}
