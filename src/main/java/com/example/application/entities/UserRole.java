package com.example.application.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
@Entity
public class UserRole {
    @EqualsAndHashCode.Include
    @Id
    private Long id;

    public UserRole(Long id, String bezeichnung) {
        this.id = id;
        this.bezeichnung = bezeichnung;
    }

    @NotBlank
    private String bezeichnung;

    public UserRole() {

    }
}
