package com.example.application.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Room {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonIgnore
    @OneToMany(fetch=FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<Workspace> workspaceList;
    @NotBlank
    private String bezeichnung;

    @NotBlank
    private String roomColor;
    @Override
    public String toString (){
        return bezeichnung;
    }


    private String imageName;
    @NotNull
    private float preis;

    private String roomDescription;
    private String roomLocation;

}
