package com.example.application.entities;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
public class Reservation {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long totalTime;

    @NotNull
    private LocalDateTime end_time;

    @NotNull
    private LocalDateTime start_time;

    @NotNull
    private LocalDate end_date;

    @NotNull
    private LocalDate start_date;


    @ManyToOne
    private ReservationStatus status_id;


    @NotNull
    @OneToOne
    private ApplicationUser user;

    @NotBlank
    private String description;
    @NotBlank
    private String title;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<DayOfWeek> reoccurringDays;

    private boolean isRecurring;

    private boolean isSpecificTime;

    private boolean isFullDay;

    private boolean singleEntryEdited;

    private String color;


    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> entryIds;

    @ManyToOne
    private Room room;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Workspace> workspaceList;

    private LocalDateTime entryStartTime;

    private LocalDateTime entryEndTime;

    private LocalDate entryStartDate;

    private LocalDate entryEndDate;


    @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private Set<ResEntry> resEntries;

    private boolean onlyRoom;

    private boolean onlyWorkspace;
    @NotNull
    private float costs;

    private String paymentStatus;

}
