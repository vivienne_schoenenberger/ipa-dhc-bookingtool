package com.example.application.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.awt.*;

@Getter
@Setter
@Entity

public class Workspace {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @ManyToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL )
    private Room room;

    @NotBlank
    private String bezeichnung;

    public Workspace(Long id, String bezeichnung, Room room) {
        this.id = id;
        this.bezeichnung = bezeichnung;
        this.room = room;
    }

    public Workspace() {

    }

    @Override
    public String toString(){
        return bezeichnung;
    }


    private Color workspaceColor;
    @NotNull
    private float preis;

    private String workspaceDescription;
    private String workspaceLocation;

}
