package com.example.application.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
public class ResEntry {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long resEntryId;

    @NotBlank
    private String title;
    @NotBlank
    private String description;

    @NotNull
    private LocalDateTime startTime;
    @NotNull
    private LocalDateTime endTime;

    private boolean allDay;
    @NotBlank
    private String entryID;
    @ManyToOne
    private Room room;

    private String color;


    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Workspace> workspaceList;

    private boolean onlyRoom;

    private boolean onlyWorkspace;
    private float resEntryTime;


}
