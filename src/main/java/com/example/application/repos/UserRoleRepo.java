package com.example.application.repos;

import com.example.application.entities.ApplicationUser;
import com.example.application.entities.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepo extends JpaRepository<UserRole, Long> {

    UserRole getUserRoleById (Long id);
}
