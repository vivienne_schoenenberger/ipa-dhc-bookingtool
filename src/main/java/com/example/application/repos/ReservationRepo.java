package com.example.application.repos;

import com.example.application.entities.ApplicationUser;
import com.example.application.entities.Reservation;
import com.example.application.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepo extends JpaRepository<Reservation,Long> {

    Reservation findReservationById (Long id);
    Reservation findReservationByEntryIds (String entryId);

    List<Reservation> findReservationsByRoom (Room room);

    List<Reservation> findReservationsByUser (ApplicationUser user);

}
