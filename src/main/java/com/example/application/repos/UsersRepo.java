package com.example.application.repos;

import com.example.application.entities.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepo extends JpaRepository<ApplicationUser, Long> {

    ApplicationUser findByUsername(String username);

    ApplicationUser findApplicationUserById(Long id);
    ApplicationUser findByEmail(String email);
    ApplicationUser findByResetPasswordToken(String token);

    boolean existsApplicationUserByUsername(String username);




}
