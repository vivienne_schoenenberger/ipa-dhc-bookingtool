package com.example.application.repos;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Set;

public interface WorkspaceRepo extends JpaRepository<Workspace,Long> {

    Workspace findWorkspaceById(Long id);
    Set<Workspace> findAllByRoom(Room room);
}
