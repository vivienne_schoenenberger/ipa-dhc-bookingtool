package com.example.application.repos;

import com.example.application.entities.ResEntry;
import com.example.application.entities.Reservation;
import com.example.application.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Set;

@Repository
public interface ResEntryRepo extends JpaRepository<ResEntry,Long> {
    ResEntry findResEntryByEntryID(String entryId);

    Set<ResEntry> findAllByRoom(Room room);
}
