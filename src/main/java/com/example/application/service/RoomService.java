package com.example.application.service;

import com.example.application.entities.Room;
import com.example.application.repos.RoomRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;


import java.util.Collection;

@Service
public class RoomService implements CrudListener<Room> {

    private final RoomRepo roomRepo;

    @Autowired
    public RoomService(RoomRepo roomRepo) {
        this.roomRepo = roomRepo;
    }

    @Override
    public Collection<Room> findAll() {
        return roomRepo.findAll();
    }

    @Override
    public Room add(Room room) {
        return roomRepo.save(room);
    }

    @Override
    public Room update(Room room) {
        return roomRepo.save(room);
    }

    @Override
    public void delete(Room room) {
        roomRepo.delete(room);
    }

    public Room findRoomById (Long id){
        return roomRepo.findRoomById(id);
    }

}
