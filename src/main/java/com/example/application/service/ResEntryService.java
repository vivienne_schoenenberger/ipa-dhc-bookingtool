package com.example.application.service;

import com.example.application.entities.ResEntry;
import com.example.application.entities.Room;
import com.example.application.repos.ResEntryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Set;

@Service
@Transactional
public class ResEntryService implements CrudListener<ResEntry>{
    private final ResEntryRepo resEntryRepo;
    @Autowired
    public ResEntryService(ResEntryRepo resEntryRepo) {
        this.resEntryRepo = resEntryRepo;
    }

    @Override
    public Collection<ResEntry> findAll() {
        return null;
    }

    @Override
    public ResEntry add(ResEntry resEntry) {
        return null;
    }

    @Override
    public ResEntry update(ResEntry resEntry) {
        return resEntryRepo.save(resEntry);
    }

    @Override
    public void delete(ResEntry resEntry) {
        resEntryRepo.delete(resEntry);
    }
    public ResEntry findResEntryByEntryId(String entryId){
        return resEntryRepo.findResEntryByEntryID(entryId);
    }

    public Set<ResEntry> findAllByRoom(Room room){ return resEntryRepo.findAllByRoom(room);

    }
}
