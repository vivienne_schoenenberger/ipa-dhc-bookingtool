package com.example.application.service;

import com.example.application.entities.UserRole;
import com.example.application.repos.UserRoleRepo;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {
    private final UserRoleRepo userRoleRepo;

    public UserRoleService(UserRoleRepo userRoleRepo) {
        this.userRoleRepo = userRoleRepo;
    }
    public UserRole getUserRoleById(Long id){return userRoleRepo.getUserRoleById( id);}


}
