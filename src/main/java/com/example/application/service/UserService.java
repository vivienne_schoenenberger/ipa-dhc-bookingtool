package com.example.application.service;

import com.example.application.repos.UsersRepo;
import com.example.application.entities.ApplicationUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;

import java.util.Collection;
import java.util.Optional;

@Service
public class UserService implements CrudListener<ApplicationUser> {

    private final UsersRepo usersRepo;


    @Autowired
    public UserService(UsersRepo usersRepo) {
        this.usersRepo = usersRepo;
    }

    @Override
    public Collection<ApplicationUser> findAll() {
        return usersRepo.findAll();
    }

    @Override
    public ApplicationUser add(ApplicationUser applicationUser) {
       return usersRepo.save(applicationUser);
    }

    @Override
    public ApplicationUser update(ApplicationUser applicationUser) {
        return usersRepo.save(applicationUser);
    }

    @Override
    public void delete(ApplicationUser applicationUser) {
        usersRepo.delete(applicationUser);
    }


    public ApplicationUser findUserbyUsername(String username) {
        return usersRepo.findByUsername(username);
    }

    public ApplicationUser findUserByID(Long id){return usersRepo.findApplicationUserById(id);}

    public boolean checkIfUsernameExists(String username){ return usersRepo.existsApplicationUserByUsername(username);}

    public void update(Optional<ApplicationUser> existingItem) {
    }
    public ApplicationUser findByResetPwToken(String token){
        return usersRepo.findByResetPasswordToken(token);
    }
    public ApplicationUser findByEmail(String email){
        return usersRepo.findByEmail(email);
    }
    public void updateResetPasswordToken(String token, String email) {
        ApplicationUser user = usersRepo.findByEmail(email);
        if (user != null) {
            user.setResetPasswordToken(token);
            usersRepo.save(user);
        }
    }
    public void updatePassword(ApplicationUser user, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodedPassword);

        user.setResetPasswordToken(null);
        usersRepo.save(user);
    }

}
