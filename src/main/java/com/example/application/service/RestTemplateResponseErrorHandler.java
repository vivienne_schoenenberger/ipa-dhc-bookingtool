package com.example.application.service;

import com.vaadin.flow.router.NotFoundException;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;
/**
 * Error Handling for resttemplate
 * the user should be navigated to failure status page accordingly
 *
 */
@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse httpResponse)
            throws IOException {
        return (
                httpResponse.getStatusCode().series() == CLIENT_ERROR
                        || httpResponse.getStatusCode().series() == SERVER_ERROR);
    }
    @Override
    public void handleError(ClientHttpResponse httpResponse)
            throws IOException {

        if (httpResponse.getStatusCode()
                .series() == HttpStatus.Series.SERVER_ERROR) {
            LoggerFactory.getLogger(getClass())
                    .info("A server error has occured");
        } else if (httpResponse.getStatusCode()
                .series() == HttpStatus.Series.CLIENT_ERROR) {
            LoggerFactory.getLogger(getClass())
                    .info("A client error has occured");
            //throw new NotFoundException();
            if (httpResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                LoggerFactory.getLogger(getClass())
                        .info("Service or Page not found");
                throw new NotFoundException();
            }
        }
    }
}
