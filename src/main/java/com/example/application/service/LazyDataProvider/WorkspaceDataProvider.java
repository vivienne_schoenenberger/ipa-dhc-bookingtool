package com.example.application.service.LazyDataProvider;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.service.RoomService;
import com.example.application.service.WorkspaceService;
import com.vaadin.flow.component.crud.CrudFilter;
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.SortDirection;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Comparator.naturalOrder;
import static org.jsoup.parser.Tag.valueOf;

public class WorkspaceDataProvider extends AbstractBackEndDataProvider<Workspace, CrudFilter> {

    private final WorkspaceService workspaceService;
    private final RoomService roomService;
    private Consumer<Long> sizeChangeListener;

    @Autowired
    public WorkspaceDataProvider(WorkspaceService workspaceService, RoomService roomService) {
        this.workspaceService = workspaceService;
        this.roomService = roomService;
    }

    @Override
    protected Stream<Workspace> fetchFromBackEnd(Query<Workspace, CrudFilter> query) {
        int offset = query.getOffset();
        int limit = query.getLimit();

        Stream<Workspace> stream = workspaceService.findAll().stream();

        if (query.getFilter().isPresent()) {
            stream = stream
                    .filter(workspacePredicate(query.getFilter().get()))
                    .sorted(workspaceComparator(query.getFilter().get()));
        }

        return stream.skip(offset).limit(limit);
    }

    @Override
    protected int sizeInBackEnd(Query<Workspace, CrudFilter> query) {
        long count = fetchFromBackEnd(query).count();

        if (sizeChangeListener != null) {
            sizeChangeListener.accept(count);
        }

        return (int) count;
    }

    static Predicate<Workspace> workspacePredicate(CrudFilter filter) {
        // For RDBMS just generate a WHERE clause
        return filter.getConstraints().entrySet().stream()
                .map(constraint -> (Predicate<Workspace>) workspace -> {
                    try {
                        Object value = valueOf(constraint.getKey(), workspace);
                        return value != null && value.toString().toLowerCase()
                                .contains(constraint.getValue().toLowerCase());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .reduce(Predicate::and)
                .orElse(e -> true);
    }

    private static Object valueOf(String fieldName, Workspace workspace) {

        try {
            Field field = Workspace.class.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(workspace);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    static Comparator<Workspace> workspaceComparator(CrudFilter filter) {
        // For RDBMS just generate an ORDER BY clause
        return filter.getSortOrders().entrySet().stream()
                .map(sortClause -> {
                    try {
                        Comparator<Workspace> comparator
                                = Comparator.comparing(workspace ->
                                (Comparable) valueOf(sortClause.getKey(), workspace));

                        if (sortClause.getValue() == SortDirection.DESCENDING) {
                            comparator = comparator.reversed();
                        }

                        return comparator;
                    } catch (Exception ex) {
                        return (Comparator<Workspace>) (o1, o2) -> 0;
                    }
                })
                .reduce(Comparator::thenComparing)
                .orElse((o1, o2) -> 0);
    }


    public void persist(Workspace workspace) {
        if (workspace.getId() == null) {
            workspace.setId(workspaceService.findAll()
                    .stream()
                    .map(Workspace::getId)
                    .max(naturalOrder())
                    .orElse(0L) + 1);
        }


        final Optional<Workspace> existingItem = Optional.ofNullable(workspaceService.findWorkspaceById((long) Math.toIntExact(workspace.getId())));
        if (existingItem.isPresent()) {
            workspaceService.update(workspace);
            Room updatedRoom = workspace.getRoom();
            if(updatedRoom!=null) {
                Set<Workspace> addedRoomWorkspaceList = new HashSet<>(updatedRoom.getWorkspaceList());
                addedRoomWorkspaceList.add(workspace);
                updatedRoom.setWorkspaceList(addedRoomWorkspaceList);
                roomService.update(workspace.getRoom());
            }

        } else {
            workspaceService.add(workspace);
            Room addedRoom = workspace.getRoom();
            if(addedRoom!=null) {
                Set<Workspace> addedRoomWorkspaceList = new HashSet<>(addedRoom.getWorkspaceList());
                addedRoomWorkspaceList.add(workspace);
                addedRoom.setWorkspaceList(addedRoomWorkspaceList);
                roomService.update(workspace.getRoom());
            }

        }

    }

    Optional<Workspace> find(Integer id) {
        return workspaceService.findAll()
                .stream()
                .filter(entity -> entity.getId().equals(id))
                .findFirst();
    }

    public void delete(Workspace item) {

        if(item.getRoom()!= null){
            Set<Workspace> workspaceSet = new HashSet<>(item.getRoom().getWorkspaceList());
            workspaceSet.remove(item);
            item.getRoom().setWorkspaceList(workspaceSet);
            roomService.update(item.getRoom());
        }
        workspaceService.delete(item);


    }
    public void roomToUpdate (Room room, Workspace workspace){
        Set<Workspace> workspaceList = room.getWorkspaceList();
        workspaceList.remove(workspace);
        roomService.update(room);
    }
}
