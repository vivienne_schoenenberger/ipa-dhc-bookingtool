package com.example.application.service.LazyDataProvider;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.service.RoomService;
import com.example.application.service.WorkspaceService;
import com.vaadin.flow.component.crud.CrudFilter;
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.SortDirection;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Comparator.naturalOrder;

public class RoomDataProvider extends AbstractBackEndDataProvider<Room, CrudFilter> {

    private final RoomService roomService;
    private final WorkspaceService workspaceService;
    private Consumer<Long> sizeChangeListener;

    @Autowired
    public RoomDataProvider(RoomService roomService, WorkspaceService workspaceService) {
        this.roomService = roomService;
        this.workspaceService = workspaceService;


    }


    @Override
    protected Stream<Room> fetchFromBackEnd(Query<Room, CrudFilter> query) {
        int offset = query.getOffset();
        int limit = query.getLimit();

        Stream<Room> stream = roomService.findAll().stream();

        if (query.getFilter().isPresent()) {
            stream = stream
                    .filter(roomPredicate(query.getFilter().get()))
                    .sorted(roomComparator(query.getFilter().get()));
        }

        return stream.skip(offset).limit(limit);
    }

    @Override
    protected int sizeInBackEnd(Query<Room, CrudFilter> query) {
        long count = fetchFromBackEnd(query).count();

        if (sizeChangeListener != null) {
            sizeChangeListener.accept(count);
        }

        return (int) count;
    }

    static Predicate<Room> roomPredicate(CrudFilter filter) {
        // For RDBMS just generate a WHERE clause
        return filter.getConstraints().entrySet().stream()
                .map(constraint -> (Predicate<Room>) room -> {
                    try {
                        Object value = valueOf(constraint.getKey(), room);
                        return value != null && value.toString().toLowerCase()
                                .contains(constraint.getValue().toLowerCase());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .reduce(Predicate::and)
                .orElse(e -> true);
    }

    private static Object valueOf(String fieldName, Room room) {

        try {
            Field field = Room.class.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(room);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    static Comparator<Room> roomComparator(CrudFilter filter) {
        // For RDBMS just generate an ORDER BY clause
        return filter.getSortOrders().entrySet().stream()
                .map(sortClause -> {
                    try {
                        Comparator<Room> comparator
                                = Comparator.comparing(room ->
                                (Comparable) valueOf(sortClause.getKey(), room));

                        if (sortClause.getValue() == SortDirection.DESCENDING) {
                            comparator = comparator.reversed();
                        }

                        return comparator;
                    } catch (Exception ex) {
                        return (Comparator<Room>) (o1, o2) -> 0;
                    }
                })
                .reduce(Comparator::thenComparing)
                .orElse((o1, o2) -> 0);
    }


    public void persist(Room room) {
        if (room.getId() == null) {
            room.setId(roomService.findAll()
                    .stream()
                    .map(Room::getId)
                    .max(naturalOrder())
                    .orElse(0L) + 1);
        }


        final Optional<Room> existingItem = Optional.ofNullable(roomService.findRoomById((long) Math.toIntExact(room.getId())));
        if (existingItem.isPresent()) {
            roomService.update(room);
            for(Workspace workspace:room.getWorkspaceList()){
                workspace.setRoom(room);
                workspaceService.update(workspace);
            }
        } else {
            roomService.add(room);
            for(Workspace workspace:room.getWorkspaceList()){
                workspace.setRoom(room);
                workspaceService.update(workspace);
            }
            }


    }

    Optional<Room> find(Integer id) {
        return roomService.findAll()
                .stream()
                .filter(entity -> entity.getId().equals(id))
                .findFirst();
    }

    public void delete(Room item) {

        roomService.delete(item);

    }

    public Set<Workspace> updatedWorkspaceSelection(){
        return  workspaceService.findAllByRoom(null);

    }


}
