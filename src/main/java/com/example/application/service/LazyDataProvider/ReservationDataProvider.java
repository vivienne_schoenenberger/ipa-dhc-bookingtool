package com.example.application.service.LazyDataProvider;

import com.example.application.entities.Reservation;
import com.example.application.service.ReservationService;
import com.vaadin.flow.component.crud.CrudFilter;
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.SortDirection;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Comparator.naturalOrder;

public class ReservationDataProvider extends AbstractBackEndDataProvider<Reservation, CrudFilter> {

    private final ReservationService reservationService;

    private Consumer<Long> sizeChangeListener;

    @Autowired
    public ReservationDataProvider(ReservationService reservationService) {
        this.reservationService = reservationService;
    }
    @Override
    protected Stream<Reservation> fetchFromBackEnd(Query<Reservation, CrudFilter> query) {
        int offset = query.getOffset();
        int limit = query.getLimit();

        Stream<Reservation> stream = reservationService.findAll().stream();

        if(query.getFilter().isPresent()){
            stream = stream
                    .filter(reservationPredicate(query.getFilter().get()))
                    .sorted(reservationComparator(query.getFilter().get()));
        }
        return stream.skip(offset).limit(limit);
    }
    @Override
    protected int sizeInBackEnd(Query<Reservation, CrudFilter> query) {
        long count = fetchFromBackEnd(query).count();

        if (sizeChangeListener != null) {
            sizeChangeListener.accept(count);
        }

        return (int) count;
    }
    static Predicate<Reservation> reservationPredicate(CrudFilter filter){
        return filter.getConstraints().entrySet().stream()
                .map(constraint ->(Predicate<Reservation>) room -> {
                    try {
                        Object value = valueOf(constraint.getKey(), room);
                        return value != null && value.toString().toLowerCase()
                                .contains(constraint.getValue().toLowerCase());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .reduce(Predicate::and)
                .orElse(e -> true);
    }
    private static Object valueOf(String fieldName, Reservation reservation){
        try {
            Field field = Reservation.class.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(reservation);
        } catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }
    static Comparator<Reservation> reservationComparator(CrudFilter filter){
        return filter.getSortOrders().entrySet().stream()
                .map(sortClause -> {
                    try {
                        Comparator<Reservation> comparator
                                = Comparator.comparing(room ->
                                (Comparable) valueOf(sortClause.getKey(), room));

                        if (sortClause.getValue() == SortDirection.DESCENDING) {
                            comparator = comparator.reversed();
                        }

                        return comparator;
                    } catch (Exception ex) {
                        return (Comparator<Reservation>) (o1, o2) -> 0;
                    }
                })
                .reduce(Comparator::thenComparing)
                .orElse((o1, o2) -> 0);
    }
    public void persist(Reservation reservation) {
        if (reservation.getId() == null) {
            reservation.setId(reservationService.findAll()
                    .stream()
                    .map(Reservation::getId)
                    .max(naturalOrder())
                    .orElse(0L) + 1);
        }
        final Optional<Reservation> existingItem = Optional.ofNullable(reservationService.findReservationById((long) Math.toIntExact(reservation.getId())));
        if (existingItem.isPresent()) {
            reservationService.update(reservation);
        } else {
            reservationService.add(reservation);
        }
    }
    Optional<Reservation> find(Integer id) {
        return reservationService.findAll()
                .stream()
                .filter(entity -> entity.getId().equals(id))
                .findFirst();
    }

    public void delete(Reservation item) {
        reservationService.delete(item);

    }
}
