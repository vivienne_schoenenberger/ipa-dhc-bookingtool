package com.example.application.service.LazyDataProvider;

import com.example.application.entities.ApplicationUser;
import com.example.application.security.MyPasswordEncoder;
import com.example.application.service.UserService;
import com.vaadin.flow.component.crud.CrudFilter;
import com.vaadin.flow.data.provider.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Comparator.naturalOrder;


public class UserDataProvider extends AbstractBackEndDataProvider<ApplicationUser, CrudFilter> {


    private Consumer<Long> sizeChangeListener;
    //List<ApplicationUser> DATABASE;


    private final UserService userService;

    @Autowired
    public MyPasswordEncoder passwordEncoder(){
        return new MyPasswordEncoder();
    }
    @Autowired
    public UserDataProvider(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected Stream<ApplicationUser> fetchFromBackEnd(Query<ApplicationUser, CrudFilter> query) {

        int offset = query.getOffset();
        int limit = query.getLimit();

        Stream<ApplicationUser> stream = userService.findAll().stream();

        if (query.getFilter().isPresent()) {
            stream = stream
                    .filter(predicate(query.getFilter().get()))
                    .sorted(comparator(query.getFilter().get()));
        }

        return stream.skip(offset).limit(limit);
    }

    @Override
    protected int sizeInBackEnd(Query<ApplicationUser, CrudFilter> query) {
        // For RDBMS just execute a SELECT COUNT(*) ... WHERE query
        long count = fetchFromBackEnd(query).count();

        if (sizeChangeListener != null) {
            sizeChangeListener.accept(count);
        }

        return (int) count;
    }

    void setSizeChangeListener(Consumer<Long> listener) {
        sizeChangeListener = listener;
    }

    static Predicate<ApplicationUser> predicate(CrudFilter filter) {
        // For RDBMS just generate a WHERE clause
        return filter.getConstraints().entrySet().stream()
                .map(constraint -> (Predicate<ApplicationUser>) person -> {
                    try {
                        Object value = valueOf(constraint.getKey(), person);
                        return value != null && value.toString().toLowerCase()
                                .contains(constraint.getValue().toLowerCase());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .reduce(Predicate::and)
                .orElse(e -> true);
    }

    static Comparator<ApplicationUser> comparator(CrudFilter filter) {
        // For RDBMS just generate an ORDER BY clause
        return filter.getSortOrders().entrySet().stream()
                .map(sortClause -> {
                    try {
                        Comparator<ApplicationUser> comparator
                                = Comparator.comparing(person ->
                                (Comparable) valueOf(sortClause.getKey(), person));

                        if (sortClause.getValue() == SortDirection.DESCENDING) {
                            comparator = comparator.reversed();
                        }

                        return comparator;
                    } catch (Exception ex) {
                        return (Comparator<ApplicationUser>) (o1, o2) -> 0;
                    }
                })
                .reduce(Comparator::thenComparing)
                .orElse((o1, o2) -> 0);
    }

    private static Object valueOf(String fieldName, ApplicationUser person) {
        try {
            Field field = ApplicationUser.class.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(person);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void persist(ApplicationUser item) {
        if (item.getId() == null) {
            item.setId(userService.findAll()
                    .stream()
                    .map(ApplicationUser::getId)
                    .max(naturalOrder())
                    .orElse(0L) + 1);
        }


        final Optional<ApplicationUser> existingItem = Optional.ofNullable(userService.findUserByID((long) Math.toIntExact(item.getId())));
        if (existingItem.isPresent()) {
            userService.update(item);
        } else {
            String defaultPassword = "defaultPassword";
            item.setPassword(passwordEncoder().encode(defaultPassword));
            userService.add(item);
        }
    }

    Optional<ApplicationUser> find(Integer id) {
        return userService.findAll()
                .stream()
                .filter(entity -> entity.getId().equals(id))
                .findFirst();
    }

    public void delete(ApplicationUser item) {
        userService.delete(item);

    }
}

