package com.example.application.service.LazyDataProvider;

import com.example.application.entities.ResEntry;
import com.example.application.entities.Reservation;
import com.example.application.filter.RecurringDaysFilter;
import com.example.application.service.ResEntryService;
import com.example.application.service.ReservationService;
import lombok.NonNull;
import org.vaadin.stefan.fullcalendar.Entry;
import org.vaadin.stefan.fullcalendar.dataprovider.AbstractEntryProvider;
import org.vaadin.stefan.fullcalendar.dataprovider.EntryQuery;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

@Transactional
public class EntryDataProvider extends AbstractEntryProvider<Entry> {

    private final ReservationService reservationService;
    private final ResEntryService resEntryService;



    private boolean notAvailable;

    public EntryDataProvider(ReservationService reservationService, ResEntryService resEntryService) {
        this.reservationService = reservationService;
        this.resEntryService = resEntryService;

    }

    @Override
    public Stream<Entry> fetch(@NonNull EntryQuery entryQuery) {

        // er holt sich alle reservationen und wandelt sie in Entries um
        List<String> recEntryIds = new ArrayList<>();
        List<Entry> entryArrayList = new ArrayList<>();
        Set<ResEntry> resEntryArrayList = new HashSet<>();

        List<Reservation> reservationArrayList = new ArrayList<>(reservationService.findAll());
        for (Reservation reservation : reservationArrayList) {
            if (reservation.getEntryIds().size() ==0) {
                RecurringDaysFilter recurringDaysFilter = new RecurringDaysFilter();

                if (reservation.isRecurring()) {
                    LocalDate recStart;
                    LocalDate recEnd;
                    if (reservation.isSpecificTime()) {
                        recStart = reservation.getStart_time().toLocalDate();
                        recEnd = reservation.getEnd_time().toLocalDate();
                    } else {
                        recStart = reservation.getStart_date();
                        recEnd = reservation.getEnd_date();
                    }
                    for (LocalDate day : recurringDaysFilter.recurringDays(recStart, recEnd, reservation.getReoccurringDays())) {

                        Entry recEntry = new Entry();
                        recEntry.setTitle(reservation.getTitle());
                        recEntry.setDescription(reservation.getDescription());
                        recEntry.setColor(reservation.getColor());
                        if (reservation.isSpecificTime()) {
                            recEntry.setStart(day.atStartOfDay().plusHours(reservation.getStart_time().getHour()).plusMinutes(reservation.getStart_time().getMinute()));
                            recEntry.setEnd(day.atStartOfDay().plusHours(reservation.getEnd_time().getHour()).plusMinutes(reservation.getEnd_time().getMinute()));

                        } else {
                            recEntry.setStart(day);
                            recEntry.setEnd(day);
                            recEntry.isAllDay();
                        }
                        ResEntry recResEntry = new ResEntry();
                        recResEntry.setTitle(reservation.getTitle());
                        recResEntry.setDescription(reservation.getDescription());
                        recResEntry.setEntryID(recEntry.getId());
                        recResEntry.setRoom(reservation.getRoom());
                        recResEntry.setColor(reservation.getColor());
                        recResEntry.setWorkspaceList(reservation.getWorkspaceList());
                        recResEntry.setOnlyRoom(reservation.isOnlyRoom());
                        recResEntry.setOnlyWorkspace(reservation.isOnlyWorkspace());
                        if (reservation.isSpecificTime()) {
                            recResEntry.setStartTime(day.atStartOfDay().plusHours(reservation.getStart_time().getHour()).plusMinutes(reservation.getStart_time().getMinute()));
                            recResEntry.setEndTime(day.atStartOfDay().plusHours(reservation.getEnd_time().getHour()).plusMinutes(reservation.getEnd_time().getMinute()));

                        } else {
                            recResEntry.setStartTime(LocalDateTime.from(day.atStartOfDay()).plusHours(5));
                            recResEntry.setEndTime(LocalDateTime.from(day.atTime(22,0)));
                            recResEntry.setAllDay(true);
                        }

                            reservationService.update(reservation);
                            recEntryIds.add(recEntry.getId());
                            reservation.setEntryIds(recEntryIds);
                            entryArrayList.add(recEntry);
                            resEntryArrayList.add(recResEntry);
                            recEntry.setColor(reservation.getColor());



                    }

                    reservation.setResEntries(resEntryArrayList);
                    reservationService.update(reservation);
                } else {
                    Entry entry = new Entry();
                    entry.setTitle(reservation.getTitle());
                    entry.setStart(reservation.getStart_time());
                    entry.setEnd(reservation.getEnd_time());
                    if (reservation.getStart_time() == null || reservation.getEnd_time() == null) {
                        entry.setStart(reservation.getStart_date());
                        entry.setEnd(reservation.getEnd_date());
                    }
                    entry.setDescription(reservation.getDescription());
                    entry.setColor(reservation.getColor());

                    ResEntry resEntry = new ResEntry();
                    resEntry.setTitle(reservation.getTitle());
                    resEntry.setDescription(reservation.getDescription());
                    resEntry.setEntryID(entry.getId());
                    resEntry.setRoom(reservation.getRoom());
                    resEntry.setColor(reservation.getColor());
                    resEntry.setWorkspaceList(reservation.getWorkspaceList());
                    if (reservation.isSpecificTime()) {
                        resEntry.setStartTime(reservation.getStart_time());
                        resEntry.setEndTime(reservation.getEnd_time());

                    } else {
                        resEntry.setStartTime(reservation.getStart_date().atStartOfDay().plusHours(5));
                        resEntry.setEndTime(reservation.getStart_date().atTime(22,0,0));
                        resEntry.setAllDay(true);
                    }


                        reservationService.update(reservation);
                        recEntryIds.add(entry.getId());
                        reservation.setEntryIds(recEntryIds);
                        reservation.getResEntries().add(resEntry);
                        reservationService.update(reservation);
                        entryArrayList.add(entry);

                }


            } else {
                for(ResEntry resEntry: reservation.getResEntries()){
                    Entry entry = new Entry(resEntry.getEntryID());
                    entry.setTitle(reservation.getTitle());
                    entry.setStart(resEntry.getStartTime());
                    entry.setEnd(resEntry.getEndTime());
                    entry.setDescription(resEntry.getDescription());
                    entry.setColor(resEntry.getColor());
                    entryArrayList.add(entry);
                }
            }

        }
        return entryArrayList.stream();
    }


    @Override
    public Optional<Entry> fetchById(@NonNull String s) {
        return Optional.empty();
    }


}
