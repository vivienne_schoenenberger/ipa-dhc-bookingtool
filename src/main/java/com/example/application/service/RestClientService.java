package com.example.application.service;

import com.example.application.entities.Reservation;
import com.example.application.entities.payment.MyAmount;
import com.example.application.entities.payment.MyPaymentMethod;
import com.example.application.entities.payment.MyPaymentRequest;
import com.example.application.entities.payment.MyPaymentResponse;
import com.nimbusds.jose.shaded.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Service
public class RestClientService {
    //@Autowired
    private RestTemplate restTemplate;
    private HttpHeaders headers;
    private MyPaymentResponse myPaymentResponse;
    private JSONObject shrotURLResponse;
    private JSONObject paymentDetailResponse;
    @Autowired
    public RestClientService(RestTemplateBuilder restTemplateBuilder) {
         restTemplate = restTemplateBuilder
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
    }


    /**
     * API for Payment creation
     *
     */
    public MyPaymentResponse createPayment(MyPaymentRequest paymentRequest, Reservation reservation ){
        headers = new HttpHeaders();
        headers.add("x-api-key","AQEqgXvdQM2NG2Yd7nqxnH12lOySTp9fLLQcCjPbkGGe2jL38kaxvMstpBsHEMFdWw2+5HzctViMSCJMYAc=-Zbtl4WVw7/7E0NntxpkR8FYl6qi8ilrBBvujGmK7b6U=-5Vp$QU2E7Lk[k6uk");
        String merchantAccount = "ShiftrAG626ECOM";
        paymentRequest.setMerchantAccount(merchantAccount);
        MyAmount myAmount= new MyAmount();
        myAmount.setCurrency("CHF");
        myAmount.setValue(reservation.getCosts()*100);
        paymentRequest.setAmount(myAmount);
        MyPaymentMethod paymentMethod= new MyPaymentMethod();
        paymentMethod.setType("twint");
        paymentRequest.setPaymentMethod(paymentMethod);
        paymentRequest.setCountryCode("CH");
        paymentRequest.setReference(reservation.getId().toString());
        paymentRequest.setReturnUrl("https://a62d-2a02-1210-2af2-8d00-9947-ebd1-3297-8c0b.eu.ngrok.io/checkingPaymentStatus");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<MyPaymentRequest>entity = new HttpEntity<>(paymentRequest, headers);
        /**
         * Error Handling for api
         * The error is passed with the response object
         */
        try{
            myPaymentResponse = restTemplate.postForObject("https://checkout-test.adyen.com/v70/payments",entity, MyPaymentResponse.class);
        }catch (Exception e
        ){
            myPaymentResponse= new MyPaymentResponse();
            myPaymentResponse.setError(e);
        }

        assert myPaymentResponse != null;
        myPaymentResponse.setReservation(reservation);
        return myPaymentResponse;
    }
    /**
     * API for shortening URL
     *
     */
    public JSONObject getShortURL(String url){
        restTemplate = new RestTemplate();
        JSONObject request = new JSONObject();
        request.put("url",url);
        headers= new HttpHeaders();
        headers.set("X-RapidAPI-Key","290f4ae19amshe536a10be4edebdp18bad9jsnf8b9087e3e0b");
        headers.set("X-RapidAPI-Host","url-shortener-service.p.rapidapi.com");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<JSONObject> entity = new HttpEntity<>(request, headers);
        try {
            shrotURLResponse = restTemplate.postForObject("https://url-shortener-service.p.rapidapi.com/shorten", entity, JSONObject.class);
        }catch (Exception e){
            shrotURLResponse.put("error",e.getMessage());
        }

        return shrotURLResponse;
    }
    /**
     * API for getting a QR code as a byte array
     *
     */
    public byte[] getQRcode(String url){
        restTemplate = new RestTemplate();
        headers= new HttpHeaders();
        //request body:
        JSONObject request = new JSONObject();
        request.put("frame_name","no-frame");
        request.put("qr_code_text",url);
        request.put("image_format","SVG");
        request.put("background_color","#ffffff");
        request.put("foreground_color","#000000");
        request.put("marker_right_inner_color","#006474");
        request.put("marker_right_outer_color","#00a9c5");
        request.put("marker_left_template","version13");
        request.put("marker_right_template","version13");
        request.put("marker_bottom_template","version13");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<JSONObject> entity = new HttpEntity<>(request, headers);
        return restTemplate.postForObject("https://api.qr-code-generator.com/v1/create?access-token=L6aJQLQoBsdk97mnPccuyv1GVmcKQ1JT9G0NGei_PwlKO0dnrOYXOnUhyzZyLpTK",entity, byte[].class);

    }
    /**
     * API for getting payment details (status)
     *
     */
    public JSONObject getPaymentDetails(String redirectResult) {
        restTemplate = new RestTemplate();
        headers = new HttpHeaders();
        headers.add("x-api-key", "AQEqgXvdQM2NG2Yd7nqxnH12lOySTp9fLLQcCjPbkGGe2jL38kaxvMstpBsHEMFdWw2+5HzctViMSCJMYAc=-Zbtl4WVw7/7E0NntxpkR8FYl6qi8ilrBBvujGmK7b6U=-5Vp$QU2E7Lk[k6uk");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        String decodedRedirectR = null;
        try {
            decodedRedirectR = java.net.URLDecoder.decode(redirectResult, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            //ignore
        }
        JSONObject request = new JSONObject();
        JSONObject subObj = new JSONObject();
        subObj.put("redirectResult", decodedRedirectR);
        request.put("details",subObj);
        HttpEntity<JSONObject> entity = new HttpEntity<>(request, headers);
        try{
            paymentDetailResponse = restTemplate.postForObject("https://checkout-test.adyen.com/v70/payments/details",entity,JSONObject.class);
        }catch (Exception e){
            paymentDetailResponse.put("error",e.getMessage());
        }

        return paymentDetailResponse;

    }
}
