package com.example.application.service;

import com.example.application.entities.ApplicationUser;
import com.example.application.entities.Reservation;
import com.example.application.entities.Room;
import com.example.application.repos.ReservationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class ReservationService implements CrudListener<Reservation> {
    private final ReservationRepo reservationRepo;

    @Autowired
    public ReservationService(ReservationRepo reservationRepo) {
        this.reservationRepo = reservationRepo;
    }

    @Override
    public Collection<Reservation> findAll() {
        return reservationRepo.findAll();
    }

    @Override
    public Reservation add(Reservation reservation) {
        return reservationRepo.save(reservation);
    }

    @Override
    public Reservation update(Reservation reservation) {
        return reservationRepo.save(reservation);
    }

    @Override
    public void delete(Reservation reservation) {
        reservationRepo.delete(reservation);

    }

    public List<Reservation> findAllReservationsByRoom(Room room) { return reservationRepo.findReservationsByRoom(room);}

    public Reservation findReservationById (Long id){
        return reservationRepo.findReservationById(id);
    }


    public Reservation findReservationByEntryId (String id) { return reservationRepo.findReservationByEntryIds(id);
    }

    public List<Reservation> findAllReservationsByUser (ApplicationUser user){
        return reservationRepo.findReservationsByUser(user);
    }

}
