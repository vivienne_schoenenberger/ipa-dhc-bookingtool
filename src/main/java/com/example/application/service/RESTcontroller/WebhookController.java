package com.example.application.service.RESTcontroller;

import com.example.application.entities.Reservation;
import com.example.application.repos.ReservationRepo;
import com.example.application.service.ReservationService;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedSession;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Configuration
@EnableWebSecurity
@RestController
@RequestMapping("api")
public class WebhookController {

    private ReservationService reservationService;
    private MultiValueMap<String,String> webhookResult;
    private final ReservationRepo reservationRepo;
    private Reservation reservation;

    public WebhookController(ReservationService reservationService,
                             ReservationRepo reservationRepo) {
        this.reservationService = reservationService;
        this.reservationRepo = reservationRepo;
    }
    /**
     * Webhook Endpoint
     */
    @PostMapping (value="/webhook/notification",consumes = "application/x-www-form-urlencoded",produces ="application/json")
    public JSONObject createDetails (@RequestBody MultiValueMap<String, String> request){
        JSONObject response = new JSONObject();
        response.put("notificationResponse","[accepted]");
        webhookResult = request ;
        if (reservationService.findReservationById(Long.valueOf(request.get("merchantReference").get(0)))!=null){
            reservation = reservationService.findReservationById(Long.valueOf(request.get("merchantReference").get(0)));
            reservation.setPaymentStatus(request.get("success").get(0));
            reservationService.update(reservation);
        }

        return response;
    }
}
