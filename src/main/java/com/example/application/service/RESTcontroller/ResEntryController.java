package com.example.application.service.RESTcontroller;

import com.example.application.entities.ResEntry;
import com.example.application.service.ResEntryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResEntryController {
    ResEntryService resEntryService;

    public ResEntryController(ResEntryService resEntryService) {
        this.resEntryService = resEntryService;
    }


    @CrossOrigin
    @GetMapping(value= "/resEntry/{entry_id}")
    public ResponseEntity<ResEntry> getByEntryId(@PathVariable String entry_id){
        ResEntry resEntry = resEntryService.findResEntryByEntryId(entry_id);
            return new ResponseEntity<>(resEntry, HttpStatus.OK);

    }
}
