package com.example.application.service.RESTcontroller;

import com.example.application.entities.Room;
import com.example.application.service.RoomService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomController {
    RoomService roomService;

    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }


    @CrossOrigin
    @GetMapping(value= "/room/{room_id}")
    public ResponseEntity<Room> getByEntryId(@PathVariable Long room_id){
        Room room = roomService.findRoomById(room_id);
        return new ResponseEntity<>(room, HttpStatus.OK);

    }
}
