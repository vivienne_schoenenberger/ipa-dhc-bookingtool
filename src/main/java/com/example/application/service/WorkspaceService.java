package com.example.application.service;

import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.repos.WorkspaceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;

import java.util.Collection;
import java.util.Set;


@Service
public class WorkspaceService implements CrudListener<Workspace> {

    private final WorkspaceRepo workspaceRepo;


    @Autowired
    public WorkspaceService(WorkspaceRepo workspaceRepo) {
        this.workspaceRepo = workspaceRepo;
    }


    @Override
    public Collection<Workspace> findAll() {
        return workspaceRepo.findAll();
    }

    @Override
    public Workspace add(Workspace workspace) {
        return workspaceRepo.save(workspace);
    }

    @Override
    public Workspace update(Workspace workspace) {
        return workspaceRepo.save(workspace);
    }

    @Override
    public void delete(Workspace workspace) {
         workspaceRepo.delete(workspace);

    }

    public Workspace findWorkspaceById (Long id){
        return workspaceRepo.findWorkspaceById(id);
    }

    public Set<Workspace> findAllByRoom(Room room){
        return workspaceRepo.findAllByRoom(room);
    }
}
