package com.example.application.filter;

import com.example.application.view.calendar.CalendarForm;
import com.example.application.entities.ResEntry;
import com.example.application.entities.Reservation;
import com.example.application.entities.Room;
import com.example.application.entities.Workspace;
import com.example.application.service.ResEntryService;
import lombok.SneakyThrows;

import java.sql.*;
import java.time.*;
import java.util.*;

public class TimeFilter {
    private LocalDateTime dateTimeCurrentOfSameRoom;
    private LocalDateTime endOfReservationTimeCDSR;

    private List<LocalDateTime> occupiedDaysInReoccurrance;

    private ResEntryService resEntryService;

    private ResEntry resEntryOccupying;

    public TimeFilter(ResEntryService resEntryService) {
        this.resEntryService = resEntryService;

    }

    public static boolean isOverlapping(LocalDateTime start1, LocalDateTime end1, LocalDateTime start2, LocalDateTime end2) {
        //2 starts before 1 ends and after 1 started
        //2 ends before 1 ends and after 1 started
        //2 starts when 1 starts
        //2 ends when 1 ends

        //not overlapping:
        //2 starts and ends before 1 starts
        //2 starts when 1 ends
        //2 starts after 1 ends
        if ((start2.compareTo(start1) < 0 && end2.compareTo(start1) < 0) || start2.compareTo(end1) >= 0) {

            return false;
        } else return true;

    }

    @SneakyThrows
    public boolean checkIfResAvailable(ResEntry resEntry, Reservation reservation) {
        List<Boolean> checkTimeAll = new ArrayList<>();
        Room resEntryRoom = resEntry.getRoom();
        Set<Workspace> resEntryWorkspace = resEntry.getWorkspaceList();
        Set<ResEntry> existingResEntries = resEntryService.findAllByRoom(resEntryRoom);

        for (ResEntry existingResEntr : existingResEntries) {
            //check room
            if (resEntryRoom != null) {
                //check if there are resEntries with the same room
                if (hasSameRoom(existingResEntr.getRoom(), resEntryRoom)) {
                    //check if it isnt the same resEntry as current
                    if (!Objects.equals(existingResEntr.getResEntryId(), resEntry.getResEntryId())) {
                        if (reservation.getId() != null) {
                            if (getReservationDetailsOfResEntry(existingResEntr) != reservation.getId()) {
                                LocalDateTime existEntrStart = existingResEntr.getStartTime();
                                LocalDateTime existEntrEnd = existingResEntr.getEndTime();
                                if (isOverlapping(existEntrStart, existEntrEnd, resEntry.getStartTime(), resEntry.getEndTime())) {
                                    checkTimeAll.add(false);
                                    setResEntryOccupying(existingResEntr);
                                }
                                checkTimeAll.add(true);
                            }
                        } else {
                            LocalDateTime existEntrStart = existingResEntr.getStartTime();
                            LocalDateTime existEntrEnd = existingResEntr.getEndTime();
                            if (isOverlapping(existEntrStart, existEntrEnd, resEntry.getStartTime(), resEntry.getEndTime())) {
                                checkTimeAll.add(false);
                                setResEntryOccupying(existingResEntr);
                            }
                            checkTimeAll.add(true);
                        }
                    } else checkTimeAll.add(true);
                }
            } else {
                if (resEntryWorkspace != null) {
                    //check if workspace of resEntry has same workspace as existing entry
                    if (hasSameWorkspaces(resEntryWorkspace, existingResEntr.getWorkspaceList())) {
                        if (!Objects.equals(existingResEntr.getResEntryId(), resEntry.getResEntryId())) {
                            if (reservation.getId() != null) {
                                if (getReservationDetailsOfResEntry(existingResEntr) != reservation.getId()) {
                                    LocalDateTime existEntrStart = existingResEntr.getStartTime();
                                    LocalDateTime existEntrEnd = existingResEntr.getEndTime();
                                    if (isOverlapping(existEntrStart, existEntrEnd, resEntry.getStartTime(), resEntry.getEndTime())) {
                                        checkTimeAll.add(false);
                                        setResEntryOccupying(existingResEntr);
                                    }
                                    checkTimeAll.add(true);
                                }
                                checkTimeAll.add(true);
                            }
                            else{
                                LocalDateTime existEntrStart = existingResEntr.getStartTime();
                                LocalDateTime existEntrEnd = existingResEntr.getEndTime();
                                if (isOverlapping(existEntrStart, existEntrEnd, resEntry.getStartTime(), resEntry.getEndTime())) {
                                    checkTimeAll.add(false);
                                    setResEntryOccupying(existingResEntr);
                                }
                                checkTimeAll.add(true);
                            }
                        }
                    }
                }
            }
        }

        return !checkTimeAll.contains(false);
    }

    public boolean hasSameRoom(Room existRoom, Room newRoom) {
        return Objects.equals(existRoom.getId(), newRoom.getId());
    }

    public boolean hasSameWorkspaces(Set<Workspace> newWorkspaces, Set<Workspace> existWorkspaces) {
        List<Boolean> checkWorkspace = new ArrayList<>();
        for (Workspace existWorkspace : existWorkspaces) {
            for (Workspace newWorkspace : newWorkspaces) {
                if (existWorkspace.getId().equals(newWorkspace.getId())) {
                    checkWorkspace.add(true);
                } else checkWorkspace.add(false);
            }

        }
        return checkWorkspace.contains(true);
    }

    private void deleteOccupiedP(CalendarForm editorForm) {
        editorForm.getChildren().forEach(child -> {
            if (child.getId().equals(Optional.of("occupation"))) {
                editorForm.remove(child);
            }

        });
    }

    private int getReservationDetailsOfResEntry(ResEntry resEntry) throws SQLException {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "mysecretpassword");
        Statement statement = c.createStatement();
        String sql = "select reservation_id from bookingtool.reservation_res_entries where res_entries_res_entry_id ='" + resEntry.getResEntryId() + "'";
        ResultSet rs = statement.executeQuery(sql);
        int resID = 0;
        while (rs.next()) {
            resID = rs.getInt("reservation_id");
        }
        c.close();
        return resID;
    }

    public ResEntry getResEntryOccupying() {
        return resEntryOccupying;
    }

    public void setResEntryOccupying(ResEntry resEntryOccupying) {
        this.resEntryOccupying = resEntryOccupying;
    }
}
