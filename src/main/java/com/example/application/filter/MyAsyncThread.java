package com.example.application.filter;

import com.example.application.entities.Reservation;
import com.example.application.service.ReservationService;
import com.example.application.view.payment.PaymentPending;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.server.VaadinSession;
import org.springframework.stereotype.Service;

import java.util.Objects;

public class MyAsyncThread extends Thread {
    private final UI ui;
    private ReservationService reservationService;
    private Reservation reservation;

    private int count = 0;

    public MyAsyncThread(UI ui, ReservationService reservationService, Reservation reservation) {
        this.ui = ui;
        this.reservationService = reservationService;
        this.reservation = reservation;

    }

    @Override
    public void run() {
        /**
         * testing if status has updated
         */
        try {
            while (reservationService.findReservationById(reservation.getId()).getPaymentStatus().equals("pending")) {
                sleep();
            }


            /**
             * navigating accoring to status
             */
            ui.access(() -> {
                if (Objects.equals(reservationService.findReservationById(reservation.getId()).getPaymentStatus(), "true")) {
                    ui.navigate("payment-successfull?id=" + reservation.getId());
                }
                if (Objects.equals(reservationService.findReservationById(reservation.getId()).getPaymentStatus(), "false")) {
                    ui.navigate("payment-unsuccessfull?id=" + reservation.getId());
                }
            });

        } catch (NullPointerException e) {
            //ignore
        }
    }


    /**
     * Make thread sleep for 30 sec
     */
    public void sleep() {
        try {
            count = 0;
            while (count <= 30) {
                Thread.sleep(1000);
                count++;

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            Notification notification = new Notification(String.valueOf(e), 3000, Notification.Position.MIDDLE);
            notification.open();
        }
    }
}
