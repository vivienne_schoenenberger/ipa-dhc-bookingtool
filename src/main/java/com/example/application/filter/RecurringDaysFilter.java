package com.example.application.filter;

import java.time.*;
import java.util.*;

public class RecurringDaysFilter {


    public List<LocalDate> recurringDays(LocalDate recStart, LocalDate recEnd, Set<DayOfWeek> selectedRecurringDays) {
        List<LocalDate> days = new ArrayList<>();
        long daysBetweenCurrent = Duration.between(recStart.atStartOfDay(), recEnd.atStartOfDay()).toDays();
        LocalDate varDate = recStart;
        for (int i = 0; i <= daysBetweenCurrent; i++) {
            for (DayOfWeek recurringDay : selectedRecurringDays) {
                if (checkDay(varDate.atStartOfDay()) == recurringDay) {
                        days.add(varDate);

                    }
                }
            varDate=varDate.plusDays(1);

        } return days;
    }
    DayOfWeek checkDay(LocalDateTime date) {
        Date dateD = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateD);
        switch (cal.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                return DayOfWeek.SUNDAY;
            case 2:
                return DayOfWeek.MONDAY;
            case 3:
                return DayOfWeek.TUESDAY;
            case 4:
                return DayOfWeek.WEDNESDAY;
            case 5:
                return DayOfWeek.THURSDAY;
            case 6:
                return DayOfWeek.FRIDAY;
            case 7:
                return DayOfWeek.SATURDAY;
        }
        return null;
    }
}
