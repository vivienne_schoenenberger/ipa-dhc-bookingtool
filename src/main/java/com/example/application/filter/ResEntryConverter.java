package com.example.application.filter;

import com.example.application.entities.ResEntry;
import com.example.application.entities.Reservation;
import com.example.application.service.ResEntryService;
import com.example.application.service.ReservationService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ResEntryConverter {
    private final ReservationService reservationService;
    private final ResEntryService resEntryService;


    public ResEntryConverter(ReservationService reservationService, ResEntryService resEntryService) {
        this.reservationService = reservationService;
        this.resEntryService = resEntryService;
    }

    public List<ResEntry> convertResToEntry(Reservation reservation) {
        List<ResEntry> resEntryArrayList = new ArrayList<>();
        RecurringDaysFilter recurringDaysFilter = new RecurringDaysFilter();
        if (reservation.isRecurring()) {
            LocalDate recStart;
            LocalDate recEnd;
            if (reservation.isSpecificTime()) {
                recStart = reservation.getStart_time().toLocalDate();
                recEnd = reservation.getEnd_time().toLocalDate();
            } else {
                recStart = reservation.getStart_date();
                recEnd = reservation.getEnd_date();
            }
            for (LocalDate day : recurringDaysFilter.recurringDays(recStart, recEnd, reservation.getReoccurringDays())) {
                ResEntry recResEntry = new ResEntry();
                recResEntry.setRoom(reservation.getRoom());
                recResEntry.setWorkspaceList(reservation.getWorkspaceList());
                if (reservation.isSpecificTime()) {
                    recResEntry.setStartTime(day.atStartOfDay().plusHours(reservation.getStart_time().getHour()).plusMinutes(reservation.getStart_time().getMinute()));
                    recResEntry.setEndTime(day.atStartOfDay().plusHours(reservation.getEnd_time().getHour()).plusMinutes(reservation.getEnd_time().getMinute()));
                } else {
                    recResEntry.setStartTime(day.atStartOfDay().plusHours(5));
                    recResEntry.setEndTime(day.atTime(22,0));
                    recResEntry.setAllDay(true);
                }
                float totalResEntryTime=0;
                for(LocalDate date = recResEntry.getStartTime().toLocalDate(); date.isBefore(recResEntry.getEndTime().toLocalDate().plusDays(1));date = date.plusDays(1)){
                    totalResEntryTime += recResEntry.getEndTime().getHour()-recResEntry.getStartTime().getHour();

                }
                recResEntry.setResEntryTime(totalResEntryTime);
                resEntryArrayList.add(recResEntry);
            }

        } else {
            ResEntry resEntry = new ResEntry();
            resEntry.setRoom(reservation.getRoom());
            resEntry.setWorkspaceList(reservation.getWorkspaceList());
            if (reservation.isSpecificTime()) {
                resEntry.setStartTime(reservation.getStart_time());
                resEntry.setEndTime(reservation.getEnd_time());
            } else {
                    resEntry.setStartTime(reservation.getStart_date().atStartOfDay().plusHours(5));
                    resEntry.setEndTime(reservation.getEnd_date().atTime(22, 0, 0));
                    resEntry.setAllDay(true);
                }
            float totalResEntryTime=0;
            for(LocalDate date = resEntry.getStartTime().toLocalDate(); date.isBefore(resEntry.getEndTime().toLocalDate().plusDays(1));date = date.plusDays(1)){
                totalResEntryTime += resEntry.getEndTime().getHour()-resEntry.getStartTime().getHour();

            }
            resEntry.setResEntryTime(totalResEntryTime);

            resEntryArrayList.add(resEntry);
        }

        return resEntryArrayList;
    }
}
