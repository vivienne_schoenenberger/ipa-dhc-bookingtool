
import tippy from 'tippy.js';
import '@fullcalendar/core/vdom' // ! add it
import '@fullcalendar/core';
import {FullCalendar} from "/generated/jar-resources/full-calendar";

export class FullCalendarWithTooltip extends FullCalendar {

    static get is() {
        return 'full-calendar-with-tooltip';
    }

    _initCalendar() {
        super._initCalendar();
        this.getCalendar().setOption("eventDidMount", e => {
            this.initTooltip(e);
        });
    }

    initTooltip(e) {
        if (!e.isMirror) {
            e.el.addEventListener("mouseenter", () => {
                let id  = e.event._def.publicId;
                findEntryDetails(id)

                    e.el._tippy = tippy(e.el, {
                        theme: 'light',
                        content: document.getElementById("final-hover-div"),
                        placement: 'bottom-end',
                        arrow:false,
                        trigger: 'manual'
                    });
                    e.el._tippy.show();
            })
            e.el.addEventListener("mouseleave", () => {
                if (e.el._tippy) {
                    e.el._tippy.destroy();
                }
            })
        }
    }
}
function findEntryDetails (entryId){
    var obj
    console.log("entry_id: "+entryId)
    fetch("https://localhost:8443/resEntry/"+entryId)
        .then(res=> res.json())
        .then(data =>{

            show(data)
            obj = data
        })
        .catch(err=>{
            console.error('Error: ',err)
        })
    console.log(obj);


}
function findRoomDetails(roomId){
    var obj
    fetch("https://localhost:8443/room/"+roomId)
        .then(res=>res.json())
        .then(data=>{
            obj=data
            showPicture(data)
        })
        .catch(err=>{
            console.error("Error: ",err)
        })
    console.log("ROOM OBJ: ", obj)
}

function show(data) {

    let roomAndtime ='';
    let wpSegment = '';
    let title =
        `<div style="height: max-content; width: max-content; margin: 0px; padding: 0px">
            <div style="background-color: ${data.color.toString()}; margin: 0px; width: 300px;">
                <h1 style="margin: 0px; font-size: 20px; color:white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">${data.title}</h1>
            </div>
            <div style="width: inherit;">
                <p>${data.description}</p> <br>`
                if(data.workspaceList != null){
                    data.workspaceList.forEach(workspaceD =>{
                        let workspaceSegment = `<p>${workspaceD.bezeichnung} is occupied</p> <br>`;
                        wpSegment += workspaceSegment;
                    });
                }
                if(data.room!=null){

                roomAndtime =
                    `<p>${data.room.bezeichnung} is occupied</p> <br>
                    <p>from: ${data.startTime}</p> <br>
                    <p>until: ${data.endTime}</p><br>
                    </div>
                </div>`;}
                else{

                    roomAndtime =
                        `<p>from: ${data.startTime}</p> <br>
                        <p>until: ${data.endTime}</p><br>
                    </div>
                   </div>`;

                }
    let tab = title + wpSegment + roomAndtime;
    console.log(tab)
    console.log(data.workspaceList)
    let div = document.createElement('div');
    div.setAttribute("id","hover-div");
    document.body.appendChild(div);
    document.getElementById("hover-div").innerHTML = tab;
    if(data.room!=null){
        findRoomDetails(data.room.id)
    }
    else{
        showWorkspace()
    }

}
function showWorkspace(){
    let roomPic=`<div style="height: 90px; margin:0px; width: 300px; "><img style="width: inherit; height: inherit; object-fit: none !important; border-top-left-radius: 5px; border-top-right-radius:5px" src="default_workspace.jpg" alt=""></div>`

    let dataSegment = document.getElementById("hover-div").innerHTML;
    let div = document.createElement('div');
    div.setAttribute("id","final-hover-div");

    document.body.appendChild(div);
    document.getElementById("final-hover-div").innerHTML = roomPic+dataSegment;


}function showPicture(data){
    let roomPic=`<div style="height: 90px; margin:0px; width: 300px; "><img style="width: inherit; height: inherit; object-fit: none !important; border-top-left-radius: 5px; border-top-right-radius:5px" src="${data.imageName}" alt=""></div>`

    let dataSegment = document.getElementById("hover-div").innerHTML;
    let div = document.createElement('div');
    div.setAttribute("id","final-hover-div");

    document.body.appendChild(div);
    document.getElementById("final-hover-div").innerHTML = roomPic+dataSegment;


}



customElements.define(FullCalendarWithTooltip.is, FullCalendarWithTooltip);